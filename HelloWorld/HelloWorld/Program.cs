﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Program
    {
        /// <summary>
        /// A method 
        /// </summary>
        /// <param name="s">The string to print</param>
        public static void foo (string s)
        {
            Console.WriteLine("Printing the string s = " + s);
        }


        static void printInt(int x)
        {
            Console.WriteLine(x);
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            //Some code to demonstrate a method call
            foo("Hi again");

            //Some code to test the debugger
            int x = 0;
            int[] iArray = { 12, 43, 54};

            //foreach
            foreach(int j in iArray)
            {
                //note the format specifiers in {}. They can be used to format the data.
                Console.WriteLine("The current element is {0} and its square is {1}", j, Math.Pow(j, 2));
                printInt(j);
            }
            for (int i =0; i < 10; i++)
            {
                x = i;
                Console.WriteLine("The value of x is {0}", x);
            }

           
        }
    }
}
