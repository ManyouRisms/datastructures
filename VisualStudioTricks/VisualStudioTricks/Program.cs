﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStudioTricks
{
    class Program
    {
    static void LoadDataFromFile(List<Person> people)
{
	StreamReader sr = new StreamReader(File.Open("People.txt", FileMode.Open));
	string sInput = "";

	try
	{
		//Read a line from the file
		while ((sInput = sr.ReadLine()) != null)
		{
			try
			{
				char[] cArray = { ' ' };
				string[] sArray = sInput.Split(cArray);
				int iSSN = Int32.Parse(sArray[0]);
				Person p = new Person(iSSN, sArray[2], sArray[1]);
				people.Add(p);

			}
			catch (ApplicationException ae)
			{
				Console.WriteLine("Exception: " + ae.Message);
			}
            ;

		}

	}
	catch (Exception ex)
	{
		Console.WriteLine(ex.Message);
	}
	sr.Close();
}
        static void Main(string[] args)
        {
            List<Person> list = new List<Person>();
            LoadDataFromFile(list);

            foreach (Person p in list)
            {
                Console.WriteLine(p.ToString());
            }
        }
    }
}
