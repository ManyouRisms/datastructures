﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorts
{
    class HeapSorter<T> : AbstractSorter<T> where T : IComparable<T>
    {
        public HeapSorter(T[] array)
            : base(array)
        {
        }

        public override void Sort()
        {
            Heapify();
            for (int i = array.Length-1; i > 0; i--) // no point of going to 0 if its the only thing in the heap, go greater than 0
            {
                Swap(0, i);
                SiftDown(0, i - 1); 
            }
        }


        protected void Heapify()
        {
            // turn the entire list into the heap on the first time, after that only sift down because everything else is already a heap
            int lastParentIndex = GetParent(array.Length - 1);
            for (int i = lastParentIndex; i >= 0; i--)
            {
                SiftDown(i, array.Length - 1); // if we have a heap it always starts at 0. 
            }
        }

        // need a way to sift something to the bottom of the list

        protected void SiftDown(int parentIndex, int lastIndexInHeap)
        {
            // has to be a left child if there is any child at all
            
            // given parent index, find the children
            int left = FindLeft(parentIndex);
            int right = FindRight(parentIndex);

            // need largest out of the parent, left, and right
            // very easy way, just need to compare two of them.
            int indexOfLargest = parentIndex; // afaik the largest is whats in the parent (possibly nto though)

            if (left <= lastIndexInHeap)
            {
                // i have at least a left child 
                if (this.array[parentIndex].CompareTo(this.array[left]) < 0)
                {
                    // then the largest thing we have so far is the left child
                    indexOfLargest = left;
                }

                if (right <= lastIndexInHeap && this.array[indexOfLargest].CompareTo(this.array[right]) < 0)
                {
                    indexOfLargest = right;
                }

                if (indexOfLargest != parentIndex) // one of the children was the largest thing
                {
                    Swap(parentIndex, indexOfLargest); // either left or right, doesn't matter which we have its index
                    SiftDown(indexOfLargest, lastIndexInHeap);

                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentIndex"></param>
        /// <returns></returns>
        public int FindLeft(int parentIndex)
        {
            return 2 * parentIndex + 1;
        }

        public int FindRight(int parentIndex)
        {
            return 2 * parentIndex + 2;
        }

        public int GetParent(int childIndex)
        {
            return (childIndex-1)/2;
        }

    }
}
