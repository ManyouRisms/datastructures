﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorts
{
    class InsertionSorter<T> : AbstractSorter<T> where T : IComparable<T>
    {
        public InsertionSorter(T[] array)
            : base(array)
        {

        }



        public override void Sort()
        {
            InsertionSortWithGap(0, 1);
        }



        protected void InsertionSortWithGap(int startIndex, int gap)
        {
            //Split the array into sorted and unsorted portions. Start the sorted portion out with the left most element
            //Take the next item from the unsorted portion
            //Shift it left by swapping until it's in the correct position
            //When there are no more items in the unsorted portion, we're done

            for (int unsortedListStartIndex = startIndex + gap; unsortedListStartIndex < this.Length; unsortedListStartIndex += gap)
            {
                //unsortedListStartIndex is the position at which the next unsorted item is initially found. This also
                // serves as our divide between the sorted and unsorted portions of the array.

                //CurrentUnsortedItemIndex is the current position in the list of the item we're inserting into the sorted list
                int currentUnsortedItemIndex = unsortedListStartIndex;

                //If we can still shift the "unsorted" item further to the left in the sorted section, and 
                //it should be shifted left (ie it's less than the item to its left), then shift it left
                while (currentUnsortedItemIndex - gap >= 0 &&
                    this.array[currentUnsortedItemIndex].CompareTo(this.array[currentUnsortedItemIndex - gap]) < 0)  // todo calculate the index outside of loop maybe? efficiency
                {
                    this.Swap(currentUnsortedItemIndex, currentUnsortedItemIndex - gap);
                    currentUnsortedItemIndex -= gap;
                }
            }
        }
    }
}


        //// split the array into sorted and unsorted portions. Start the sorted portion out with the left most element
            //// take the next item from the unsorted poriton
            //// shift it left by swapping until it's in the correct position
            //// when there are no more items in the unsorted poriton, we're done
            //int i, j;

            //for (i = 1; i < this.array.Length; i++)
            //{
            //    T value = this.array[i];
            //    j = i - 1;

            //    while ((j >= 0) && (this.array[j].CompareTo(value) > 0))
            //    {
            //        Swap(j + 1, j);
            //        j--;
            //    }
            //    this.array[j + 1] = value;
            //}
