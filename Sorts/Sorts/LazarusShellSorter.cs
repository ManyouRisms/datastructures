﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorts
{
    class LazarusShellSorter<T> : ShellSorter<T> where T : IComparable<T>
    {
        public LazarusShellSorter(T[] array)
            : base(array)
        {

        }

        /// <summary>
        /// Returns a gap list according to lazaraus & frank specifications
        /// 2(arraySize/2^(k-1))+1
        /// </summary>
        /// <param name="arraySize">Size of array to base the gaplist on</param>
        /// <returns>Queue of gaps, with the largest first, and 1 last</returns>
        protected override IEnumerable<int> GetGapList(int arraySize)
        {
            int gap = 1;
            Queue<int> gaps = new Queue<int>();
            int gapNumber = 1;

            gap = 2 * (arraySize / pow(2, gapNumber + 1)) + 1;

            while (gap >= 1)
            {
                gaps.Enqueue(gap);
                gapNumber++;
                gap = 2 * (arraySize / pow(2, gapNumber + 1)) + 1;

                // a somewhat hacky way to exit properly. other than switching 
                // to a stack and changing things around, i couldnt think of 
                // a better way to do it with a queue

                if (gap == 1)
                {
                    gaps.Enqueue(gap);
                    gap = -1;
                }
            }
            return gaps;
        }
    }
}