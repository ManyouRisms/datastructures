﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorts
{
    class KnuthShellSorter<T> : ShellSorter<T> where T : IComparable<T>
    {
        public KnuthShellSorter(T[] array)
            : base(array)
        {

        }

        /// <summary>
        /// 
        /// Get a list of gaps, first of which is the largest, declining 
        /// to the smallest gap (being 1, <-- necessary)
        /// </summary>
        /// <param name="arraySize">How big of an array to get the gaplist for</param>
        /// <returns></returns>
        protected override IEnumerable<int> GetGapList(int arraySize)
        {

            // knuths algorthm for generating gaps

            //gap 1 is (3^1-1)/2
            //gap 2 is (3^2-1/2
            //gap 3 is (3^3-1)/2
            //.. as long as gap is < arraySize/3

            Stack<int> gaps = new Stack<int>();
            int gap = 1;
            int gapNumber = 1;

            gap = (pow(3, gapNumber) - 1) / 2;
            while (gap < arraySize / 3)
            {
                gaps.Push(gap);
                gapNumber++;
                gap = (pow(3,gapNumber)-1)/2;
            }
            return gaps;
        }
    }
}