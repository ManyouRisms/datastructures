﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorts
{
    class BubbleSorter<T> : AbstractSorter<T> where T : IComparable<T>
    {
        public BubbleSorter(T[] array) : base(array)
        {

        }

        public override void Sort()
        {
            int count = 0;
            bool sorted = false;
            while (!sorted)
            {
                sorted = true;
                for (int i = 0; i < this.array.Length-1 - count; i++)
                {
                    if (this.array[i].CompareTo(this.array[i + 1]) > 0)
                    {
                        Swap(i, i+1);
                        sorted = false;
                    }
                }
                count++;
            }            
        }
    }
}
