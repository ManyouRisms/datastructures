﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorts
{
    class RadixSorter<T> : AbstractSorter<T> where T : IComparable<T>
    {
        List<List<int>> buckets;
        int[] intArray;
        public RadixSorter(T[] array)
            : base(array)
        {

            // big time troubles if the items in array aren't integers i reckon
            intArray = new int[array.Length];

            for (int i = 0; i < array.Length; i++)
            {
                intArray[i] = int.Parse("" + array[i]);
            }
        }

        public override void Sort()
        {

            int significance = 1;
            buckets = new List<List<int>>(10);
            int count = GetIterationCount();
            for (int i = 0; i < count; i++, significance++)
            {
                int currNum = GetIntegerPlace(significance, i);
                Console.WriteLine("\n\ncount: " + count + "\n\ncurrNum: " + currNum);
                buckets[currNum].Add(currNum); 
            }
        }

        public int GetIntegerPlace(int significance, int index)
        {
            Stack<int> vals = new Stack<int>();
            for (int i = 0; i <= significance; i++)
            {
                vals.Push(this.intArray[index] % 10);
            }

            return vals.Pop();
        }

        private int GetIterationCount()
        {
            int bucketsLength = 0;

            for (int i = 0; i < this.intArray.Length; i++)
            {
                int count = 0;
                do
                {
                    count++;
                } while ((this.intArray[i] /= 10) >= 1);

                if (count > bucketsLength)
                {
                    bucketsLength= count;
                }
            }
            return bucketsLength;
        }
    }
}
