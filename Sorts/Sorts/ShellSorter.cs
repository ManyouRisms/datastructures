﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorts
{
    class ShellSorter<T> : InsertionSorter<T> where T : IComparable<T>
    {
        public ShellSorter(T[] array)
            : base(array)
        {

        }


        public override void Sort()
        {
            //For each gap in the list of gaps
            //Insertion sort each sublist in this list, with elements separated by gap.
            foreach (int gap in this.GetGapList(this.Length))
            {
                for (int startOfList = 0; startOfList < gap; startOfList++)
                {
                    this.InsertionSortWithGap(startOfList, gap);
                }
            }
        }

        /// <summary>
        /// Get a list of values to use for gap. First item when iterating should be the largest,
        /// last item should be 1.
        /// </summary>
        /// <param name="arraySize"></param>
        /// <returns></returns>
        protected virtual IEnumerable<int> GetGapList(int arraySize)
        {
            int gap = arraySize / 2;
            while (gap >= 1)
            {
                yield return gap;
                gap = gap / 2;
            }
        }

        public static int pow(int num, int exp)
        {
            int rVal = num;
            if (exp == 0)
            {
                rVal = 1;
            }
            for (int i = 1; i < exp; i++)
            {
                rVal *= num;
            }
            return rVal;
        }
    }
}