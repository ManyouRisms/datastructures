﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorts
{
    class Quicksorter<T> : AbstractSorter<T> where T : IComparable<T>
    {

        public Quicksorter(T[] array)
            : base(array)
        {

        }


        public override void Sort()
        {
            // calls the quicksort method
            doQuickSort(0, array.Length - 1);
        }


        /// <summary>
        /// Recursive quick sort method
        /// </summary>
        /// <param name="left">first item in the 'array'</param>
        /// <param name="right">last item in the 'array'</param>
        protected void doQuickSort(int left, int right)
        {
            if (left < right)
            {
                //int partitionPosition = Partition(left, right);
                int partitionPosition = Partition(left, right);

                doQuickSort(left, partitionPosition - 1);


                doQuickSort(partitionPosition + 1, right);
            }
        }

        // Partition step. Choose a pivot, re-arrange the list so that the
        // pivot is in a position where everything to its left is less than,
        // everything to its right is greater than. Return the final position of the
        // pivot
        protected virtual int Partition(int left, int right)
        {
            // get the pivot (right)
            int pivot = GetPivotBetween(left, right);

            // value of the pivot
            T pivVal = array[pivot];

            // swap for the pivot
            Swap(pivot, right);

            // initially set final position to left
            int finalPosition = left;

            for (int i = left; i <= right; i++)
            {
                if (this.array[i].CompareTo(pivVal) == -1) // if current position is less than the pivot value
                {
                    Swap(i, finalPosition);
                    finalPosition++;
                }
            }
            Swap(finalPosition, right);
            return finalPosition;
        }

        // Return the index of the chosen pivot value. For QuickSort, this should always be "right".
        // For medianOf3QuickSort, this should be the index of the median value from the set of
        // the rightmost, leftmost and center values.
        public virtual int GetPivotBetween(int left, int right)
        {
            return right;
        }

    }
}
