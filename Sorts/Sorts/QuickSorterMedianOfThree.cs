﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorts
{
    class QuickSorterMedianOfThree<T>: Quicksorter<T> where T : IComparable<T>
    {


        public QuickSorterMedianOfThree(T[] array)
            : base(array)
        {

        }

        /// <summary>
        /// Finds the middle value given right and left, returns the closest index from the array
        /// </summary>
        /// <param name="left">First item in the array</param>
        /// <param name="right">Last item in the array</param>
        /// <returns></returns>
        public override int GetPivotBetween(int left, int right)
        {
            // get the midpoint
            int midPoint = (left + right) / 2;

            // store an index
            int finalIndex = midPoint;

            // if the first item is greater than middle item
            if (array[left].CompareTo(array[midPoint]) > 0)
            {
                finalIndex = left;
            }


            // if the first item is greater than the last
            if (array[left].CompareTo(array[right]) > 0)
            {
                finalIndex = left;
            }

            // if the middle item is greater than the last item
            if (array[midPoint].CompareTo(array[right]) > 0 )
            {
                finalIndex = right;
            }

            return finalIndex;
        }
    }
}

