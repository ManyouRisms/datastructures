﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorts
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter number of elements to sort: ");
            //Read input from user
            String input = Console.ReadLine();
            int arraySize = Int32.Parse(input);
            int[] array = new int[arraySize];
            //fill the array with random values
            Random r = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = r.Next(array.Length * 100);
            }

            //AbstractSorter<int> bs = new BubbleSorter<int>(array);
            //AbstractSorter<int> isorter = new InsertionSorter<int>(array);

            AbstractSorter<int> isorter = new BubbleSorter<int>(array);

            //Console.WriteLine(Math.Log(8, 2));
            TestSorter(isorter);

            
        }

        private static void TestSorter(AbstractSorter<int> sorter)
        {
            Console.WriteLine(sorter.GetType().Name + " with "
                + sorter.Length + " elements.");
            if (sorter.Length <= 50)
            {
                Console.WriteLine("Before sort: \n" + sorter);
            }

            //Determine the time it takes to sort
            long startTime = Environment.TickCount;
            sorter.Sort();
            long endTime = Environment.TickCount;

            if (sorter.Length <= 50)
            {
                Console.WriteLine("After sort: \n" + sorter);
            }

            //Print the time to do the sort
            Console.WriteLine("Sort took " + (endTime - startTime) + " milliseconds");
        }
    }
}
