﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorts
{
    class SelectionSort<T> : AbstractSorter<T> where T : IComparable<T>
    {
        public SelectionSort(T[] array)
            : base(array)
        {

        }

        public override void Sort()
        {
            // split two parts, sorted and unsorted
            // sorted starts empty
            // for each position in the list (this current position becomes the last item in the sorted list
                // look through the unsorted portion, finding the smallest item
                // once there are no more unsorted items to inspect, swap the smallest item in the list with the item in the current position
                // move to the next position
            // when there are no more unsorted, we're done

            for (int indexOfSorted=1; indexOfSorted < this.Length; indexOfSorted++)
            {
                int indexOfSmallestItem = indexOfSorted;
                for (int currentUnsortedItemIndex = indexOfSmallestItem; currentUnsortedItemIndex < this.Length; currentUnsortedItemIndex++)
                {
                    if (this.array[currentUnsortedItemIndex].CompareTo(this.array[indexOfSmallestItem]) < 0)
                    {
                        indexOfSmallestItem = currentUnsortedItemIndex;
                    }

                }
                Swap(indexOfSmallestItem, indexOfSorted);
            }
        }



        //public override void Sort()
        //{
        //    T smallest;
        //    for (int outer = 0; outer < this.array.Length; outer++)
        //    {
        //        smallest = this.array[outer];
        //        for (int inner = 0; inner < this.array.Length; inner++)
        //        {
        //            if (this.array[inner].CompareTo(smallest) < 0)
        //            {
        //                smallest = this.array[inner];
        //            }
        //        }
        //        this.array[outer] = smallest;
        //    }
        //}
    }
}
