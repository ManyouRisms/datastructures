﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{
    public class UndirectedAdjacencyListGraph<T> : AbstractAdjacencyListGraph<T> where T : IComparable<T>
    {

        public override void AddEdge(T from, T to, double weight)
        {
            base.AddEdge(from, to, weight);
            base.AddEdge(to, from, weight);
        }

        public override IEnumerable<Edge<T>> EnumerateEdges()
        {
            List<Vertex<T>> alreadyIncludedVertices = new List<Vertex<T>>();

            List<Edge<T>> uniqueEdges = new List<Edge<T>>();

            Vertex<T> currentFromVertex = null;

            //Inspect each edge in the graph
            foreach(List<Edge<T>> currentAdjacencyList in this.adjacencies)
            {
                //In the outer loop, we're iterating through the list of adjacency lists. Within
                 //this loop, currentAdjacencyList is the list of outbound edges from a single vertex
                 //to each of its neighbors. Because of this, all of the edges within currentAdjacencyList
                 //will have the same Vertex in their "From" property.

                foreach(Edge<T> currentEdge in currentAdjacencyList)
                {
                    //In the inner loop, we're iterating over the list of outbound edges from the current
                    //vertex. Since we're in an undirected graph, we want the set of unique edges (ie one
                    //half of each edge pair, since we add mirrored edges in an undirected graph). While there
                    //are many ways to accomplish this, a simple way is to simply keep track of the "From" vertices
                    //we've already enumerated the edges for. If we ever see an edge that goes "To" a vertex we've
                    //already enumerated the outbound edges for, we know it's a duplicate edge.

                    currentFromVertex = currentEdge.From;
                    if (alreadyIncludedVertices.Contains(currentEdge.To))
                    {
                        uniqueEdges.Add(currentEdge);
                    }
                    
                }

                //If there are no edges coming "from" this vertex, then by definition there will be no edges to it,
                //since this is an undirected graph. In the case where there is at least one edge exiting this vertex,
                //we need to keep track of the fact that we've enumerated all edges from this vertex so that we don't include
                //any of those going "to" it in the future.
                if (currentFromVertex != null)
                {
                    alreadyIncludedVertices.Add(currentFromVertex);
                }
                currentFromVertex = null;
            }
            return uniqueEdges;
        }


        protected override string GetDOTType()
        {
            return "graph";
        }

        protected override string FormatEdgeAsDOT(Edge<T> edge)
        {
            string fromID = "v_" + edge.From.GetHashCode();
            string toID = "v_" + edge.To.GetHashCode();
            return fromID + " -- " + toID + "[label=" + edge.Weight + "]\n";
        }
    }
}
