﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{
    public abstract class AbstractAdjacencyListGraph<T> : AbstractGraph<T> where T : IComparable<T>
    {

        /// <summary>
        /// adjacencies is a list of lists of edges.
        /// Each inner list contains the set of edges "leaving" a given vertex.
        /// By inspecting the "To" attribute of each edge in a given list, we can
        /// discover the neighbors of a given vertex. The "from" attribute of each
        /// Edge in a given list should be identical.
        /// </summary>
        protected List<List<Edge<T>>> adjacencies;


        public AbstractAdjacencyListGraph()
        {
            this.adjacencies = new List<List<Edge<T>>>();
        }


        protected override void AddVertexAdjustEdges(Vertex<T> vertex)
        {
            //Keep track of where we'll find adjacencies in "this.adjacencies" for the new vertex
            this.vertexIndices.Add(vertex.Data, this.vertices.Count - 1);

            //Make a space in the adjacencies list for edges coming from the new vertex
            this.adjacencies.Add(new List<Edge<T>>());

        }

        public override void RemoveVertexFor(T data)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Vertex<T>> EnumerateNeighborsOfVertexFor(T data)
        {
            List<Vertex<T>> neighbors = new List<Vertex<T>>();

            //Where to look in this.adjacencies for the list of edges comign from this vertex
            int adjacencyListIndex = this.vertexIndices[data];

            //Get the list of outbound edges from this vertex. Each edge points to one of the
            //neighbors of this vertex.
            List<Edge<T>> adjacentEdges = this.adjacencies[adjacencyListIndex];
            foreach(Edge<T> currentEdge in adjacentEdges)
            {
                //Each edge in the adjacency list should have the vertex for the method parameter "data" as its "from" property,
                // since this list contains all edges coming "From" the given vertex. The neighbors are the "To" property of each edge.
                neighbors.Add(currentEdge.To);
            }

            return neighbors;
        }

        protected override void AddEdge(Edge<T> newEdge)
        {
            //Each edge is tracked in the list of edges "outbound" or coming from the "from" side of the edge.
            // this results in every edge in the adjacency list having the same "from"

            int adjacencyListIndex = this.vertexIndices[newEdge.From.Data];

            //Each item in adjacencies is already pre-loaded with at least an empty List<Edge<T>>
            this.adjacencies[adjacencyListIndex].Add(newEdge);
        }

        public override bool HasEdge(T from, T to)
        {
            //An edge from x to y will be in the adjacency list for x, since
            // that list stores the edges exiting x, to the vertices adjacent to x
            int fromAdjacencyListIndex = this.vertexIndices[from];

            List<Edge<T>> neighbors = this.adjacencies[fromAdjacencyListIndex];

            foreach(Edge<T> currentEdge in neighbors)
            {
                //Compare "to" to the data for the current edge using compareTo, since
                // we're guaranteed that T is comparable
                if(currentEdge.To.Data.CompareTo(to) == 0)
                {
                    //If the current edge goes from "from" to "to", then we've found the edge we're looking for.
                    return true;
                }
            }
            return false;
        }

        public override Edge<T> GetEdge(T from, T to)
        {
            //An edge from x to y will be in the adjacency list for x, since
            // that list stores the edges exiting x, to the vertices adjacent to x
            int fromAdjacencyListIndex = this.vertexIndices[from];

            List<Edge<T>> neighbors = this.adjacencies[fromAdjacencyListIndex];

            //Checking for the presence of an edge between two vertices is more work in an adjacency list, since
            // we can't index directly to the edge as we could in adjacency matrix. We have to check each adjacent
            // edge to see if it's the one we were looking for, in this implementation.
            foreach (Edge<T> currentEdge in neighbors)
            {
                //Compare "to" to the data for the current edge using compareTo, since
                // we're guaranteed that T is comparable
                if (currentEdge.To.Data.CompareTo(to) == 0)
                {
                    //If the current edge goes from "from" to "to", then we've found the edge we're looking for.
                    return currentEdge;
                }
            }
            
            //If we get to this point, we did not find an edge between the two vertices. We could also 
            // have called "HasEdge" at the beginning of this method instead, just as we did in AdjacencyMatrixGraph
            throw new ApplicationException("No edge exists between " + from.ToString() + " and " + to.ToString());
        }


        public override void RemoveEdge(T from, T to)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Vertices:\n");
            foreach (Vertex<T> v in this.vertices)
            {
                sb.Append(this.vertexIndices[v.Data] + ": " + v.ToString() + "\n");
            }

            sb.Append("\n\nAdjacencies:\n");

            foreach(KeyValuePair<T,int> currentVertexIndex in this.vertexIndices)
            {
                sb.Append(currentVertexIndex.Key.ToString() + "=>[");
                sb.Append(string.Join(", ", this.adjacencies[currentVertexIndex.Value]));
                sb.Append("]\n");
            }
            return sb.ToString() + "\n";
        }
        
    }
}
