﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{
    public class DirectedAdjacencyMatrixGraph<T> : AbstractAdjacencyMatrixGraph<T> where T : IComparable<T>
    {

        public override IEnumerable<Edge<T>> EnumerateEdges()
        {
            //Returns a list of all edges. Since this is a directed graph, we return both edges v1->v2 and v2->v1
            //In other words, include every non-null edge from the edgeMatrix
            List<Edge<T>> edges = new List<Edge<T>>();

            for(int from = 0; from < this.edgeMatrix.GetLength(0); from++)
            {
                for (int to = 0; to < this.edgeMatrix.GetLength(1); to++)
                {
                    Edge<T> currentEdge = edgeMatrix[from,to];
                    if(currentEdge != null)
                    {
                        edges.Add(currentEdge);
                    }
                }
            }
            return edges;
        }


        protected override string GetDOTType()
        {
            return "digraph";
        }

        protected override string FormatEdgeAsDOT(Edge<T> edge)
        {
            string fromID = "v_" + edge.From.GetHashCode();
            string toID = "v_" + edge.To.GetHashCode();
            return fromID + " -> " + toID + "[label=" + edge.Weight + "]\n";
        }
    }
}
