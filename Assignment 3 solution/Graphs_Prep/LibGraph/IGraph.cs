﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{
    public delegate void VisitorDelegate<T>(T data);

    public interface IGraph<T> where T : IComparable<T>
    {
        

        #region Vertex related methods

        void AddVertexFor(T data);

        bool HasVertexFor(T data);

        Vertex<T> GetVertexFor(T data);

        void RemoveVertexFor(T data);

        IEnumerable<Vertex<T>> EnumerateVertices();

        IEnumerable<Vertex<T>> EnumerateNeighborsOfVertexFor(T data);
        #endregion



        #region Edge related methods

        void AddEdge(T from, T to); //Consistency is key

        void AddEdge(T from, T to, double weight);

        bool HasEdge(T from, T to);

        Edge<T> GetEdge(T from, T to);

        IEnumerable<Edge<T>> EnumerateEdges();

        void RemoveEdge(T from, T to);
        #endregion

        #region Traversals

        void DepthFirstTraversal(T whereToStart, VisitorDelegate<T> whatToDo);

        void BreadthFirstTraversal(T whereToStart, VisitorDelegate<T> whatToDo);
        #endregion

        #region Graph Operations

        IGraph<T> ShortestWeightedPath(T start, T end);

        IGraph<T> MinimumSpanningTree();
        #endregion

    }
}
