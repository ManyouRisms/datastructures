﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{



    public class UndirectedAdjacencyMatrixGraph<T> : AbstractAdjacencyMatrixGraph<T> where T : IComparable<T>
    {

        public override void AddEdge(T from, T to, double weight)
        {
            base.AddEdge(from, to, weight);
            base.AddEdge(to, from, weight);
        }

        public override IEnumerable<Edge<T>> EnumerateEdges()
        {
            //Since this is an undirected graph, we only need to include edges in one direction (ie one half of the diagonal)
            List<Edge<T>> edges = new List<Edge<T>>();

            for (int from = 0; from < this.edgeMatrix.GetLength(0); from++)
            {
                for (int to = from+1; to < this.edgeMatrix.GetLength(1); to++)
                {
                    Edge<T> currentEdge = edgeMatrix[from, to];
                    if (currentEdge != null)
                    {
                        edges.Add(currentEdge);
                    }
                }
            }
            return edges;
        }

        
        protected override string GetDOTType()
        {
            return "graph";
        }

        protected override string FormatEdgeAsDOT(Edge<T> edge)
        {
            string fromID = "v_" + edge.From.GetHashCode();
            string toID = "v_" + edge.To.GetHashCode();
            return fromID + " -- " + toID + "[label=" + edge.Weight + "]\n";
        }
    }
}
