﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{
    public class Vertex<T> : IComparable<Vertex<T>> where T : IComparable<T>
    {
        private T data;

        public Vertex(T data)
        {
            this.Data = data;
        }

        public T Data
        {
            get{
                return this.data;
            }
            set
            {
                this.data = value;
            }
        }

        public int CompareTo(Vertex<T> other)
        {
            return this.Data.CompareTo(other.Data);
        }

        public override string ToString()
        {
            return this.data.ToString();
        }
    }
}
