﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//http://www.webgraphviz.com/
namespace LibGraph
{
    public abstract class AbstractAdjacencyMatrixGraph<T> : AbstractGraph<T> where T : IComparable<T>
    {




        //A square, multidimensional array which stores edges between nodes. 
        //From in dimension 0, To in dimension 1
        protected Edge<T>[,] edgeMatrix;


        public AbstractAdjacencyMatrixGraph()
        {
            this.edgeMatrix = new Edge<T>[0, 0];
        }



        /// <summary>
        /// Set up our Edge storage to be able to store edges for the new Vertex
        /// </summary>
        /// <param name="vertex"></param>
        protected override void AddVertexAdjustEdges(Vertex<T> vertex)
        {
            Edge<T>[,] oldMatrix = this.edgeMatrix;

            //We need to expand the edgeMatrix in both dimensions so 
            // that it'll have enough room for all of our vertices
            // We need one "row" and one "column" per vertex
            Edge<T>[,] newMatrix = new Edge<T>[this.vertices.Count, this.vertices.Count];

            //Now copy the old edges to the new matrix
            for(int from = 0; from < oldMatrix.GetLength(0); from++)
            {
                for(int to = 0; to < oldMatrix.GetLength(1); to++)
                {
                    newMatrix[from, to] = oldMatrix[from, to];
                }
            }

            this.edgeMatrix = newMatrix;

            //New vertices go at the "end" of the edgeMatrix array in each dimension
            // Add a mapping between the vertex T and its position in the edgeMatrix 
            // and vertices list
            this.vertexIndices.Add(vertex.Data, this.vertices.Count - 1);

        }



        public override void RemoveVertexFor(T data)
        {
            int indexToRemove = this.vertexIndices[data];
            Edge<T>[,] newMatrix = new Edge<T>[this.vertices.Count-1, this.vertices.Count-1];
            int newI = 0;
            int newJ = 0;
            if (this.HasVertexFor(data))
            {

                for (int i = 0; i < this.edgeMatrix.GetLength(0); i++)
                {
                    newJ = 0;
                    for (int j = 0; j < this.edgeMatrix.GetLength(1); j++)
                    {
                        if (j != indexToRemove && i != indexToRemove)
                        {
                            if (this.edgeMatrix[i, j]!= null)
                            {
                                newMatrix[newI, newJ] = this.edgeMatrix[i, j];
                                newJ++;

                            }
                        }
                    }
                    if (i != indexToRemove)
                    {
                        newI++;
                    }
                }
                this.edgeMatrix = newMatrix;
                this.vertices.RemoveAt(indexToRemove);
            }
            else
            {
                throw new ApplicationException("No vertex found");

            }



            //We'll implement this if we get time. Keep in mind that removing a vertex
            // will require an update to the edgeMatrix table and vertexIndices may change


            //This might be a good exam question? Given an adjacency list graph implementation with the following fields,
            // implement the remove method
        }



        /// <summary>
        /// Returns an enumerable list of all of the neighbors of the vertex which
        ///   represents the given data item
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override IEnumerable<Vertex<T>> EnumerateNeighborsOfVertexFor(T data)
        {
            if(!this.HasVertexFor(data))
            {
                throw new ApplicationException("No vertex for data "
                    + data.ToString());
            }
            int fromIndex = this.vertexIndices[data];

            List<Vertex<T>> neighbors = new List<Vertex<T>>();
            for (int currentToIndex = 0; currentToIndex < this.edgeMatrix.GetLength(1); currentToIndex++)
            {
                Edge<T> currentEdge = this.edgeMatrix[fromIndex, currentToIndex];
                if(currentEdge != null)
                {
                    //Remember that neighbors is the list of vertices that this 
                    // vertex is connected to
                    neighbors.Add(currentEdge.To);
                }
            }
            return neighbors;
        }

 



        /// <summary>
        /// Save the given edge to our edge storage system
        /// </summary>
        /// <param name="newEdge"></param>
        protected override void AddEdge(Edge<T> newEdge)
        {
            int fromIndex = this.vertexIndices[newEdge.From.Data];
            int toIndex = this.vertexIndices[newEdge.To.Data];
            
            edgeMatrix[fromIndex, toIndex] = newEdge;
        }

        /// <summary>
        /// Return true if there is an edge connecting the vertices
        /// which represent the given data items, false otherwise
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public override bool HasEdge(T from, T to)
        {
            int fromIndex = this.vertexIndices[from];
            int toIndex = this.vertexIndices[to];

            return this.edgeMatrix[fromIndex, toIndex] != null;
            
        }

        /// <summary>
        /// Get the Edge object which represents the connection
        /// between the Vertices for the given data items
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public override Edge<T> GetEdge(T from, T to)
        {
            if(!HasEdge(from, to))
            {
                throw new ApplicationException("No edge exists between " + from.ToString() + " and " + to.ToString());
            }
            int fromIndex = this.vertexIndices[from];
            int toIndex = this.vertexIndices[to];

            return this.edgeMatrix[fromIndex, toIndex];
        }

        

        public override void RemoveEdge(T from, T to) //We'll do this if we have time
        {
            throw new NotImplementedException();
        }

        

        

        #region display convenience

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Vertices:\n");
            foreach(Vertex<T> v in this.vertices)
            {
                sb.Append(this.vertexIndices[v.Data] + ": " + v.ToString() + "\n");
            }




            sb.Append("\n\nEdge matrix (from in rows, to in columns)\n\t");
            for (int i = 0; i < this.edgeMatrix.GetLength(1); i++)
            {
                sb.Append(i + ":\t");
            }
            sb.Append("\n");
            for (int from = 0; from < this.edgeMatrix.GetLength(0); from++)
            {
                sb.Append(from + ":\t");
                for (int to = 0; to < this.edgeMatrix.GetLength(1); to++)
                {
                    Edge<T> currentEdge = this.edgeMatrix[from, to];
                    if (currentEdge != null)
                    {
                        sb.Append("(" + currentEdge.From + "," + currentEdge.To);
                        if (currentEdge.Weight != Double.PositiveInfinity)
                        {
                            sb.Append("," + currentEdge.Weight);
                        }
                        sb.Append(")\t");
                    }
                    else
                    {
                        sb.Append("\t");
                    }
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }

        

        /// <summary>
        /// Generate DOT to display the graph, highlighting the important sections which
        /// apply while performing a traversal
        /// </summary>
        /// <param name="currentlyVisiting">The node we're currently visiting</param>
        /// <param name="currentlyInspecting">The neighbor of current we're inspecting</param>
        /// <param name="visitedVertices">The list of all previously visited vertices</param>
        /// <param name="toVisit">The list of neighbors of previously visited vertices</param>
        /// <returns></returns>
        public string ToTraversalDOT(Vertex<T> currentlyVisiting, Vertex<T> currentlyInspecting, List<Vertex<T>> visitedVertices, List<Vertex<T>> toVisit)
        {
            IEnumerable<Edge<T>> edges = this.EnumerateEdges();
            IEnumerable<Vertex<T>> vertices = this.EnumerateVertices();
            StringBuilder builder = new StringBuilder();
            builder.Append(this.GetDOTType() + " cstgraph { \n");

            foreach (Vertex<T> currentVertex in vertices)
            {
                builder.Append("v_" + currentVertex.GetHashCode() + " [label=\"" + currentVertex.ToString());
                List<string> fillColours = new List<string>();
                if (currentVertex.CompareTo(currentlyVisiting) == 0)
                {
                    fillColours.Add("yellow");
                    //builder.Append(" color=yellow style=filled");
                }
                if(visitedVertices.Contains(currentVertex))
                {
                    fillColours.Add("red");
                    //builder.Append(" color=red style=filled");
                }
                if(toVisit.Contains(currentVertex))
                {
                    builder.Append(":" + toVisit.IndexOf(currentVertex));
                    fillColours.Add("green");
                    //builder.Append(" color=green style=filled");
                }
                if (currentlyInspecting != null && currentVertex.CompareTo(currentlyInspecting) == 0)
                {
                    if(fillColours.Count < 1)
                    {
                        fillColours.Add("white");
                    }
                    fillColours.Add("blue");
                    //builder.Append(" color=yellow style=filled");
                }
                if(fillColours.Count < 1)
                {
                    fillColours.Add("white");
                }
                builder.Append("\" fillcolor=\"" + string.Join(":", fillColours.ToArray()) + "\" style=\"filled\" gradientangle=\"0\"");
                
                builder.Append("]\n");
            }

            foreach (Edge<T> currentEdge in edges)
            {
                builder.Append(this.FormatEdgeAsDOT(currentEdge));
            }
            builder.Append("}");
            return builder.ToString();

        }
        #endregion
    }
}