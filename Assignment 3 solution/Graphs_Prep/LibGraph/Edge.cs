﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{


    public class Edge<T> : IComparable<Edge<T>> where T : IComparable<T>
    {
        private Vertex<T> to;
        private Vertex<T> from;
        private double weight;

        public Vertex<T> To
        {
            get
            {
                return this.to;
            }
        }

        public Vertex<T> From
        {
            get
            {
                return this.from;
            }
        }

        public double Weight
        {
            get
            {
                return this.weight;
            }
        }

        public Edge(Vertex<T> from, Vertex<T> to): this(from, to, Double.PositiveInfinity)
        {
        }

        public Edge(Vertex<T> from, Vertex<T> to, double weight)
        {
            this.to = to;
            this.from = from;
            this.weight = weight;
        }

        public override string ToString()
        {
            string toString =  "("+this.From+"," + this.To;
            if(!Double.IsPositiveInfinity(this.Weight))
            {
                toString += "," + this.Weight;
            }
            return toString + ")";
        }

        public int CompareTo(Edge<T> other)
        {
            int value = this.Weight.CompareTo(other.Weight);
            if(value == 0)
            {
                value = this.From.CompareTo(other.From);
                if(value == 0)
                {
                    value = this.To.CompareTo(other.To);
                }
            }
            return value;
        }
    }
}
