﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{
    public abstract class AbstractGraph<T> : IGraph<T> where T : IComparable<T>
    {
        //A simple list of all vertices which are part of this graph
        protected List<Vertex<T>> vertices;


        //A lookup table to allow us to find the index into edge storage at 
        //which a given T's edges are stored
        protected Dictionary<T, int> vertexIndices;

        public AbstractGraph()
        {
            this.vertexIndices = new Dictionary<T, int>();
            this.vertices = new List<Vertex<T>>();
        }

        /// <summary>
        /// Creates a new vertex in the graph to represent the given data.
        /// </summary>
        /// <param name="data">The data to represent in the graph</param>
        public void AddVertexFor(T data)
        {
            if (this.HasVertexFor(data))
            {
                throw new ApplicationException("Vertex for data "
                    + data.ToString() + " already exists");
            }
            Vertex<T> newVertex = new Vertex<T>(data);

            this.vertices.Add(newVertex);

            this.AddVertexAdjustEdges(newVertex);
        }

        protected abstract void AddVertexAdjustEdges(Vertex<T> vertex);


        /// <summary>
        /// Check whether or not we have a Vertex in the graph to represent the given data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool HasVertexFor(T data)
        {
            //Whenever a new Vertex is added, we're already adding a mapping
            // in vertexIndices betwee the data and the index at which its 
            // edges are located in our edge matrix. Therefore, if vertexIndices
            // does not have a key for the given data then that data has no vertex
            // in this graph
            return this.vertexIndices.ContainsKey(data);
        }

        /// <summary>
        /// Finds and returns the vertex which represents the given data item
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Vertex<T> GetVertexFor(T data)
        {
            //If there is no vertex representing this data in the graph, then
            // throw an exception
            if (!this.HasVertexFor(data))
            {
                throw new ApplicationException("No vertex for data "
                    + data.ToString());
            }
            //Use the vertexIndices dict to find the index at which the vertex for
            // the given data is stored, then fetch and return it
            int index = this.vertexIndices[data];
            return vertices[index];
        }


        public abstract void RemoveVertexFor(T data);

        /// <summary>
        /// Fetch an enumerable form of our list of vertices. List<> objects are already
        /// enumerable
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Vertex<T>> EnumerateVertices()
        {
            //Maybe for next year this should only be in the abstract
            // so that we don't end up exposing Vertex to the outside world
            return this.vertices;
        }

        public abstract IEnumerable<Vertex<T>> EnumerateNeighborsOfVertexFor(T data);

        /// <summary>
        /// Adds an unweighted edge between the vertexes representing "to" and
        /// "from". Unweighted edges have a weight of positive infinity
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public void AddEdge(T from, T to) //AddEdgeBetweenVerticesFor
        {
            this.AddEdge(from, to, Double.PositiveInfinity);
        }

        /// <summary>
        /// Constructs and adds a weighted edge between the 
        /// vertices representing "to" and "from", with the given weight.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="weight"></param>
        public virtual void AddEdge(T from, T to, double weight)
        {
            if (this.HasEdge(from, to))
            {
                throw new ApplicationException("Edge between " + from.ToString()
                    + " and " + to.ToString() + " already exists");
            }
            //Find the vertexes which represent each of the given data items
            Vertex<T> fromVertex = this.GetVertexFor(from);
            Vertex<T> toVertex = this.GetVertexFor(to);

            Edge<T> newEdge = new Edge<T>(fromVertex, toVertex, weight);
            this.AddEdge(newEdge);
        }

        /// <summary>
        /// Implementation specific way to add an edge to the graph
        /// </summary>
        /// <param name="newEdge">The newly-created edge object to add</param>
        protected abstract void AddEdge(Edge<T> newEdge);

        public abstract bool HasEdge(T from, T to);

        public abstract Edge<T> GetEdge(T from, T to);

        public abstract IEnumerable<Edge<T>> EnumerateEdges();

        public abstract void RemoveEdge(T from, T to);


        #region Graph operations
        public void DepthFirstTraversal(T whereToStart, VisitorDelegate<T> whatToDo)
        {
            Vertex<T> startPoint = this.GetVertexFor(whereToStart);
            Stack<Vertex<T>> toProcess = new Stack<Vertex<T>>();
            List<Vertex<T>> visitedVertices = new List<Vertex<T>>();

            toProcess.Push(startPoint);

            while (toProcess.Count > 0)
            {
                Vertex<T> current = toProcess.Pop();

                //File.WriteAllText("X:\\graph.txt", this.ToTraversalDOT(current, null, visitedVertices, toProcess.ToList()));
                //;
                if (!visitedVertices.Contains(current))
                {
                    whatToDo(current.Data);
                    visitedVertices.Add(current);
                    foreach (Vertex<T> neighbor in this.EnumerateNeighborsOfVertexFor(current.Data))
                    {
                        //File.WriteAllText("X:\\graph.txt", this.ToTraversalDOT(current, neighbor, visitedVertices, toProcess.ToList()));
                        //;
                        if (!visitedVertices.Contains(neighbor) && !toProcess.Contains(neighbor))
                        {
                            toProcess.Push(neighbor);
                        }
                    }
                    //File.WriteAllText("X:\\graph.txt", this.ToTraversalDOT(current, null, visitedVertices, toProcess.ToList()));
                    //;
                }

            }


        }

        public void BreadthFirstTraversal(T whereToStart, VisitorDelegate<T> whatToDo)
        {
            Vertex<T> startPoint = this.GetVertexFor(whereToStart);
            Queue<Vertex<T>> toProcess = new Queue<Vertex<T>>();
            List<Vertex<T>> visitedVertices = new List<Vertex<T>>();

            toProcess.Enqueue(startPoint);

            while (toProcess.Count > 0)
            {

                Vertex<T> current = toProcess.Dequeue();

                if (!visitedVertices.Contains(current))
                {
                    whatToDo(current.Data);
                    visitedVertices.Add(current);
                    foreach (Vertex<T> neighbor in this.EnumerateNeighborsOfVertexFor(current.Data))
                    {

                        if (!visitedVertices.Contains(neighbor) && !toProcess.Contains(neighbor))
                        {
                            toProcess.Enqueue(neighbor);
                        }


                    }

                }

            }
        }


        protected internal class VertexData : IComparable<VertexData>
        {
            public double tentative_distance;
            public Vertex<T> vertex;
            public bool visited;
            public VertexData previous;

            public VertexData(Vertex<T> vertex)
                : this(vertex, Double.PositiveInfinity)
            {

            }

            public VertexData(Vertex<T> vertex, double tentative_distance)
            {
                this.tentative_distance = tentative_distance;
                this.vertex = vertex;
                this.visited = false;
                this.previous = null;
            }

            public int CompareTo(VertexData other)
            {
                return this.tentative_distance.CompareTo(other.tentative_distance);
            }
        }

        public IGraph<T> ShortestWeightedPath(T start, T end)
        {
            Vertex<T> startVertex = this.GetVertexFor(start);
            Vertex<T> endVertex = this.GetVertexFor(end);

            IEnumerable<Vertex<T>> vertices = this.EnumerateVertices();

            //Do this part later
            //We need a way to get our "decorated" vertexData object for a given vertex
            Dictionary<Vertex<T>, VertexData> vertexMap = new Dictionary<Vertex<T>, VertexData>();

            IPriorityQueue<VertexData> tdQueue = new SimplePriorityQueue<VertexData>();

            //For each vertex in the graph, keep track of some extra data
            foreach (Vertex<T> currentVertex in vertices)
            {
                VertexData vertexWrapper;
                if (currentVertex.Data.Equals(start))
                {
                    //The start vertex has a distance of 0
                    vertexWrapper = new VertexData(currentVertex, 0);
                }
                else
                {
                    //Every other vertex has a distance of infinity
                    vertexWrapper = new VertexData(currentVertex, Double.PositiveInfinity);
                }
                vertexMap.Add(currentVertex, vertexWrapper);
                tdQueue.Enqueue(vertexWrapper);
            }


            //While there are more vertices to process
            //Will the queue ever be empty without finding either the target vertex or that the target can't be reached?
            //If the target can't be reached, we'll always pull it from the vertex list with a tentative distance of positive infinity
            //If it doesn't exist, we won't have reached this point anyways.
            while (!tdQueue.IsEmpty())
            {
                //Get the next lowest tentative distance vertex from the priority queue
                VertexData vDataCurrent = tdQueue.Dequeue();

                //Mark the vertex as visited/complete
                vDataCurrent.visited = true;

                //Check our end conditions.
                //If this vertex has a TD of positive infinity, then it is unreachable. Since we always process
                //Lower TD vertices first, we're guaranteed that every vertex from here on is unreachable from the start point,
                //therefore there is no path to the finish.
                if (Double.IsPositiveInfinity(vDataCurrent.tentative_distance))
                {
                    throw new ApplicationException("The target item is not reachable from the start point");
                }

                //If we reach the end vertex and it has a tentative distance, we've found a shortest path
                if (vDataCurrent.vertex == endVertex)
                {
                    //We're done. Construct a new graph and return it.
                    break;
                }

                IEnumerable<Vertex<T>> neighbors = this.EnumerateNeighborsOfVertexFor(vDataCurrent.vertex.Data);

                foreach (Vertex<T> currentNeighbor in neighbors)
                {
                    //For each neighbor of the current vertex, check the tentative distance and update the
                    //TD and pointer if there is a shorter path to that vertex through current.
                    VertexData vDataCurrentNeighbor = vertexMap[currentNeighbor];

                    if (!vDataCurrentNeighbor.visited)
                    {
                        //Keep in mind the difference between "current" and the current neighbor
                        Edge<T> edgeBetween = this.GetEdge(vDataCurrent.vertex.Data, vDataCurrentNeighbor.vertex.Data);
                        if (vDataCurrent.tentative_distance + edgeBetween.Weight < vDataCurrentNeighbor.tentative_distance)
                        {
                            vDataCurrentNeighbor.tentative_distance = vDataCurrent.tentative_distance + edgeBetween.Weight;
                            vDataCurrentNeighbor.previous = vDataCurrent;
                            //I've discovered an implementation flaw. Does anyone else see what it is?
                            //Move the sort to right before we remove something
                        }
                    }
                }
            }

            //Our vertexData collection, at this point, has a path marked from end to beginning which represents the shortest path
            // reconstruct a new graph defining this path.
            return ReconstructShortestPathGraph(vertexMap[endVertex]);
        }

        protected IGraph<T> ReconstructShortestPathGraph(VertexData endPoint)
        {
            IGraph<T> newGraph = (IGraph<T>)
                                GetType().Assembly.CreateInstance(GetType().FullName);

            //Add a vertex to represent the end point, so that there will always be at least one
            //involved vertex in the graph when we start looping
            newGraph.AddVertexFor(endPoint.vertex.Data);

            //Start the loop at the end point
            VertexData vDataCurrent = endPoint;

            //While the current vertex has a previous vertex
            while (vDataCurrent.previous != null)
            {
                //Get the VertexData object for the previous vertex
                VertexData vDataPrevious = vDataCurrent.previous;

                //Get the vertex objects for the current and previous vertices
                Vertex<T> previousVertex = vDataPrevious.vertex;
                Vertex<T> currentVertex = vDataCurrent.vertex;

                //Add a vertex for the "previous" vertex
                newGraph.AddVertexFor(previousVertex.Data);

                //Now that both the current and previous vertices exist in the new graph, we can add
                //an edge between them. Make sure to add the edge in the right direction, as the 
                //chain of previous pointers is actually the opposite of the path, and to add the
                //edge weights to match the existing edges.
                Edge<T> existingEdge = this.GetEdge(previousVertex.Data, currentVertex.Data);
                newGraph.AddEdge(previousVertex.Data, currentVertex.Data, existingEdge.Weight);

                //Move on to inspect the next vertex
                vDataCurrent = vDataPrevious;
            }
            return newGraph;
        }

        /// <summary>
        /// Fetch a graph representing a minimum spanning tree of the current graph.
        /// A minimum spanning tree is the minimum total weight set of edges required to connect
        /// all vertices of the original graph. The resulting graph has all of the vertices of the
        /// original graph, but a minimum subset of edges.
        /// </summary>
        /// <returns>A new IGraph representing the minimum spanning tree</returns>
        public IGraph<T> MinimumSpanningTree()
        {
            List<IGraph<T>> forest = new List<IGraph<T>>();
            IPriorityQueue<Edge<T>> edgeQueue = new SimplePriorityQueue<Edge<T>>();

            //Create a collection of graphs, each with a single vertex from this tree
            IEnumerable<Vertex<T>> allVertices = this.EnumerateVertices();

            foreach (Vertex<T> currentVertex in allVertices)
            {
                IGraph<T> newGraph = (IGraph<T>)
                                GetType().Assembly.CreateInstance(GetType().FullName);
                newGraph.AddVertexFor(currentVertex.Data);
                forest.Add(newGraph);
            }

            //Create a priority collection of edges, lowest coming first
            IEnumerable<Edge<T>> allEdges = this.EnumerateEdges();
            foreach (Edge<T> currentEdge in allEdges)
            {
                edgeQueue.Enqueue(currentEdge);
            }


            //While there are still edges and more than one tree remaining
            while (!edgeQueue.IsEmpty() && forest.Count > 1)
            {
                //Get the next lowest edge
                Edge<T> nextLowestEdge = edgeQueue.Dequeue();

                IGraph<T> fromTree = GetGraphContainingVertex(forest, nextLowestEdge.From);
                IGraph<T> toTree = GetGraphContainingVertex(forest, nextLowestEdge.To);

                //If the two sides of that edge are not in the same tree, then...
                //If the two sides of the current edge were in the same tree, then adding this edge
                // to the MST would create a cycle (since every vertex in each tree in forest is already connected), so
                // the edge should not be added.
                if (fromTree != toTree)
                {
                    //Merge graph containing "From" into the graph containing "To"
                    MergeGraphs(toTree, fromTree, nextLowestEdge);

                    //Remove the extra graph from the list
                    forest.Remove(toTree);
                }

            }

            if (forest.Count > 1)
            {
                throw new ApplicationException("Could not create minimum spanning tree");
            }
            return forest[0];
        }

        /// <summary>
        /// Returns the graph object from forest which contains the search vertex
        /// </summary>
        /// <param name="forest"></param>
        /// <param name="searchFor"></param>
        /// <returns></returns>
        private IGraph<T> GetGraphContainingVertex(List<IGraph<T>> forest, Vertex<T> searchFor)
        {
            foreach (IGraph<T> currentGraph in forest)
            {
                if (currentGraph.HasVertexFor(searchFor.Data))
                {
                    return currentGraph;
                }
            }
            throw new ApplicationException("No graph found for " + searchFor.Data);
        }

        /// <summary>
        /// Merges the graph source into the graph destination, connecting the two with the edge connectingEdge.
        /// 
        /// </summary>
        /// <param name="source">The graph to take edges and vertices from</param>
        /// <param name="destination">The graph to add edges and vertices to</param>
        /// <param name="connectingEdge">The edge to use to connect the two "sides" of the merged graph</param>
        private void MergeGraphs(IGraph<T> source, IGraph<T> destination, Edge<T> connectingEdge)
        {
            //Before we can add any edges, all of the new vertices need to be present
            foreach (Vertex<T> currentVertex in source.EnumerateVertices())
            {
                destination.AddVertexFor(currentVertex.Data);
            }

            //Add edges to dest to match those in source
            foreach (Edge<T> currentEdge in source.EnumerateEdges())
            {
                destination.AddEdge(currentEdge.From.Data, currentEdge.To.Data, currentEdge.Weight);
            }

            //Connect the two sides of the graph
            destination.AddEdge(connectingEdge.From.Data, connectingEdge.To.Data, connectingEdge.Weight);
        }
        #endregion


        protected abstract string GetDOTType();


        // DOT represents edges on directed and undirected graphs differently
        protected abstract string FormatEdgeAsDOT(Edge<T> edge);

        /// <summary>
        /// Format the graph using DOT syntax.
        /// DOT syntax consists of a list of vertex identifiers
        /// then a list of edges between those vertices
        /// http://en.wikipedia.org/wiki/DOT_(graph_description_language)
        /// http://www.webgraphviz.com/
        /// </summary>
        /// <returns></returns>
        public string ToDOT()
        {
            IEnumerable<Edge<T>> edges = this.EnumerateEdges();
            IEnumerable<Vertex<T>> vertices = this.EnumerateVertices();
            StringBuilder builder = new StringBuilder();
            builder.Append(this.GetDOTType() + " cstgraph { \n");

            foreach (Vertex<T> currentVertex in vertices)
            {
                builder.Append("v_" + currentVertex.GetHashCode() + " [label=\"" + currentVertex.ToString() + "\"]\n");
            }

            foreach (Edge<T> currentEdge in edges)
            {
                builder.Append(this.FormatEdgeAsDOT(currentEdge));
            }
            builder.Append("}");
            return builder.ToString();
        }

    }
}
