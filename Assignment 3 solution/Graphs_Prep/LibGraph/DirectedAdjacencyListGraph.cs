﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{
    public class DirectedAdjacencyListGraph<T> : AbstractAdjacencyListGraph<T> where T : IComparable<T>
    {
        public override IEnumerable<Edge<T>> EnumerateEdges()
        {
            List<Edge<T>> allEdges = new List<Edge<T>>();

            //In a directed graph, every edge is distinct. We simply need to collect up all the edge objects
            // from our adjacency lists (all of the adjacencies for all of the vertices), then return them.
            foreach(List<Edge<T>> currentAdjacencyList in this.adjacencies)
            {
                //AddRange adds every individual item from the passed in collection.
                // It's essentially a shortcut for having a second innner loop here to
                // add each edge individually from currentAdjacencyList to allEdges.
                allEdges.AddRange(currentAdjacencyList);
            }
            return allEdges;
        }

        protected override string GetDOTType()
        {
            return "digraph";
        }

        protected override string FormatEdgeAsDOT(Edge<T> edge)
        {
            string fromID = "v_" + edge.From.GetHashCode();
            string toID = "v_" + edge.To.GetHashCode();
            return fromID + " -> " + toID + "[label=" + edge.Weight + "]\n";
        }
    }
}
