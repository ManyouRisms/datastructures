﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGraph;
using System.IO;

namespace Graphs_Prep
{
    class Program
    {

        public static void PrintVertexValue(string data)
        {
            Console.WriteLine(data.ToString());
        }

        static void Main(string[] args)
        {
            //AddEdgeTestCode();

            //EnumerateEdgesTestCode();

            //EnumerateNeighborsOfVertexForTestCode();

            //GetEdgeTestCode();

            //GetVertexForTestCode();

            //HasVertexForTestCode();

            //EnumerateVerticesTestCode();

            //MSTTestCode();

            RemoveVertexMatrixTestCode();

            
 
        }

        public static void RemoveVertexMatrixTestCode()
        {
            AbstractAdjacencyMatrixGraph<string> graph = new UndirectedAdjacencyMatrixGraph<string>();
            graph.AddVertexFor("a");
            graph.AddVertexFor("b");
            graph.AddVertexFor("c");
            graph.AddVertexFor("d");
            graph.AddVertexFor("e");
            graph.AddVertexFor("f");
            graph.AddVertexFor("g");

            graph.AddEdge("a", "b");
            graph.AddEdge("a", "c");
            graph.AddEdge("a", "d");
            graph.AddEdge("b", "c");
            graph.AddEdge("d", "c");
            graph.AddEdge("c", "e");
            graph.AddEdge("f", "c");
            graph.AddEdge("d", "g");

            Console.WriteLine("Before removing vertex");
            Console.WriteLine(graph);

            Console.WriteLine("\n\n\nAfter removing vertex");
            graph.RemoveVertexFor("c");
            Console.WriteLine(graph);





        }



        public static void AddEdgeTestCode()
        {
            Console.WriteLine("===============\nAddEdgeTestCode\n==========\n\n\n");
            AbstractAdjacencyListGraph<string> testUGraph = new UndirectedAdjacencyListGraph<string>();
            testUGraph.AddVertexFor("A");
            testUGraph.AddVertexFor("B");
            testUGraph.AddVertexFor("C");

            Console.WriteLine(testUGraph.ToString());
            //At this point, testUGraph should have 3 vertices and no edges. adjacencies property should be a List of length
            // 3, with an empty List<Edge<T>> at each index. vertexIndices should be of length 3, with entries for A=>0, B=>1, C=>2

            testUGraph.AddEdge("A", "C", 2);
            //At this point, testUGraph should have 2 edges; one in the adjacency list for A (A,C,2) and one in the adjacency list for C (C,A,2).
            Console.WriteLine(testUGraph.ToString());


            testUGraph.AddEdge("A", "B", 5);
            //At this point, testUGraph should have 4 edges:
                //two in the adjacency list for A (A,C,2),(A,B,5)
                //one in the adjacency list for B (B,A,5)
                //one in the adjacency list for C (C,A,2).
            Console.WriteLine(testUGraph.ToString());



            AbstractAdjacencyListGraph<string> testDGraph = new DirectedAdjacencyListGraph<string>();
            testDGraph.AddVertexFor("A");
            testDGraph.AddVertexFor("B");
            testDGraph.AddVertexFor("C");
            Console.WriteLine(testDGraph.ToString());

            //At this point, testDGraph should have 3 vertices and no edges. adjacencies property should be a List of length
            // 3, with an empty List<Edge<T>> at each index. vertexIndices should be of length 3, with entries for A=>0, B=>1, C=>2

            testDGraph.AddEdge("A", "C", 2);
            //At this point, testDGraph should have 1 edge. In the adjacency list for A, there should be an edge (A,C,2).
            Console.WriteLine(testDGraph.ToString());


            testDGraph.AddEdge("B", "C", 5);
            //At this point, testUGraph should have 2 edges
                //One in the adjacency list for A (A,C,2)
                //One in the adjacency list for B (B,C,5)
            //There should be no edges in the adjacency list for C, since this is a directed graph and we have not added
                //any edges from C to another vertex.
            Console.WriteLine(testDGraph.ToString());

        }


        public static void GetEdgeTestCode()
        {
            Console.WriteLine("===============\nGetEdgeTestCode\n==========\n\n\n");

            AbstractAdjacencyListGraph<string> testUGraph = new UndirectedAdjacencyListGraph<string>();
            testUGraph.AddVertexFor("A");
            testUGraph.AddVertexFor("B");
            testUGraph.AddVertexFor("C");
            testUGraph.AddVertexFor("D");

            Console.WriteLine(testUGraph.ToString());

            //At this point, testUGraph should have 3 vertices and no edges. adjacencies property should be a List of length
            // 3, with an empty List<Edge<T>> at each index. vertexIndices should be of length 3, with entries for A=>0, B=>1, C=>2

            testUGraph.AddEdge("A", "C", 2);
            testUGraph.AddEdge("A", "B", 5);
            testUGraph.AddEdge("C", "B", 4);
            testUGraph.AddEdge("D", "A", 1);

            //GetEdge should return the edge between C and A, since that edge does exist in an undirected graph
            Console.WriteLine(testUGraph.GetEdge("C", "A"));
            Console.WriteLine(testUGraph.GetEdge("A", "C"));


            AbstractAdjacencyListGraph<string> testDGraph = new DirectedAdjacencyListGraph<string>();
            testDGraph.AddVertexFor("A");
            testDGraph.AddVertexFor("B");
            testDGraph.AddVertexFor("C");
            testDGraph.AddVertexFor("D");

            Console.WriteLine(testDGraph.ToString());

            //At this point, testUGraph should have 3 vertices and no edges. adjacencies property should be a List of length
            // 3, with an empty List<Edge<T>> at each index. vertexIndices should be of length 3, with entries for A=>0, B=>1, C=>2

            testDGraph.AddEdge("A", "C", 2);
            testDGraph.AddEdge("A", "B", 5);
            testDGraph.AddEdge("C", "B", 4);
            testDGraph.AddEdge("D", "A", 1);

            //Should be able to get an edge from A to C, but not from C to A (since this is a directed graph and we didn't add the opposite edge)
            Console.WriteLine(testDGraph.GetEdge("A", "C"));
            try
            {
                testDGraph.GetEdge("C", "A");
            }
            catch(ApplicationException e)
            {
                Console.WriteLine("No edge exists from C to A, as it should be");
            }



        }

        public static void GetVertexForTestCode()
        {
            Console.WriteLine("===============\nGetVertexForTestCode\n==========\n\n\n");

            AbstractAdjacencyListGraph<string> testUGraph = new UndirectedAdjacencyListGraph<string>();
            testUGraph.AddVertexFor("A");
            testUGraph.AddVertexFor("B");
            testUGraph.AddVertexFor("C");

            Console.WriteLine(testUGraph.GetVertexFor("A") + " is vertex for A");
            Console.WriteLine(testUGraph.GetVertexFor("C") + " is vertex for C");

            AbstractAdjacencyListGraph<string> testDGraph = new DirectedAdjacencyListGraph<string>();
            testDGraph.AddVertexFor("A");
            testDGraph.AddVertexFor("B");
            testDGraph.AddVertexFor("C");

            Console.WriteLine(testUGraph.GetVertexFor("A") + " is vertex for A");
            Console.WriteLine(testUGraph.GetVertexFor("C") + " is vertex for C");
        }


        public static void EnumerateVerticesTestCode()
        {
            Console.WriteLine("===============\nEnumerateVerticesTestCode\n==========\n\n\n");

            AbstractAdjacencyListGraph<string> testUGraph = new UndirectedAdjacencyListGraph<string>();
            testUGraph.AddVertexFor("A");
            testUGraph.AddVertexFor("B");
            testUGraph.AddVertexFor("C");

            Console.WriteLine(testUGraph.ToString());
            //EnumerateVertices should return a list of all three vertices A,B,C
            Console.WriteLine(String.Join(", ", testUGraph.EnumerateVertices()));


            //Same test, but for a directed graph
            AbstractAdjacencyListGraph<string> testDGraph = new DirectedAdjacencyListGraph<string>();
            testDGraph.AddVertexFor("A");
            testDGraph.AddVertexFor("B");
            testDGraph.AddVertexFor("C");

            Console.WriteLine(testUGraph.ToString());
            //EnumerateVertices should return a list of all three vertices A,B,C

            Console.WriteLine(String.Join(", ", testDGraph.EnumerateVertices()));
        }

        public static void HasVertexForTestCode()
        {
            Console.WriteLine("===============\nHasVertexForTestCode\n==========\n\n\n");

            AbstractAdjacencyListGraph<string> testUGraph = new UndirectedAdjacencyListGraph<string>();
            testUGraph.AddVertexFor("A");
            testUGraph.AddVertexFor("B");
            testUGraph.AddVertexFor("C");

            Console.WriteLine(testUGraph.HasVertexFor("A") + "==true, does contain vertex for A");
            Console.WriteLine(testUGraph.HasVertexFor("D") + "==false, does not have vertex for D");

            AbstractAdjacencyListGraph<string> testDGraph = new DirectedAdjacencyListGraph<string>();
            testDGraph.AddVertexFor("A");
            testDGraph.AddVertexFor("B");
            testDGraph.AddVertexFor("C");
            Console.WriteLine(testDGraph.HasVertexFor("A") + "==true, does contain vertex for A");
            Console.WriteLine(testDGraph.HasVertexFor("D") + "==false, does not have vertex for D");
        }


        public static void EnumerateNeighborsOfVertexForTestCode()
        {
            Console.WriteLine("===============\nEnumerateNeighborsOfVertexFor\n==========\n\n\n");

            AbstractAdjacencyListGraph<string> testUGraph = new UndirectedAdjacencyListGraph<string>();
            testUGraph.AddVertexFor("A");
            testUGraph.AddVertexFor("B");
            testUGraph.AddVertexFor("C");
            testUGraph.AddVertexFor("D");

            Console.WriteLine(testUGraph.ToString());

            //At this point, testUGraph should have 3 vertices and no edges. adjacencies property should be a List of length
            // 3, with an empty List<Edge<T>> at each index. vertexIndices should be of length 3, with entries for A=>0, B=>1, C=>2

            testUGraph.AddEdge("A", "C", 2);
            testUGraph.AddEdge("A", "B", 5);
            testUGraph.AddEdge("C", "B", 4);
            testUGraph.AddEdge("D", "A", 1);
            
            //At this point, the neighbors of
                //A should be C,D, and B
                //B should be A and C
                //C should be A and B
                //D should be A
            Console.WriteLine(String.Join(", ", testUGraph.EnumerateNeighborsOfVertexFor("A")));
            Console.WriteLine(String.Join(", ", testUGraph.EnumerateNeighborsOfVertexFor("B")));
            Console.WriteLine(String.Join(", ", testUGraph.EnumerateNeighborsOfVertexFor("C")));
            Console.WriteLine(String.Join(", ", testUGraph.EnumerateNeighborsOfVertexFor("D")));


            //Same test, but for a directed graph
            AbstractAdjacencyListGraph<string> testDGraph = new DirectedAdjacencyListGraph<string>();
            testDGraph.AddVertexFor("A");
            testDGraph.AddVertexFor("B");
            testDGraph.AddVertexFor("C");
            testDGraph.AddVertexFor("D");

            Console.WriteLine(testDGraph.ToString());

            //At this point, testDGraph should have 3 vertices and no edges. adjacencies property should be a List of length
            // 3, with an empty List<Edge<T>> at each index. vertexIndices should be of length 3, with entries for A=>0, B=>1, C=>2

            testDGraph.AddEdge("A", "C", 2);
            testDGraph.AddEdge("A", "B", 5);
            testDGraph.AddEdge("C", "B", 4);
            testDGraph.AddEdge("D", "A", 1);

            //At this point, the neighbors of
            //A should be C and B
            //B should be an empty list
            //C should be B
            //D should be A
            Console.WriteLine(String.Join(", ", testDGraph.EnumerateNeighborsOfVertexFor("A")));
            Console.WriteLine(String.Join(", ", testDGraph.EnumerateNeighborsOfVertexFor("B")));
            Console.WriteLine(String.Join(", ", testDGraph.EnumerateNeighborsOfVertexFor("C")));
            Console.WriteLine(String.Join(", ", testDGraph.EnumerateNeighborsOfVertexFor("D")));


        }


        public static void EnumerateEdgesTestCode()
        {
            Console.WriteLine("===============\nEnumerateEdgesTestCode\n==========\n\n\n");

            AbstractAdjacencyListGraph<string> testUGraph = new UndirectedAdjacencyListGraph<string>();
            testUGraph.AddVertexFor("A");
            testUGraph.AddVertexFor("B");
            testUGraph.AddVertexFor("C");

            Console.WriteLine(testUGraph.ToString());

            //At this point, testUGraph should have 3 vertices and no edges. adjacencies property should be a List of length
            // 3, with an empty List<Edge<T>> at each index. vertexIndices should be of length 3, with entries for A=>0, B=>1, C=>2

            testUGraph.AddEdge("A", "C", 2);
            testUGraph.AddEdge("A", "B", 5);
            testUGraph.AddEdge("C", "B", 4);

            //At this point, testUGraph contains 6 edges (the forward and backward edges for each edge added).
                //However, enumerating them should return only the unique edges (A,C,2)(A,B,5)(C,B,4).
            Console.WriteLine(testUGraph.ToString());

            Console.WriteLine(String.Join(", ", testUGraph.EnumerateEdges()));


            //Same test, but for a directed graph
            AbstractAdjacencyListGraph<string> testDGraph = new DirectedAdjacencyListGraph<string>();
            testDGraph.AddVertexFor("A");
            testDGraph.AddVertexFor("B");
            testDGraph.AddVertexFor("C");

            Console.WriteLine(testDGraph.ToString());

            //At this point, testDGraph should have 3 vertices and no edges. adjacencies property should be a List of length
            // 3, with an empty List<Edge<T>> at each index. vertexIndices should be of length 3, with entries for A=>0, B=>1, C=>2

            testDGraph.AddEdge("A", "C", 2);

            Console.WriteLine(String.Join(", ", testDGraph.EnumerateEdges()));

            //At this point, testDGraph should have 1 edge. In the adjacency list for A, there should be an edge (A,C,2).

            testDGraph.AddEdge("B", "C", 5);

            Console.WriteLine(String.Join(", ", testDGraph.EnumerateEdges()));

            //At this point, testUGraph should have 2 edges. EnumerateEdges should return both.

        }


        public static void MSTTestCode()
        {
            AbstractGraph<string> sourceGraph = new UndirectedAdjacencyListGraph<string>();
            sourceGraph.AddVertexFor("A");
            sourceGraph.AddVertexFor("B");
            sourceGraph.AddVertexFor("C");
            sourceGraph.AddVertexFor("D");
            sourceGraph.AddVertexFor("E");
            sourceGraph.AddVertexFor("F");
            sourceGraph.AddVertexFor("G");
            sourceGraph.AddVertexFor("H");

            sourceGraph.AddEdge("A", "F", 10);
            sourceGraph.AddEdge("A", "B", 28);
            sourceGraph.AddEdge("F", "E", 25);
            sourceGraph.AddEdge("E", "G", 24);
            sourceGraph.AddEdge("E", "D", 22);
            sourceGraph.AddEdge("G", "D", 18);
            sourceGraph.AddEdge("G", "B", 14);
            sourceGraph.AddEdge("B", "C", 16);
            sourceGraph.AddEdge("D", "C", 12);
            sourceGraph.AddEdge("B", "H", 13);

            
            //File.WriteAllText("x:\\graph.txt", sourceGraph.ToDOT());

            AbstractAdjacencyListGraph<string> mstGraph = (AbstractAdjacencyListGraph<string>)sourceGraph.MinimumSpanningTree();

            //File.WriteAllText("x:\\graph.txt", mstGraph.ToDOT());
            ;
        }
    }
}
