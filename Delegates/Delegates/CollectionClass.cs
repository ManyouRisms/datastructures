﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    //define a delegate data type
    public delegate void HandleAnInt(int x);

    // set up an enumeration to indicate forward or backward iteration
    public enum DIRECTION { BACKWARD, FORWARD};


    public class CollectionClass
    {
        // attributes
        private int[] iArray = { 5, 10, 20};

        // a method to iterate through the data
        // add a parameter that takes an instance of the delegate
        public void iterate (HandleAnInt fp, DIRECTION dir)
        {
            switch(dir)
            {
                case DIRECTION.BACKWARD:
                    for (int i = iArray.Length -1 ; i >= 0; i--)
                    {
                        fp(iArray[i]);
                    }

                    break;
                case DIRECTION.FORWARD:
                    for (int i = 0; i < iArray.Length; i++)
                    {
                        // hardcore what happens to the data
                        //Console.WriteLine(iArray[i]);

                        // call the function passed in
                        fp(iArray[i]);
                    }
                    break;
                default:
                    break;
            }
                

        }
    }
}
