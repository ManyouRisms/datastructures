﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    class Program
    {

        public static void DisplayAnInt(int y)
        {
            Console.WriteLine(y);
        }

        public static void SquareAnInt(int y)
        {
            Console.WriteLine(y*y);
        }

        public static void ModAnInt(int y)
        {
            Console.WriteLine(y % y);
        }



        static void Main(string[] args)
        {
            CollectionClass c = new CollectionClass();
            //c.iterate(DisplayAnInt, DIRECTION.FORWARD);
            //c.iterate(SquareAnInt, DIRECTION.FORWARD);
            c.iterate(ModAnInt, DIRECTION.FORWARD);

        }
    }
}
