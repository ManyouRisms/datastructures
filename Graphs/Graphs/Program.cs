﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGraph;
using System.IO;
namespace Graphs
{
    class Program
    {
        public static void PrintVertexValue(string data)
        {
            Console.WriteLine(data.ToString());
        }

        static void Main(string[] args)
        {

            //TestBreadthFirstTraversal();
            //TestShortestPath();
            //TestShortestWeightedPath();
            //TestRecursiveDepthFirst();
            //TestRemoveEdgeDirected();
            //TestRemoveEdgeUndirected();
            TestClearGraph();

        }

        private static void TestClearGraph()
        {
            AbstractAdjacencyMatrixGraph<string> graph = GenerateMatrix(1);
            graph.ClearGraph();
            graph.AddVertexFor("A");
            graph.AddVertexFor("B");
            graph.AddEdge("A", "B");
            graph.BreadthFirstTraversal("A", PrintVertexValue);

        }

        public static void TestRemoveEdgeUndirected()
        {
            // generate undirected graph
            AbstractAdjacencyMatrixGraph<string> graph = GenerateMatrix(1);
            // show traversal before removing edges
            graph.DepthFirstTraversal("A", PrintVertexValue);

            // remove two edges
            graph.RemoveEdge("A", "B");
            graph.RemoveEdge("A", "E");

            // show it again
            Console.WriteLine("\n\nAfter removing edge\n\n");
            Console.WriteLine("===========================");
            graph.DepthFirstTraversal("A", PrintVertexValue);

        }

        public static void TestRemoveEdgeDirected()
        {
            // generate directed graph
            AbstractAdjacencyMatrixGraph<string> graph = GenerateMatrix(0);
            // show traversal before removing edges
            graph.DepthFirstTraversal("A", PrintVertexValue);
            // remove two edges
            graph.RemoveEdge("A", "B");
            graph.RemoveEdge("A", "E");
            // show it again
            Console.WriteLine("\n\nAfter removing edge\n\n");
            Console.WriteLine("===========================");
            graph.DepthFirstTraversal("A", PrintVertexValue);

        }

        public static void TestRecursiveDepthFirst()
        {
            AbstractAdjacencyMatrixGraph<string> graph = GenerateMatrix(1);
            graph.AddVertexFor("A");
            graph.AddVertexFor("B");
            graph.AddVertexFor("C");
            graph.AddVertexFor("D");
            graph.AddVertexFor("E");
            graph.AddVertexFor("F");
            graph.AddVertexFor("G");

            graph.AddEdge("A","B");
            graph.AddEdge("B","C");
            graph.AddEdge("C","G");
            graph.AddEdge("A","D");
            graph.AddEdge("A","E");
            graph.AddEdge("E", "F");
            graph.AddEdge("F", "G");

            graph.DepthFirstTraversalRec("G", PrintVertexValue);
        }

        public static void TestShortestWeightedPath()
        {
            AbstractAdjacencyMatrixGraph<string> graph = new UndirectedAdjacencyMatrixGraph<string>();

            graph.AddVertexFor("A");
            graph.AddVertexFor("B");
            graph.AddVertexFor("C");
            graph.AddVertexFor("D");
            graph.AddVertexFor("E");
            graph.AddVertexFor("F");
            graph.AddVertexFor("G");
            graph.AddVertexFor("H");
            graph.AddVertexFor("K");

            graph.AddEdge("A", "C", 4);
            graph.AddEdge("A", "B", 2);
            graph.AddEdge("C", "B", 1);
            graph.AddEdge("C", "D", 2);
            graph.AddEdge("B", "E", 3);
            graph.AddEdge("B", "D", 5);
            graph.AddEdge("D", "H", 1);
            graph.AddEdge("E", "H", 5);
            graph.AddEdge("E", "G", 2);
            graph.AddEdge("E", "F", 6);
            graph.AddEdge("F", "G", 4);
            graph.AddEdge("H", "K", 4);
            graph.AddEdge("G", "K", 3);


            //File.WriteAllText("p:\\public_html\\graph.txt", graph.ToDOT());

            ////Breakpoint goes here to allow you to see the constructed graph
            // before swp
            Console.WriteLine(graph.ToString());

            AbstractAdjacencyMatrixGraph<string> spGraph = (AbstractAdjacencyMatrixGraph<string>)graph.ShortestWeightedPath("A", "K");
            Console.WriteLine(spGraph.ToString());
            ////Display the shortest path graph
            //File.WriteAllText("p:\\public_html\\graph.txt", spGraph.ToDOT());
        }

        /*
         * Uncomment file comments in breadth first traversal to visualize graph
         *  */

   
        public static void TestBreadthFirstTraversal()
        {
            AbstractAdjacencyMatrixGraph<string> graph = new UndirectedAdjacencyMatrixGraph<string>();
            graph.AddVertexFor("A");
            graph.AddVertexFor("B");
            graph.AddVertexFor("C");
            graph.AddVertexFor("D");
            graph.AddVertexFor("E");
            graph.AddVertexFor("F");
            graph.AddVertexFor("G");
            graph.AddVertexFor("H");
            graph.AddVertexFor("K");

            graph.AddEdge("A", "B");
            graph.AddEdge("A", "C");
            graph.AddEdge("A", "D");
            graph.AddEdge("C", "E");
            graph.AddEdge("C", "F");
            graph.AddEdge("E", "G");
            graph.AddEdge("E", "H");
            graph.AddEdge("G", "K");
            graph.AddEdge("H", "K");

            //File.WriteAllText("p:\\public_html\\graph.txt", graph.ToDOT());
            graph.BreadthFirstTraversal("A", PrintVertexValue);
            Console.WriteLine(graph.ToString());
        }

        public static void TestRemoveVertex()
        {
            AbstractAdjacencyMatrixGraph<string> graph = new DirectedAdjacencyMatrixGraph<string>();

            graph.AddVertexFor("A");
            graph.AddVertexFor("B");
            graph.AddVertexFor("C");
            graph.AddVertexFor("D");

            graph.AddEdge("A", "C", 5);
            graph.AddEdge("D", "A", 5);
            graph.AddEdge("D", "C", 5);
            graph.AddEdge("B", "D", 5);
            graph.AddEdge("B", "A", 5);
            Console.WriteLine(graph.ToString());

            graph.RemoveVertexFor("C");
            Console.WriteLine(graph.ToString());
        }

        public static void TestShortestPath()
        {

            /**
             * Test for shortest path
             *
            
             **/
            AbstractAdjacencyMatrixGraph<string> sourceGraph = new UndirectedAdjacencyMatrixGraph<string>();


            sourceGraph.AddVertexFor("A");
            sourceGraph.AddVertexFor("B");
            sourceGraph.AddVertexFor("C");
            sourceGraph.AddVertexFor("D");
            sourceGraph.AddVertexFor("E");
            sourceGraph.AddVertexFor("F");
            sourceGraph.AddVertexFor("G");
            sourceGraph.AddVertexFor("H");

            sourceGraph.AddEdge("A", "F", 10);
            sourceGraph.AddEdge("A", "B", 28);
            sourceGraph.AddEdge("F", "E", 25);
            sourceGraph.AddEdge("E", "G", 24);
            sourceGraph.AddEdge("E", "D", 22);
            sourceGraph.AddEdge("G", "D", 18);
            sourceGraph.AddEdge("G", "B", 14);
            sourceGraph.AddEdge("B", "C", 16);
            sourceGraph.AddEdge("D", "C", 12);
            sourceGraph.AddEdge("B", "H", 13);

            Console.WriteLine(sourceGraph.ToString());

            //File.WriteAllText("p:\\public_html\\graph.txt", sourceGraph.ToDOT());

            //sourceGraph.BreadthFirstTraversal(
            //    "A", PrintVertexValue);
            //Put a breakpoint here to see the graph we're creating a MST for
            //AbstractAdjacencyMatrixGraph<string> mstGraph = (AbstractAdjacencyMatrixGraph<string>)sourceGraph.MinimumSpanningTree();

            //File.WriteAllText("p:\\public_html\\graph.txt", mstGraph.ToDOT());


        }

        /// <summary>
        /// Generates a Matrix graph of specs entered, either directed or undirected.
        /// </summary>
        /// <param name="type">
        /// 0: Directed Matrix
        /// 1: Undirected Matrix
        /// </param>
        /// <returns>A new AbstractAdjacencyMatrixGraph (string)</returns>
        public static AbstractAdjacencyMatrixGraph<string> GenerateMatrix(int type)
        {
            AbstractAdjacencyMatrixGraph<string> graph;
            if (type == 0) /* directed */ { graph = new DirectedAdjacencyMatrixGraph<string>(); }
            else if (type == 1) /* undirected */ { graph = new UndirectedAdjacencyMatrixGraph<string>(); }
            else /* invalid type */ { throw new ApplicationException("Invalid type specified"); }

            graph.AddVertexFor("A");
            graph.AddVertexFor("B");
            graph.AddVertexFor("C");
            graph.AddVertexFor("D");
            graph.AddVertexFor("E");
            graph.AddVertexFor("F");
            graph.AddVertexFor("G");

            graph.AddEdge("A", "B");
            graph.AddEdge("B", "C");
            graph.AddEdge("C", "G");
            graph.AddEdge("A", "D");
            graph.AddEdge("A", "E");
            graph.AddEdge("E", "F");
            graph.AddEdge("F", "G");

            return graph;
        }

    }
}
