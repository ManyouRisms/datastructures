﻿using Hashtables;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtables
{
    public abstract class A_OpenAddressing<K, V> : A_Hashtables<K, V>
        where V : IComparable<V>
        where K : IComparable<K>
    {
        // a method to get the next increment if there is a collision
        protected abstract int GetIncrement(int iAttempt, K key);

        // create an instance of the prime number class
        PrimeNumber pm = new PrimeNumber();
        // constructor sets up the data array
        public A_OpenAddressing()
        {
            oDataArray = new object[pm.GetNextPrime()];
        }

        public override V Get(K key)
        {
            // some key ideas
            // don't search the whole table, quit if you know it isn't there. (ie dont use a for loop from start to end)
            // NOT A HASH TABLE
            // know where to start (your hash function)

            //similar to the add method


            V found = default(V); // care to see if it might be a potential value in the table
            int iAttempt = 1;
            bool bFound = false;
            int initialHash = HashFunction(key);
            int iCurrentLocation = initialHash;
            while (oDataArray[iCurrentLocation] != null && !bFound)
            {
                if (oDataArray[iCurrentLocation].GetType() == typeof(KeyValue<K, V>))
                {
                    KeyValue<K, V> kv = (KeyValue<K, V>)oDataArray[iCurrentLocation];
                    if (kv.Key.CompareTo(key) == 0)
                    {
                        found = kv.Value;
                        bFound = true;
                    }
                }

                iCurrentLocation = initialHash + GetIncrement(iAttempt++, key);
                iCurrentLocation %= HTSize; // make sure current location is within the size of the array

            }
            if (!bFound)
            {
                throw new ApplicationException("Not Found");
            }
            return found;

        }

        public override void Add(K key, V value)
        {
            // how many attempts you have made
            int iAttempt = 1;

            // get the initial hash position
            int iInitialHash = HashFunction(key); // gives us the number somewhere in the array

            // the current location we are checking
            int iCurrentLocation = iInitialHash;

            // position where to finally add
            int iPositionToAdd = -1;

            // boolean to help track a collision for science
            bool bCollided = false;

            // loop through the current collision sequence (chain) if it exists
            while (oDataArray[iCurrentLocation] != null)
            {
                // have to find out if the current value is a KV or a tombstone
                // and act accordingly

                // if the current value is a KV
                if (oDataArray[iCurrentLocation].GetType() == typeof(KeyValue<K, V>))
                {
                    // compare current value with the value to add
                    KeyValue<K, V> kv = (KeyValue<K, V>)oDataArray[iCurrentLocation];
                    if (kv.Key.CompareTo(key) == 0) // they are the same
                    {
                        throw new ApplicationException("Item already exists in hash table at location  " + iCurrentLocation);
                    }

                }
                // else if it's a tombstone
                else
                {
                    // if it's the first tombstone set it to iPositionToAdd
                    if (iPositionToAdd == -1) // was set to -1, only possible if it doesn't have a set position yet
                    {
                        iPositionToAdd = iCurrentLocation;
                    }
                }
                // move to the next location 
                iCurrentLocation = iInitialHash + GetIncrement(iAttempt++, key); // in linear the key doesn't actually do anything
                // wrap around to top of table
                iCurrentLocation %= HTSize;

                if (!bCollided)
                {
                    // increment collisions
                    iNumCollisions++;
                    bCollided = true;
                }
            }

            // we either found a tombstone or didn't. either way we're going to add an element (but where?)

            // how do we know if we found a tombstone?
            if (iPositionToAdd == -1)
            {
                iPositionToAdd = iCurrentLocation; // icurrentlocation will be the end of the collision sequence.
            }

            KeyValue<K, V> kvNew = new KeyValue<K, V>(key, value);

            oDataArray[iPositionToAdd] = kvNew;
            iCount++;
            // check if we have to expand
            if (IsOverloaded())
            {
                ExpandHashTable();
            }
        }


        private void ExpandHashTable()
        {
            //create reference to the old object array (or we'd lose it?)
            object[] oOldArray = oDataArray;

            //create an array based on what size it should be
            oDataArray = new object[pm.GetNextPrime()];

            //reset all of the attributes
            iCount = 0;
            iNumCollisions = 0;


            // loop through elements int he old array
            for (int i = 0; i < oOldArray.Length; i++)
            {
                // if current location is not null
                if (oOldArray[i] != null)
                {
                    // use this opportunity to get rid of the tombstones
                    // if the current type is a key/value pair, readd it
                    if (oOldArray[i].GetType() == typeof(KeyValue<K, V>))
                    {
                        KeyValue<K, V> kv = (KeyValue<K, V>)oOldArray[i];
                        Add(kv.Key, kv.Value);
                    }
                }
            }
        }

        private bool IsOverloaded()
        {
            return iCount / (double)HTSize > dLoadFactor;
        }

        public override void Remove(K key) // add override if setting back
        {

            int iAttempt = 1; // to figure out the increment
            int initialHash = HashFunction(key); // fundamental hashtable shit
            bool bFound = false; // loop ending helper thing
            int iCurrentLocation = initialHash; 
            while (oDataArray[iCurrentLocation] != null && !bFound) // loop while still in collision sequence and the item wasn't found
            {
                if (oDataArray[iCurrentLocation].GetType() == typeof(KeyValue<K, V>))
                {
                    KeyValue<K, V> kv = (KeyValue<K, V>)oDataArray[iCurrentLocation];
                    if (kv.Key.CompareTo(key) == 0)
                    {
                        oDataArray[iCurrentLocation] = new Tombstone();
                        bFound = true;
                    }
                }
                // get next location to try
                iCurrentLocation = initialHash + GetIncrement(iAttempt++, key);
                iCurrentLocation %= HTSize; // make sure current location is within the size of the array, ie wrap to top of array
                --iCount;
            }

            if (!bFound)
            {
                throw new KeyNotFoundException("Key" + key + "Not Found, can't remove.");
            }
        }


        public override IEnumerator<V> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < oDataArray.Length; i++)
            {
                sb.Append("Bucket " + i + ": ");
                if (oDataArray[i] != null)
                {
                    if (oDataArray[i].GetType() == typeof(Tombstone))
                    {
                        sb.Append("Tombstone");
                    }
                    else
                    {
                        KeyValue<K, V> kv = (KeyValue<K, V>)oDataArray[i];
                        sb.Append(kv.Value.ToString() + "-->" + HashFunction(kv.Key));
                    }

                }
                sb.Append("\n");
            }
            return sb.ToString();
        }
    }



}
