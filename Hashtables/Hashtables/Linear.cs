﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtables
{
    public class Linear<K, V> : A_OpenAddressing<K, V>
        where V : IComparable<V>
        where K : IComparable<K>
    {
        public override void Remove(K key)
        {

            // default of kv pair to keep track of deleted one
            KeyValue<K, V> suitableReplacement = default(KeyValue<K, V>); // placeholder

            KeyValue<K, V> kv = default(KeyValue<K, V>); // extracted to have a reference outside

            int iAttempt = 1; // to figure out the increment

            int initialHash = HashFunction(key); // fundamental hashtable

            bool bFound = false; // loop ending helper thing

            int iCurrentLocation = initialHash;

            int iStoredLocation = -1; ;
            while (oDataArray[iCurrentLocation] != null && !bFound) // loop while still in collision sequence and the item wasn't found
            {
                // set kv to the kv in current spot
                kv = (KeyValue<K, V>)oDataArray[iCurrentLocation];

                // if the hashes match
                if (HashFunction(kv.Key).CompareTo(HashFunction(key)) == 0)
                {
                    // remove the desired value
                    oDataArray[iCurrentLocation] = null;

                    bFound = true;

                    // if next item in chain isn't null
                    if (oDataArray[iCurrentLocation + 1] != null)
                    {
                        //set it to our stored location
                        iStoredLocation = iCurrentLocation+1;

                        // make our replacement the next spot in the chain
                        suitableReplacement = (KeyValue<K, V>)oDataArray[iCurrentLocation + 1];
                    }

                }


                // get next location to try
                iCurrentLocation = initialHash + GetIncrement(iAttempt++, key);

                iCurrentLocation %= HTSize; // make sure current location is within the size of the array, ie wrap to top of array

            }

            if (!bFound)
            {
                throw new KeyNotFoundException("Key " + key + "Not Found, can't remove.");
            }

            --iCount;


            if (suitableReplacement != null)
            {
                Console.WriteLine("adding back " + suitableReplacement.Value);
                this.Add(suitableReplacement.Key, suitableReplacement.Value);
                oDataArray[iStoredLocation] = null;

            }
        }

        protected override int GetIncrement(int iAttempt, K key)
        {
            // set an increment value
            int iIncrement = 1;
            return iIncrement * iAttempt;
        }
    }
}
