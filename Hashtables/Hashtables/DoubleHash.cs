﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtables
{
    class DoubleHash<K, V> : A_OpenAddressing<K, V>
        where V : IComparable<V>
        where K : IComparable<K>
    {
        protected override int GetIncrement(int iAttempt, K key)
        {
            // gives a unique increment for a particular key. 
            // how does the iAttempt factor in? The formula is only 
            // for the initial increment, so we times it by our attempts
            // to get the correct increment. 
            // Also note we use gethashcode and not HashFunction(key)
            // because we have to use the initial hash, while the hashfunction
            // mods by the tablesize.
            return iAttempt * (1 + Math.Abs(key.GetHashCode()) % (HTSize-1));
        }
    }
}