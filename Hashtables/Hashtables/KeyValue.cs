﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtables
{
    class KeyValue<K, V> : System.Collections.IComparer
        where V : IComparable<V>
        where K : IComparable<K>
    {
        // store the key of the data (similar to a primary key in database records)
        K kKey;
        // store the actual data
        V vValue;

        // store an accessor count
        private int timesAccessed;

        public int TimesAccessed
        {
            get { return this.timesAccessed; }
            set { timesAccessed = value; }
        }


        public KeyValue(K key, V value)
        {
            kKey = key;
            vValue = value;
        }

        public V Value
        {
            get { return vValue; }
        }

        public K Key
        {
            get { return kKey; }
        }

        /// <summary>
        /// Need to override equals so the key/value pair can be compared to another key/value pair. 
        /// Espcially important for the ArrayList.Contains method, which calls Equals for comparisons.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            KeyValue<K, V> kv = (KeyValue<K, V>)obj;
            return this.Key.CompareTo(kv.Key) == 0;
        }


        // comparer used to sort t
        public int Compare(object x, object y)
        {
            Console.WriteLine("Calling compare method in KV class.");
            int returnval = 0;
            if (x.GetType() == typeof(KeyValue<K, V>) && y.GetType() == typeof(KeyValue<K, V>))
            {
                KeyValue<K, V> x2 = (KeyValue<K, V>)x;
                KeyValue<K, V> y2 = (KeyValue<K, V>)y;
                Console.WriteLine(x2.ToString() + "\n" + y2.ToString());

                returnval = x2.TimesAccessed > y2.TimesAccessed
                ? 1
                : (x2.TimesAccessed < y2.TimesAccessed
                ? -1
                : returnval);

            }
            return returnval;
        }
    }

}


