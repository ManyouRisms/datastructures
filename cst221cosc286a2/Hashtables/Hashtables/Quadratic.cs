﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtables
{
    public class Quadratic<K, V> : A_OpenAddressing<K, V>
        where V : IComparable<V>
        where K : IComparable<K>
    {
        protected override int GetIncrement(int iAttempt, K key)
        {
            double c1 = 0.5;
            double c2 = 0.5;

            //implement the quadratic formula
            //  (h(x) + c1*i + c2*i2)%n
            return (int)(c1 * iAttempt + c2 * Math.Pow(iAttempt, 2));


        }

        public Quadratic()
        {
            //set the load factor to 0.50 to ensure that we don't get into an infinite loop
            // Note this is only necessary for quadratic
            this.dLoadFactor = 0.50;
        }

    }
}
