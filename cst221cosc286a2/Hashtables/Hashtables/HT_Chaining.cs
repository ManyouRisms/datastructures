﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtables
{

    /// <summary>
    /// Implements the A_Hashtable using ArrayLists at each hash table location.
    /// </summary>
    class HT_Chaining<K, V> : A_Hashtables<K, V>
        where V : IComparable<V>
        where K : IComparable<K>
    {

        // inner class creates a comparer to use with the sort of the arraylist.
        public class MyCompare : IComparer
        {
            // the compare method used in the sort
            int IComparer.Compare(Object x, Object y)
            {
                int returnval = 0;

                // force a check to make sure we're working with key/values, so we can
                // sort on it's 'timesaccessed' property. 
                if (x.GetType() == typeof(KeyValue<K, V>) && y.GetType() == typeof(KeyValue<K, V>))
                {
                    KeyValue<K, V> x2 = (KeyValue<K, V>)x;
                    KeyValue<K, V> y2 = (KeyValue<K, V>)y;

                    // if first object's timesaccessed is less than 
                    // object 2's 'timesaccessed' return a 1,
                    // return -1 if the opposite is true, and return 
                    // 0 if they are the same.
                    returnval = x2.TimesAccessed < y2.TimesAccessed
                    ? 1
                    : (x2.TimesAccessed > y2.TimesAccessed
                    ? -1
                    : returnval);

                }
                return returnval;
            }
        }


        // intial size for oDataArray
        private const int iInitialSize = 4;

        // how many buckets (locations in the array) are occupied by ArrayLists
        // not the same as count, becuase we need to know the amount of arraylists
        // to calculate our current load factor. then we can use 
        // bucketcount / htsize > loadfactor to increase
        private int iBucketCount = 0;

        public HT_Chaining()
        {
            // set up the data array
            this.oDataArray = new object[iInitialSize];

        }

        public HT_Chaining(int iInitialSize, double dLoadFactor)
        {
            this.oDataArray = new object[iInitialSize];
            this.dLoadFactor = dLoadFactor;
        }

        #region A_Hashtable Implementation


        /*
         * With the index, you're using the key to check, that is fine
         * However the array doesn't know about key objects, so its trying to compare
         * a key to a key/value pair. That confuses the computer so we have to
         * look at the data type. indexof requires k/v pair. we extantiate one, we pass
         * the key in and a default of v(dont care about val to compare). IndexOf returns
         * an index, or returns an invalid index otherwise.
         * 
         * 
         */
        /// <summary>
        /// Given a key, return the value associated with it if it exists. Else throw
        /// an exception.
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Value to be returned</returns>
        /// 
        public override V Get(K key)
        {
            KeyValue<K, V> kv = new KeyValue<K, V>(key, default(V));
            V found = default(V);
            ArrayList arrayList;
            int hashcode = HashFunction(key);
            if (oDataArray[hashcode] == null)
            {
                throw new ApplicationException("not found");
            }
            else
            {
                arrayList = (ArrayList)oDataArray[hashcode];
                if (arrayList.Contains(kv))
                {
                    // create a kv pair based on the kv at the location of the array, so we can access its 'TimesAccessed' property.
                    KeyValue<K, V> newKV = (KeyValue<K, V>)arrayList[arrayList.IndexOf(kv)];

                    // append a count to the data
                    newKV.TimesAccessed++;

                    // find the index of the key value pair in the arraylist, and then cast it to a kv pair, and assign its value to found (returning v)
                    found = ((KeyValue<K, V>)arrayList[arrayList.IndexOf(kv)]).Value;
                }
                else
                {
                    throw new ApplicationException("not found");
                }
            }

            // initialize a comparator to sort array by
            MyCompare comparator = new MyCompare();

            // could potentially be moved outside of get as to be done as a batch 
            // late at night or otherwise.
            arrayList.Sort(comparator);

            return found;
        }



        public override void Add(K key, V value)
        {
            //hashcode <-- hash of the key 
            //create the key value pair object
            //if the hashcode location of the key is null
            // create an arraylist and store it's reference at that location
            // increment a bucket count
            //else
            // increment the number of collisions
            //get a reference to the arraylist at the hashed location
            //if the arraylist contains the current kv pair
            // throw an exception that the key exists already
            //else
            // add the kv value to the arraylist
            // increment the count
            //if the hashtable is now overloaded
            //expand the hashtable

        #endregion
            KeyValue<K, V> kv = new KeyValue<K, V>(key, value);
            int hashCode = HashFunction(key);
            ArrayList list;


            if (oDataArray[hashCode] == null)
            {
                //list = new ArrayList();
                oDataArray[hashCode] = new ArrayList();
                iBucketCount++;
            }
            else
            {
                iNumCollisions++;
                //list = (ArrayList)oDataArray[hashCode];
            }
            list = (ArrayList)oDataArray[hashCode];

            if (list.Contains(kv))
            {
                throw new Exception("That key/value exists");
            }
            else
            {
                list.Add(kv);
                iCount++;
            }

            if (IsOverLoaded())
            {
                ExpandHashTable();
            }
        }



        //<summary>
        //Expand the hashtable size by a factor of 2.
        //Note that each element is rehashed into the new array. The reason for this
        //is that the location for any key/value pair may be different in the new array
        //compared to the old array.. This is a result of the fact that we % by hashtable size
        //in the hash function.
        //</summary>
        private void ExpandHashTable()
        {
            //create reference to the old object array (or we'd lose it?)
            object[] oOldArray = oDataArray;

            //create an array twice as big!
            oDataArray = new object[HTSize * 2];

            //reset all of the attributes
            iCount = 0;
            iBucketCount = 0;
            iNumCollisions = 0;

            // loop through elements int he old array
            for (int i = 0; i < oOldArray.Length; i++)
            {
                // if current location is not null
                if (oOldArray[i] != null)
                {
                    // get a reference to the current arraylist
                    ArrayList alCurrent = (ArrayList)oOldArray[i];
                    // loop through each item in that list (because we have to rehash)
                    foreach (KeyValue<K, V> kv in alCurrent)
                    {
                        // add it to the hash table
                        Add(kv.Key, kv.Value);
                    }
                }
            }
        }


        private bool IsUnderloaded()
        {
            return ((float)iBucketCount / HTSize <= dMinLoadFactor);
        }



        /// <summary>
        /// Returns true or false depending on how full the data array is
        /// </summary>
        /// <returns>True -> Too full.  False -> Not too full</returns>
        private bool IsOverLoaded()
        {
            return ((float)iBucketCount / HTSize >= dLoadFactor);
        }

        /// <summary>
        /// Remove a value based on the key. Throw an exception
        /// if not found.
        /// </summary>
        /// <param name="key">Key of the item to find.</param>
        public override void Remove(K key)
        {

            // get hashcode for key
            int iHashCode = HashFunction(key);
            //get reference to the arraylist
            ArrayList alCurrent = (ArrayList)oDataArray[iHashCode];
            //boolean for found if not
            bool bFound = false;
            //if arraylist exists
            if (alCurrent != null)
            {
                KeyValue<K, V> kv = new KeyValue<K, V>(key, default(V));
                if (alCurrent.Contains(kv))
                {
                    alCurrent.Remove(kv);
                    bFound = true;
                    //decrement count, check size of array list. if its zero remove it
                    iCount--;
                    if (alCurrent.Count == 0)
                    {
                        oDataArray[iHashCode] = null;
                        iBucketCount--;
                    }
                }
            }
            //if arraylist is null or value wasn't found
            if (alCurrent == null || !bFound)
            // throw an exception
            {
                throw new ApplicationException("Not found");
            }


        }

        public override IEnumerator<V> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            //Loop through each bucket
            for (int i = 0; i < HTSize; i++)
            {
                //Add the bucket number to the string
                sb.Append("Bucket " + i.ToString() + ": ");
                //check if an arraylist exists at this location
                if (oDataArray[i] != null)
                {
                    ArrayList alCurrent = (ArrayList)oDataArray[i];
                    foreach (KeyValue<K, V> kv in alCurrent)
                    {
                        sb.Append("Accessed:" + kv.TimesAccessed + "\t" + kv.Value.ToString() + " --> ");
                    }
                    sb.Remove(sb.Length - 5, 5);
                }
                sb.Append("\n");

            }
            return sb.ToString();
        }
    }
}
