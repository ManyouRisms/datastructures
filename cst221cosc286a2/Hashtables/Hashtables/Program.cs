﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtables
{
    class Program
    {


        /// <summary>
        /// Loads data from a file into a hashtable
        /// </summary>
        /// <param name="ht"></param>
        static void LoadDataFromFile(A_Hashtables<Person, Person> ht)
        {
            StreamReader sr = new StreamReader(File.Open("People.txt", FileMode.Open));
            string sInput = "";

            try
            {
                //Read a line from the file
                while ((sInput = sr.ReadLine()) != null)
                {
                    try
                    {
                        char[] cArray = { ' ' };
                        string[] sArray = sInput.Split(cArray);
                        int iSSN = Int32.Parse(sArray[0]);
                        Person p = new Person(iSSN, sArray[2], sArray[1]);
                        ht.Add(p, p);

                    }
                    catch (ApplicationException ae)
                    {
                        Console.WriteLine("Exception: " + ae.Message);
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            sr.Close();
        }
        static void SimpleHashTableTest()
        {
            HT_Chaining<int, string> ht = new HT_Chaining<int, string>();

            ht.Add(1352 ,"Bob");
            ht.Add(1427 ,"Jim");
            ht.Add(1563 ,"Sam");
            ht.Add(1863 ,"Rick");
            ht.Add(1134, "Marge");
            ht.Add(4241, "Smurf");
            ht.Add(4211, "goober");
            ht.Add(2144, "Staahp");
            Console.WriteLine(ht.ToString());


        }

        static void TestGetChain()
        {

            HT_Chaining<int, string> ht = new HT_Chaining<int, string>();
            ht.Add(1, "hi");
            ht.Get(1);
            ht.Get(1);
            ht.Add(2, "hello");
            ht.Add(3, "howrya");
            ht.Add(4, "realgood");
            ht.Add(5, "whyuask");
            ht.Add(6, "imsostupid");
            ht.Add(7, "illneverbeaprogrammer");
            // unsorted
            Console.WriteLine(ht.ToString());


            ht.Get(1);

            Console.WriteLine(ht.ToString());

            ht.Get(5);
            ht.Get(5);
            ht.Get(5);
            ht.Get(5);

            //sorted
            Console.WriteLine(ht.ToString());
        }

        static void TestRemove(A_Hashtables<int, string> ht)
        {
            ht.Add(1634, "John");
            ht.Add(1884, "Jacob");
            ht.Add(1434, "Schmidt");
            ht.Add(12894, "Otherone");
            ht.Add(16934, "123");
            ht.Add(12074, "");
            ht.Add(1534, "");

            try
            {
                ht.Remove(1434);
            }
            catch (KeyNotFoundException k)
            {
                Console.WriteLine(k.Message);
            }
        }

        public static void TestAllImplementations()
        {
            Linear<Person, Person> lin = new Linear<Person, Person>();
            Quadratic<Person, Person> quad = new Quadratic<Person, Person>();
            DoubleHash<Person, Person> dh = new DoubleHash<Person, Person>();

            // linear
            LoadDataFromFile(lin);
            Console.WriteLine("Linear num collisions: \t" + lin.NumCollisions);
            Console.WriteLine("Linear count file is: " + lin.Count);

            // quad
            LoadDataFromFile(quad);
            Console.WriteLine("Quadratic num collisions: \t" + quad.NumCollisions);
            Console.WriteLine("Quadratic count file is: " + quad.Count);

            // double hash
            LoadDataFromFile(dh);
            Console.WriteLine("DoubleHash num collisions: \t" + dh.NumCollisions);
            Console.WriteLine("DoubleHash count file is: " + dh.Count);

        }

        public static void TestHT_Chaining_Add(HT_Chaining<int, string> ht)
        {
            ht.Add(1234, "Rick");
            ht.Add(2135, "Rob");
            ht.Add(2387, "Ron");
            ht.Add(3098, "Shane");
            ht.Add(4756, "Sharon");

            Console.WriteLine(ht.ToString());
        }
        public static void Main(string[] args)
        {

            // part II question 2
            //TestGetChain();




           // HT_Chaining<int, string> ht2 = new HT_Chaining<int, string>();

            //TestRemove(ht2);

            //TestAllImplementations();

            A_OpenAddressing<int, string> oa = new Linear<int, string>();
            TestOpenAddressingRemove(oa);


        }

        public static void TestOpenAddressingRemove(A_OpenAddressing<int, string> oa)
        {
            oa.Add(1, "hi");
            oa.Add(2, "bye");
            oa.Add(3, "welcome");
            oa.Add(4, "hostile");
            oa.Add(5, "bird");
            oa.Add(6, "turtle");
            oa.Add(7, "android");
            oa.Add(8, "simon");



            Console.WriteLine(oa.ToString());

            oa.Remove(1);
            Console.WriteLine(oa.ToString());


        }





    }
}
