﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtables
{
    public class Linear<K, V> : A_OpenAddressing<K, V>
        where V : IComparable<V>
        where K : IComparable<K>
    {
        public override void Remove(K key)
        {

            // default of kv pair to keep track of deleted one
            KeyValue<K, V> suitableReplacement = default(KeyValue<K, V>); // placeholder

            KeyValue<K, V> kv = default(KeyValue<K, V>); // extracted to have a reference outside

            int iAttempt = 1; // to figure out the increment

            int initialHash = HashFunction(key); // fundamental hashtable

            bool bFound = false; // loop ending helper thing

            int iCurrentLocation = initialHash;

            while (oDataArray[iCurrentLocation] != null && !bFound) // loop while still in collision sequence and the item wasn't found
            {

                kv = (KeyValue<K, V>)oDataArray[iCurrentLocation];
                {
                    if (HashFunction(kv.Key).CompareTo(HashFunction(key)) == 0 && oDataArray[iCurrentLocation + 1] == null)
                    {
                        // found a suitable replacement because the hashs are the same, remember 
                        // it to add it back after.
                        suitableReplacement = kv;

                        //remove desired kv
                        oDataArray[iCurrentLocation] = null;

                        bFound = true;
                        Console.WriteLine("Deleted");
                    }
                }


                // get next location to try
                iCurrentLocation = initialHash + GetIncrement(iAttempt++, key);

                iCurrentLocation %= HTSize; // make sure current location is within the size of the array, ie wrap to top of array

            }

            if (!bFound)
            {
                throw new KeyNotFoundException("Key " + key + "Not Found, can't remove.");
            }

            --iCount;
            oDataArray[iCurrentLocation] = null;
            //oDataArray[initialHash] = kv == null ? suitibleReplacement : kv;
            this.Add(suitableReplacement.Key, suitableReplacement.Value);
        }
        protected override int GetIncrement(int iAttempt, K key)
        {
            // set an increment value
            int iIncrement = 1;
            return iIncrement * iAttempt;
        }
    }
}
