﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{
    public class Edge<T> : IComparable<Edge<T>> where T : IComparable<T>
    {
        private Vertex<T> to;
        private double weight;


        public Edge(Vertex<T> from, Vertex<T> to, double weight)
        {
            this.to = to;
            this.from = from;
            this.weight = weight;
        }

        public Edge(Vertex<T> from, Vertex<T> to) : this(from,to, Double.PositiveInfinity)
        {

        }


        public override string ToString()
        {
            return this.from.ToString() + " -> " + this.weight + " -> " + this.to.ToString();
        }

        public int CompareTo(Edge<T> other)
        {
            int value = this.Weight.CompareTo(other.Weight);
            if (value==0)
            {
                value = this.To.CompareTo(other.To);
                {
                    if (value==0)
                    {
                        value = this.From.CompareTo(other.To);
                    }
                }
            }
            return value;
        }




        public double Weight
        {
            get { return weight; }
            set { weight = value; }
        }
        private Vertex<T> from;

        public Vertex<T> To
        {
            get { return to; }

        }

        public Vertex<T> From
        {
            get { return from; }

        }
    }
}
