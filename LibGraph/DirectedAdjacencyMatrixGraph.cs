﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{
    public  class DirectedAdjacencyMatrixGraph<T> : AbstractAdjacencyMatrixGraph<T> where T : IComparable<T>
    {
        public override IEnumerable<Edge<T>> EnumerateEdges()
        {
            List<Edge<T>> edges = new List<Edge<T>>();
            for (int rows = 0; rows < this.edgeMatrix.GetLength(0); rows++)
            {
                for (int cols = 0; cols < this.edgeMatrix.GetLength(0); cols++)
                {
                    if (edgeMatrix[rows, cols] != null)
                    {
                        edges.Add(edgeMatrix[rows, cols]);
                    }
                }
            }
            return edges;
        }

        protected override string GetDOTType()
        {
            return "digraph";
        }

        protected override string FormatEdgeAsDOT(Edge<T> edge)
        {
            string fromID = "v_" + edge.From.GetHashCode();
            string toID = "v_" + edge.To.GetHashCode();
            return fromID + " -> " + toID + "[label=" + edge.Weight + "]\n";
        }

        public override void RemoveEdge(T from, T to)
        {
            int nFrom = this.vertexIndices[from];
            int nTo = this.vertexIndices[to];
            this.edgeMatrix[nFrom, nTo] = null;
        }
    }
}
