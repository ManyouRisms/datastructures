﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{
    public class UndirectedAdjacencyMatrixGraph<T> : AbstractAdjacencyMatrixGraph<T> where T : IComparable<T>
    {
        // override some addEdge related methods to insert
        public override void AddEdge(T from, T to, double weight)
        {
            base.AddEdge(from, to, weight);
            base.AddEdge(to, from, weight);
        }



        public override IEnumerable<Edge<T>> EnumerateEdges()
        {
            List<Edge<T>> edges = new List<Edge<T>>();
            for (int rows = 0; rows < this.edgeMatrix.GetLength(0); rows++)
            {

                for (int cols = rows; cols < this.edgeMatrix.GetLength(1); cols++)
                {
                    if (edgeMatrix[rows, cols] != null)
                    {
                        edges.Add(edgeMatrix[rows, cols]);
                    }
                }
            }
            return edges;
        }

        protected override string GetDOTType()
        {
            return "graph";
        }

        protected override string FormatEdgeAsDOT(Edge<T> edge)
        {
            string fromID = "v_" + edge.From.GetHashCode();
            string toID = "v_" + edge.To.GetHashCode();
            return fromID + " -- " + toID + "[label=" + edge.Weight + "]\n";
        }

        public override void RemoveEdge(T from, T to)
        {
            int nFrom = this.vertexIndices[from];
            int nTo = this.vertexIndices[to];
            this.edgeMatrix[nFrom, nTo] = null;
            this.edgeMatrix[nTo, nFrom] = null;
        }
    }
}
