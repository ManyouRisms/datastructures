﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{

    public delegate void VisitorDelegate<T>(T data);

    public interface IGraph<T> where T : IComparable<T>
    {
        #region Vertex-related Methods      


        // to the outside world, we just store some sort of outside data
        void AddVertexFor(T data);

        // see if the graph has a given vertex for a passed piece of data
        bool HasVertexFor(T data);

        // remove some vertex the represents some piece of data
        void RemoveVertexFor(T data);

        // will be important later
        Edge<T> GetEdge(T from, T to);

        // get some vertex the represents some piece of data
        Vertex<T> GetVertexFor(T data);


        // get all the vertices in the graph and return it
        IEnumerable<Vertex<T>> EnumerateVertices();

        IEnumerable<Vertex<T>> EnumerateNeighborsOfVertexFor(T data);

        #endregion

        #region Edge-related Methods
        
        // need a way to relate the vertices to eachother

        // add an edge between the vertices that represent the passed in items
        void AddEdge(T from, T to);


        void AddEdge(T from, T to, double weight);

        // way to see if there is in fact an edge between two items
        bool HasEdge(T from, T to);

        void RemoveEdge(T from, T to);

        // see all of the edges present in a graph ( the collection of all of them )
        IEnumerable<Edge<T>> EnumerateEdges();

        #endregion

        #region Graph Operations

        // four traversals

        void DepthFirstTraversal(T whereToStart, VisitorDelegate<T> whatToDo);

        void BreadthFirstTraversal(T whereToStart, VisitorDelegate<T> whatToDo);

        // basically returns edges and vertices, so we return them as a graph
        IGraph<T> MinimumSpanningTree();

        // paths have a start and an end, need to know them
        IGraph<T> ShortestWeightedPath(T whereToStart, T whereToEnd);

        #endregion


    }
}
