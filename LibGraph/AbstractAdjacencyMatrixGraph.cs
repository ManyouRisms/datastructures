﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{
    public abstract class AbstractAdjacencyMatrixGraph<T> : IGraph<T> where T : IComparable<T>
    {

        // some way to store edges, 'from' will always be a row, 'to' will always be a column
        protected Edge<T>[,] edgeMatrix;

        // some way to store verticies
        protected List<Vertex<T>> vertices;

        // some way to tell where we can find edges for a given piece of data, just maps the data to indices.
        // specific to matrix implementaiton of a graph
        protected Dictionary<T, int> vertexIndices;


        public AbstractAdjacencyMatrixGraph()
        {
            // no verticies to store, should be empty in both directions
            this.edgeMatrix = new Edge<T>[0, 0];

            this.vertexIndices = new Dictionary<T, int>();

            this.vertices = new List<Vertex<T>>();

        }


        // lets you define methods that only run when this objec t is run in a certain context. If you have 
        // for example IGraph graph = new AdjacencyMatrixGraph, it would call this explicit add vertex for on that 
        // object.
        // public void IGraph<T>.AddVertexFor(T data)
        // {
        //     throw new NotImplementedException();
        // }


        // trying to make as generic as possible. 
        // our steps to add here were expand edge storage (matrix), add mapping for the data, and adding vertex
        // to the list of vertices. we should pull out the storage of vertices and edges
        public void AddVertexFor(T data)
        {
            // if this data is already in the graph
            if (this.HasVertexFor(data))
            {
                // if you've provided a dll to someone with this, give them an idea of what they did wrong
                // or they would have no idea.
                throw new ApplicationException("Vertex for data " + data.ToString() + " already exists.");
            }
            // we error checked a bit, first step is to add it to the list of vertexes.
            Vertex<T> newVertex = new Vertex<T>(data);
            this.vertices.Add(newVertex);

            // nothing thus far is specific to how matrixes are implemented. doesn't
            // care how we store edges or anything. relies on things common to all classes

            // in some way set up the storage of edges to set it up for the vertex. This method
            // doesn't care how it does that. since this is an adjacencymatrix, we will implement it
            // according to the adjacencymatrix
            this.AddVertexAdjustEdgeStorage(newVertex);
        }

        protected void AddVertexAdjustEdgeStorage(Vertex<T> vertex)
        {
            // how do we increase the size of an array?
            // map between the data we're storing and the index at which we'll find it in the other two data structures

            // increase the size of the edge matrix
            //create larger 2d array (old plus 1)
            //copy values from old to new
            //replace the old array


            int currFrom = this.edgeMatrix.GetLength(0);
            int currTo = this.edgeMatrix.GetLength(1);
            Edge<T>[,] tempMatrix = new Edge<T>[currFrom + 1, currTo + 1];

            for (int i = 0; i < currFrom; i++)
            {
                for (int j = 0; j < currTo; j++)
                {
                    tempMatrix[i, j] = this.edgeMatrix[i, j];
                }
            }

            this.vertexIndices.Add(vertex.Data, currFrom);

            this.edgeMatrix = tempMatrix;

        }

        public bool HasVertexFor(T data)
        {
            bool returnVal = false;
            if (this.vertexIndices.ContainsKey(data))
            {
                returnVal = true;
            }
            return returnVal;
        }

        // do as an assignment
        public void RemoveVertexFor(T data)
        {
            // check for and remove edges for the data
            RemoveEdgesForData(data);


            // if the data has a vertex
            if (this.HasVertexFor(data))
            {
                // get the index where it's at in the vertices list
                int removeIndex = this.vertexIndices[data];

                // then remove it from the indicies dictionary
                this.vertexIndices.Remove(data);

                // as well as turf it from the list
                this.vertices.RemoveAt(removeIndex);
            }
            
            // otherwise nothing left to do
            else
            {
                throw new ApplicationException("Vertex for data " + data.ToString() + " doesn't exist.");
            }
        }

        public void RemoveEdgesForData(T data)
        {
            if (this.HasVertexFor(data))
            {
                int fromIndex = this.vertexIndices[data];
                // this.RemoveEdge(data.);
            }
        }


        public Edge<T> GetEdge(T from, T to)
        {
            if (!this.HasEdge(from, to))
            {
                throw new ApplicationException("No edge between " + from.ToString() + " and " + to.ToString());
            }
            int fromIndex = this.vertexIndices[from];
            int toIndex = this.vertexIndices[to];
            return this.edgeMatrix[fromIndex, toIndex];

        }

        public Vertex<T> GetVertexFor(T data)
        {

            if (!this.HasVertexFor(data))
            {
                throw new ApplicationException("No vertex for data " + data.ToString());
            }
            // find the index for the given data
            int index = this.vertexIndices[data]; // it's not an array but we can do this in c# cuz its the tits

            // return the vertex from vertices at the index
            return vertices[index];
        }

        public IEnumerable<Vertex<T>> EnumerateVertices()
        {
            // if we want a list of all the vertices in the class
            return this.vertices;

        }

        public IEnumerable<Vertex<T>> EnumerateNeighborsOfVertexFor(T data)
        {
            if (!this.HasVertexFor(data))
            {
                throw new ApplicationException("No vertex for");
            }
            List<Vertex<T>> neighbors = new List<Vertex<T>>();
            int indexOfData = this.vertexIndices[data];

            for (int i = 0; i < this.edgeMatrix.GetLength(1); i++)
            {
                if (edgeMatrix[indexOfData, i] != null)
                {
                    neighbors.Add(edgeMatrix[indexOfData, i].To);
                }
            }
            return neighbors;
        }

        public void AddEdge(T from, T to)
        {
            this.AddEdge(from, to, Double.PositiveInfinity);
        }

        public virtual void AddEdge(T from, T to, double weight)
        {
            // make sure both vertices exist to create an edge.
            // also make sure there isn't already an edge between those vertices
            if (!HasVertexFor(from) || !HasVertexFor(to))
            {
                throw new ApplicationException("No vertex for " + from.ToString() + " or " + to.ToString());
            }
            if (HasEdge(from, to))
            {
                throw new ApplicationException("Edge between " + from.ToString() + " and " + to.ToString() + " already exists.");
            }

            // need some placeholders
            Vertex<T> fromVertex = this.GetVertexFor(from);
            Vertex<T> toVertex = this.GetVertexFor(to);
            Edge<T> newEdge = new Edge<T>(fromVertex, toVertex, weight);

            this.AddEdge(newEdge);
            // now some implementation specific shit
        }

        protected void AddEdge(Edge<T> edge)
        {
            int fromIndex = this.vertexIndices[edge.From.Data];
            int toIndex = this.vertexIndices[edge.To.Data];

            this.edgeMatrix[fromIndex, toIndex] = edge;

        }



        public bool HasEdge(T from, T to)
        {
            // lookup indexes for data in dictionary
            // use those to check vertexindices to see
            // see if the value the users are giving us are completely frigged or not
            if (!this.HasVertexFor(from))
            {
                throw new ApplicationException("No vertex for " + from.ToString());
            }
            if (!this.HasVertexFor(to))
            {
                throw new ApplicationException("No vertex for " + to.ToString());
            }

            // find out where to look
            int fromIndex = this.vertexIndices[from];
            int toIndex = this.vertexIndices[to];

            return this.edgeMatrix[fromIndex, toIndex] != null;

        }

        public abstract void RemoveEdge(T from, T to);


        public abstract IEnumerable<Edge<T>> EnumerateEdges();

        // could make these have the same interface (have a wrapper around it)
        // interface describes a collection that is ordered, has add and remove,
        // implement in two different ways.
        // breadth-first takes in an 'iOrderable' parameter



        // not looking for specific node, don't have a root to start with, start somewhere and visit nodes along the way
        // use the delegate to manipulate each vertex. both methods for traversing will be iterative
        // as it uses resources better
        public void DepthFirstTraversal(T whereToStart, VisitorDelegate<T> whatToDo)
        {
            // Initialize data structures
            // visitedVertices
            List<Vertex<T>> visitedVertices = new List<Vertex<T>>();
            // verticesToVisit
            Stack<Vertex<T>> verticesToVisit = new Stack<Vertex<T>>();
            // get the vertex for where to start
            Vertex<T> startVertex = this.GetVertexFor(whereToStart);
            // add vertex to 'ToVisit'
            verticesToVisit.Push(startVertex);
            // while there are more vertices in 'ToVisit'
            while (verticesToVisit.Count > 0)
            {
                // get the next vertex from 'ToVisit' (depth-first, this is the most recently added vertex)
                Vertex<T> currentVertex = verticesToVisit.Pop();
                // has this vertex been visited? if not
                if (!visitedVertices.Contains(currentVertex))
                {
                    // visit the vertex
                    whatToDo(currentVertex.Data);
                    // mark as visited (add to visited list)
                    visitedVertices.Add(currentVertex);

                    // get the neighbors of the current vertex
                    // inspect each neighbor 
                    foreach (Vertex<T> neighbor in this.EnumerateNeighborsOfVertexFor(currentVertex.Data))
                    {
                        // if current neighbor hasn't been visited + is not in the list to be visited
                        if (!visitedVertices.Contains(neighbor) && !verticesToVisit.Contains(neighbor))
                        {
                            // add to list of vertices to visit
                            verticesToVisit.Push(neighbor);
                        }
                    }
                }
            }
        }

        /*
         * Depth first traversal recursively. Starts at a given piece of data,
         * traverses depth-first processing it with delegate passed in.
         */
        public void DepthFirstTraversalRec(T whereToStart, VisitorDelegate<T> whatToDo)
        {
            // create the visitedVertices once for effiency
            List<Vertex<T>> visitedVertices = new List<Vertex<T>>();
            
            // vertex from data passed
            Vertex<T> start = GetVertexFor(whereToStart);
            
            // if it exists, start recursion
            if (start != null)
            {
                RecVisit(start, whatToDo, visitedVertices);

            }
        }

        /**
         * Private helper for depthfirstrec
         */
        private void RecVisit(Vertex<T> start, VisitorDelegate<T> whatToDo, List<Vertex<T>> visitedVertices)
        {
            // if 'start' isn't already visited
            if (!visitedVertices.Contains(start))
            {
                // process the data
                whatToDo(start.Data);
                // add it to the visited list
                visitedVertices.Add(start);
                // for all neighbors of 'start'
                foreach (var neighbor in EnumerateNeighborsOfVertexFor(start.Data))
                {
                    // visit them
                    RecVisit(neighbor, whatToDo, visitedVertices);
                }
            }
        }

        public void BreadthFirstTraversal(T whereToStart, VisitorDelegate<T> whatToDo)
        {
            // Initialize data structures
            // visitedVertices
            List<Vertex<T>> visitedVertices = new List<Vertex<T>>();
            // verticesToVisit
            Queue<Vertex<T>> verticesToVisit = new Queue<Vertex<T>>();
            // get the vertex for where to start
            Vertex<T> startVertex = this.GetVertexFor(whereToStart);
            // add vertex to 'ToVisit'
            verticesToVisit.Enqueue(startVertex);
            // while there are more vertices in 'ToVisit'
            while (verticesToVisit.Count > 0)
            {
                // get the next vertex from 'ToVisit' (breadth-first, this is the least recently added vertex)
                Vertex<T> currentVertex = verticesToVisit.Dequeue();

                //File.WriteAllText("p:\\public_html\\graph.txt", this.ToTraversalDOT(currentVertex, null, visitedVertices, verticesToVisit.ToList()));
                ;

                // has this vertex been visited? if not
                if (!visitedVertices.Contains(currentVertex))
                {
                    // visit the vertex
                    whatToDo(currentVertex.Data);
                    // mark as visited (add to visited list)
                    visitedVertices.Add(currentVertex);
                    // get the neighbors of the current vertex
                    // inspect each neighbor 
                    foreach (Vertex<T> neighbor in this.EnumerateNeighborsOfVertexFor(currentVertex.Data))
                    {
                        //File.WriteAllText("p:\\public_html\\graph.txt", this.ToTraversalDOT(currentVertex, neighbor, visitedVertices, verticesToVisit.ToList()));
                        ;
                        // if current neighbor hasn't been visited + is not in the list to be visited
                        if (!visitedVertices.Contains(neighbor) && !verticesToVisit.Contains(neighbor))
                        {
                            // add to list of vertices to visit
                            verticesToVisit.Enqueue(neighbor);
                        }
                    }
                }
            }
        }


        public IGraph<T> MinimumSpanningTree()
        {
            //create a collection of graphs, one for each vertex in the source graph, each containing one vertex
            // the forest
            // create some sorted collection of edges containing all edges from the source graph. they must be
            // retievable in order from lowest cost to highest cost
            // loop while created tree is not spanning (there is more than one remaining tree in the forest), and we 
            // haven't run out of edges
            // ...

            List<IGraph<T>> forest = new List<IGraph<T>>();
            // for each vertex that makes up the vertices in this graph
            foreach (Vertex<T> currentVertex in this.EnumerateVertices())
            {
                IGraph<T> newGraph = (IGraph<T>)GetType().Assembly.CreateInstance(GetType().FullName);
                newGraph.AddVertexFor(currentVertex.Data);
                forest.Add(newGraph);

                // don't have to use a priority queue, but will because we have it.
                // could also use an array and sort it or something
            }
            
            IPriorityQueue<Edge<T>> edgeQueue = new SimplePriorityQueue<Edge<T>>();
            foreach (Edge<T> currentEdge in this.EnumerateEdges())
            {
                edgeQueue.Enqueue(currentEdge);
            }

            while (forest.Count > 1 && !edgeQueue.IsEmpty())
            {
                // get the next lowest weighted edge

                // find out if the two sides (to and from) of that edge are in separate graphs
                // different ways, could take edge between two vertices, find graph out of 
                // forest that contains c, examine to see if list of vertices contains both

                // could also go through forest, find graph that contains c, get a pointer
                // to the graph object, go through the list, get a pointer to the list that
                // contains b, if they point to the same place (are the same object), then
                // they contain both b and c
                // we will do the second method

                // get graph object for to and from, see if they are the same

                // if the graph objects are NOT the same, then it is safe to add the edge
                // merge graph containing to with graph containing from
                // remove the extra graph
                // return something
                Edge<T> nextLowestEdge = edgeQueue.Dequeue();
                IGraph<T> fromTree = GetGraphContainingVertex(forest, nextLowestEdge.From);
                IGraph<T> toTree = GetGraphContainingVertex(forest, nextLowestEdge.To);
                ;
                if (fromTree != toTree) // a trap! 
                {
                    // merge graphs together
                    MergeGraphs(fromTree, toTree, nextLowestEdge);

                    // remove the extra graph
                    // timber! chainsaw! battleaxe
                    forest.Remove(fromTree);
                }

            }
                // we may or may not have a mst, look at loop condition
                if (forest.Count > 1)
                {
                    throw new ApplicationException("Couldn't create the minimum spanning tree");
                }

            return forest[0]; // return the only thing in the forest
        }


        // do this
        private void MergeGraphs(IGraph<T> source, IGraph<T> destination, Edge<T> connetingEdge)
        {
            foreach (Vertex<T> fromSource in source.EnumerateVertices())
            {
                destination.AddVertexFor(fromSource.Data);
            }

            foreach (Edge<T> fromSource in source.EnumerateEdges())
            {
                destination.AddEdge(fromSource.From.Data, fromSource.To.Data, fromSource.Weight);
            }

            destination.AddEdge(connetingEdge.From.Data, connetingEdge.To.Data, connetingEdge.Weight);

            // copy all the vertices from source to destination          

            // copy all of the edges from source to destination (ie add edge to dest for each edge in source)

            // add a new edge to connect the two sides (a copy of connecting edge), and edge from
            // and to, with the same weight of connectingEdge
            // add a new edge to connect the two sides (recreate connectingEdge in dest)

        }

        // cheap and easy way, loop through list of graphs
        // if current graph contains vertexToFind (or vertex representing same data)
        // return the current graph
        // else
        // explode
        // return the graph object you find from the list, or the 
        // fromTree != toTree won't work in the above method
        // DONT CREATE A NEW ONE! lollololol
        protected IGraph<T> GetGraphContainingVertex(List<IGraph<T>> forest, Vertex<T> vertexToFind)
        {
            foreach (var graph in forest)
            {
                if (graph.HasVertexFor(vertexToFind.Data))
                {
                    return graph;
                }
            }

            throw new ApplicationException("Vertex not found in existing graphs");



        }
        public IGraph<T> ShortestWeightedPath(T whereToStart, T whereToEnd)
        {
            // get vertex for will throw an exception otherwise
            Vertex<T> startVertex = this.GetVertexFor(whereToStart);
            Vertex<T> endVertex = this.GetVertexFor(whereToEnd);

            // list of all vertices, some enumerable list
            IEnumerable<Vertex<T>> allVertices = this.EnumerateVertices();

            // need our priority queue
            IPriorityQueue<VertexData> vertexQueue = new SimplePriorityQueue<VertexData>();

            //hashtable will map our vertices onto the vertexdata object that represents the extra
            // data about that vertex.
            // a way to find the vertexData object for a given vertex, since things like EnumerateNeighbors
            // don't return vertexData objects
            Dictionary<Vertex<T>, VertexData> vertexMap = new Dictionary<Vertex<T>, VertexData>();

            // for each vertex in this graph, keep track of some extra data
            foreach (Vertex<T> currentVertex in allVertices)
            {
                VertexData vertexWrapper;

                // == because we want it to be the same object in memory
                if (currentVertex == startVertex)
                {
                    // beginning vertex is 0 points of cost from the start
                    vertexWrapper = new VertexData(currentVertex, 0);
                }
                else
                {
                    // doing this way instead of in the constructor if not specified shows us better
                    // what is going on here.
                    // initially, every other vertex other than the start has an unknown shortest
                    // distance, so we set it to infinity.
                    vertexWrapper = new VertexData(currentVertex, Double.PositiveInfinity);
                }

                vertexQueue.Enqueue(vertexWrapper);
                // add to dictionary so we can find vertex data given an object
                vertexMap.Add(currentVertex, vertexWrapper);

                // while there are vertices left to process (something left in the queue) which will always be true
                // the loop will break out before its empty if it's gotten this far
                while (!vertexQueue.IsEmpty())
                {
                    // get next vertex from queue and set to current
                    VertexData vDataCurrent = vertexQueue.Dequeue();
                    vDataCurrent.visited = true;

                    // if the current vertex has a tentative distance of infinity
                    if (Double.IsPositiveInfinity(vDataCurrent.tentative_distance))
                    {
                        // explode because there is no shortest path to the target vertex
                        throw new ApplicationException("There is no path to vertex" + endVertex.ToString());
                    }

                    // if the current vertex is the target
                    if (vDataCurrent.vertex == endVertex)
                    {
                        // we've found the shortest path, so reconstruct the graph representing the shortest path
                        // the edges and vertices of the shortest path 
                        break;
                    }

                    // list of all the neighbors of the current vertex
                    IEnumerable<Vertex<T>> neighbors = this.EnumerateNeighborsOfVertexFor(vDataCurrent.vertex.Data);

                    // for each of its neighbors
                    foreach (Vertex<T> currentNeighbor in neighbors)
                    {
                        // if the neighbor is unvisited
                        VertexData vDataCurrentNeighbor = vertexMap[currentNeighbor];
                        if (!vDataCurrentNeighbor.visited)
                        {
                            // check to see if we have a shorter path to that neighbor through current than 
                            // previously discovered, current.td + edgeweight < neighbor td
                            Edge<T> edgeBetween = this.GetEdge(vDataCurrent.vertex.Data, currentNeighbor.Data);
                            if (vDataCurrent.tentative_distance + edgeBetween.Weight < vDataCurrentNeighbor.tentative_distance)
                            {
                                // if we have a shorter path, update neighbor td and previous pointer
                                vDataCurrentNeighbor.tentative_distance = vDataCurrent.tentative_distance + edgeBetween.Weight;
                                // because thats how  you get to that neighbor
                                vDataCurrentNeighbor.previous = vDataCurrent;
                            }
                        }
                    }
                }
            }
            return ReconstructShortestPathGraph(vertexMap[endVertex]);
        }

        protected IGraph<T> ReconstructShortestPathGraph(VertexData endPoint)
        {
            IGraph<T> newGraph = (IGraph<T>)
                                GetType().Assembly.CreateInstance(GetType().FullName);

            //Add a vertex to represent the end point, so that there will always be at least one
            //involved vertex in the graph when we start looping
            newGraph.AddVertexFor(endPoint.vertex.Data);

            //Start the loop at the end point
            VertexData vDataCurrent = endPoint;

            //While the current vertex has a previous vertex
            while (vDataCurrent.previous != null)
            {
                //Get the VertexData object for the previous vertex
                VertexData vDataPrevious = vDataCurrent.previous;

                //Get the vertex objects for the current and previous vertices
                Vertex<T> previousVertex = vDataPrevious.vertex;
                Vertex<T> currentVertex = vDataCurrent.vertex;

                //Add a vertex for the "previous" vertex
                newGraph.AddVertexFor(previousVertex.Data);

                //Now that both the current and previous vertices exist in the new graph, we can add
                //an edge between them. Make sure to add the edge in the right direction, as the 
                //chain of previous pointers is actually the opposite of the path, and to add the
                //edge weights to match the existing edges.
                Edge<T> existingEdge = this.GetEdge(previousVertex.Data, currentVertex.Data);
                newGraph.AddEdge(previousVertex.Data, currentVertex.Data, existingEdge.Weight);

                //Move on to inspect the next vertex
                vDataCurrent = vDataPrevious;
            }
            return newGraph;
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Vertices:\n");
            foreach (Vertex<T> v in this.vertices)
            {
                sb.Append(this.vertexIndices[v.Data] + ": " + v.ToString() + "\n");
            }

            sb.Append("\n\nEdge matrix (from in rows, to in columns)\n\t");
            for (int i = 0; i < this.edgeMatrix.GetLength(1); i++)
            {
                sb.Append(i + ":\t");
            }
            sb.Append("\n");
            for (int from = 0; from < this.edgeMatrix.GetLength(0); from++)
            {
                sb.Append(from + ":\t");
                for (int to = 0; to < this.edgeMatrix.GetLength(1); to++)
                {
                    Edge<T> currentEdge = this.edgeMatrix[from, to];
                    if (currentEdge != null)
                    {
                        sb.Append("(" + currentEdge.From + "," + currentEdge.To);
                        if (currentEdge.Weight != Double.PositiveInfinity)
                        {
                            sb.Append("," + currentEdge.Weight);
                        }
                        sb.Append(")\t");
                    }
                    else
                    {
                        sb.Append("\t");
                    }
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }
        protected abstract string GetDOTType();


        // DOT represents edges on directed and undirected graphs differently
        protected abstract string FormatEdgeAsDOT(Edge<T> edge);

        /// <summary>
        /// Format the graph using DOT syntax.
        /// DOT syntax consists of a list of vertex identifiers
        /// then a list of edges between those vertices
        /// http://en.wikipedia.org/wiki/DOT_(graph_description_language)
        /// http://www.webgraphviz.com/
        /// </summary>
        /// <returns></returns>
        public string ToDOT()
        {
            IEnumerable<Edge<T>> edges = this.EnumerateEdges();
            IEnumerable<Vertex<T>> vertices = this.EnumerateVertices();
            StringBuilder builder = new StringBuilder();
            builder.Append(this.GetDOTType() + " cstgraph { \n");

            foreach (Vertex<T> currentVertex in vertices)
            {
                builder.Append("v_" + currentVertex.GetHashCode() + " [label=\"" + currentVertex.ToString() + "\"]\n");
            }

            foreach (Edge<T> currentEdge in edges)
            {
                builder.Append(this.FormatEdgeAsDOT(currentEdge));
            }
            builder.Append("}");
            return builder.ToString();
        }

        /// <summary>
        /// Generate DOT to display the graph, highlighting the important sections which
        /// apply while performing a traversal
        /// </summary>
        /// <param name="currentlyVisiting">The node we're currently visiting</param>
        /// <param name="currentlyInspecting">The neighbor of current we're inspecting</param>
        /// <param name="visitedVertices">The list of all previously visited vertices</param>
        /// <param name="toVisit">The list of neighbors of previously visited vertices</param>
        /// <returns></returns>
        public string ToTraversalDOT(Vertex<T> currentlyVisiting, Vertex<T> currentlyInspecting, List<Vertex<T>> visitedVertices, List<Vertex<T>> toVisit)
        {
            IEnumerable<Edge<T>> edges = this.EnumerateEdges();
            IEnumerable<Vertex<T>> vertices = this.EnumerateVertices();
            StringBuilder builder = new StringBuilder();
            builder.Append(this.GetDOTType() + " cstgraph { \n");

            foreach (Vertex<T> currentVertex in vertices)
            {
                builder.Append("v_" + currentVertex.GetHashCode() + " [label=\"" + currentVertex.ToString());
                List<string> fillColours = new List<string>();
                if (currentVertex.CompareTo(currentlyVisiting) == 0)
                {
                    fillColours.Add("yellow");
                    //builder.Append(" color=yellow style=filled");
                }
                if (visitedVertices.Contains(currentVertex))
                {
                    fillColours.Add("red");
                    //builder.Append(" color=red style=filled");
                }
                if (toVisit.Contains(currentVertex))
                {
                    builder.Append(":" + toVisit.IndexOf(currentVertex));
                    fillColours.Add("green");
                    //builder.Append(" color=green style=filled");
                }
                if (currentlyInspecting != null && currentVertex.CompareTo(currentlyInspecting) == 0)
                {
                    if (fillColours.Count < 1)
                    {
                        fillColours.Add("white");
                    }
                    fillColours.Add("blue");
                    //builder.Append(" color=yellow style=filled");
                }
                if (fillColours.Count < 1)
                {
                    fillColours.Add("white");
                }
                builder.Append("\" fillcolor=\"" + string.Join(":", fillColours.ToArray()) + "\" style=\"filled\" gradientangle=\"0\"");

                builder.Append("]\n");
            }

            foreach (Edge<T> currentEdge in edges)
            {
                builder.Append(this.FormatEdgeAsDOT(currentEdge));
            }
            builder.Append("}");
            return builder.ToString();

        }

        protected internal class VertexData : IComparable<VertexData>
        {
            public double tentative_distance;
            public Vertex<T> vertex;
            public bool visited;
            public VertexData previous;

            public VertexData(Vertex<T> vertex)
                : this(vertex, Double.PositiveInfinity)
            {

            }

            public VertexData(Vertex<T> vertex, double tentative_distance)
            {
                this.tentative_distance = tentative_distance;
                this.vertex = vertex;
                this.visited = false;
                this.previous = null;
            }

            public int CompareTo(VertexData other)
            {
                return this.tentative_distance.CompareTo(other.tentative_distance);
            }
        }

        public void ClearGraph()
        {
            this.vertices.Clear();
            this.vertexIndices.Clear();
            int length1 = this.edgeMatrix.GetLength(0);
            int length2 = this.edgeMatrix.GetLength(1);

            for (int i = 0; i < length1; i++)
            {
                for (int j = 0; j < length2; j++)
                {
                    this.edgeMatrix[i, j] = null;
                }
            }

        }


    }

}
