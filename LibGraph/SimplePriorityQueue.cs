﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGraph
{
    public class SimplePriorityQueue<T> : IPriorityQueue<T> where T : IComparable<T>
    {
        private List<T> items;
        public SimplePriorityQueue()
        {
            this.items = new List<T>();
        }

        public void Enqueue(T data)
        {
            this.items.Add(data);
            this.items.Sort();
        }

        public T Dequeue()
        {
            if (this.items[0] != null)
            {
                T returnItem = this.items[0];
                items.RemoveAt(0);
                return returnItem;
            }
            else
            {
                throw new ApplicationException("List is empty");
            }

        }

        public bool IsEmpty()
        {
            return this.items.Count < 1;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("[");
            foreach(T item in this.items)
            {
                builder.Append("{");
                builder.Append(item.ToString());
                builder.Append("},");
            }
            builder.Append("]");
            return builder.ToString();
        }
    }
}
