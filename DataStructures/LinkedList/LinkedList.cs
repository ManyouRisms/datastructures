﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataStructuresCommon;

namespace LinkedList
{
    // the following class will implement the list interface as a singley linked list. this means that
    // we can traverse the list from the head node to the tail node in that direction only. Not a sorted 
    // list, so additions occur at the tail node.
    public class LinkedList<T> : A_List<T> where T : IComparable<T>
    {
        #region attributes

        // need reference to the head node 
        private Node head;
        #endregion

        #region constructors

        public LinkedList()
        {
            head = null; //head node will not be set (defaults to null anyways, but meh, 
        }
        #endregion


        #region enumerator
        private class Enumerator : IEnumerator<T>
        {

            //A reference to the linked list
            private LinkedList<T> parent;
            private Node lastVisited; //The current node that we visited
            private Node scout; //the next node to visit

            public Enumerator(LinkedList<T> parent)
            {
                this.parent = parent;
                Reset();
            }

            //Generic version of Current
            public T Current
            {
                get { return lastVisited.data; }
            }

            public void Dispose()
            {
                parent = null;
                scout = null;
                lastVisited = null;
            }

            //Object version of Current
            object System.Collections.IEnumerator.Current
            {
                get { return lastVisited.data; }
            }

            public bool MoveNext()
            {
                bool result = false;
                if (scout != null)
                {
                    result = true;
                    lastVisited = scout;
                    scout = lastVisited.next;

                }
                return result;
            }

            public void Reset()
            {
                //Set the node currently being looked at to null
                lastVisited = null;
                //Set the scout to the head of the list
                scout = parent.head;
            }
        }
        #endregion
        #region Node class
        private class Node
        {
            #region attributes

            // a reference to the next node
            public Node next;
            // a reference to the data stored by the node
            public T data;
            #endregion

            #region Constructors
            // constructor chaining
            public Node(T data)
                : this(data, null) // constructor chaining in c#
            {

            }

            public Node(T data, Node next)
            {
                this.data = data;
                this.next = next;
            }
            #endregion

        }

        public override void Insert(int index, T data)
        {
            // this will recurse to get to the right position

            // if the index is greater than the count
            // throw a new exception
            //head <-- RecInsert(index, data, headnode)

            //if index is out of range
            if (index > this.Count())
            {
                throw new IndexOutOfRangeException("You went too far noob");
            }
            // otherwise call RecInsert
            head = RecInsert(index, data, head);
        }

        private Node RecInsert(int index, T data, Node current)
        {

            // if index is equal to zero
            // newNode = new node with data and current
            // 
            // else
            // currentNext = RecInsert(index-1, data, current's next)

            //return newNode


            // base case 
            if (index == 0)
            {
                current = new Node(data, current);
            }

            else
            {
                current.next = RecInsert(--index, data, current.next);
            }
            return current;
        }



        //given an index, find that index and remove that item, return type T (the data item in the node)
        // 3 ways of implementing a recursive method in the linkedlist class
        //1. return a node reference (as in the add method). might be an issue if you're already returning something
        //2. Passs the "next" node reference by reference (as in recremove)
        //3. "Look forward one node" as you iterate down. Check if the node ahead is the node you want to operate on, before recursing. 
            // this one requires checks on the head node before 

        public override T RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public override T ReplaceAt(int index, T data)
        {

            //given the index, iterate through until that index
                // if it exists, replace it's .data with data and return the data replaced
            if (index > this.Count() )
            {
                throw new IndexOutOfRangeException("You went too far noob");

            }

            return RecReplaceAt(head, index, data);
            
        }

        //
        private T RecReplaceAt(Node current, int index, T data)
        {
            T myT = default(T);
            if (index == 0)
            {
                // store the data we are replacing because we have to return it
                myT = current.data;
                // since we found the right one to replace, set it to what was passed in
                current.data = data;
            }
            else
            {
                //we werent at our index, so push it down the stack until we find it
                myT = RecReplaceAt(current.next, --index, data);

            }
            return myT;
        }

        public override void Add(T data)
        {
            head = RecAdd(head, data);
        }

        private Node RecAdd(Node current, T data)
        {
            // base case
            if (current == null)
            {
                current = new Node(data);
            }

            //recursive case
            else
            {
                // recurse to next node
                current.next = RecAdd(current.next, data);
            }

            return current;
        }

        public override void Clear()
        {
            head = null;
        }

        // return recremove
        public override bool Remove(T data)
        {
            
            return RecRemove(ref head, data);
        }

        private bool RecRemove(ref Node current, T data)
        {
            //recremove --> returns a boolean
            // bool found = false;
            //if current is null
            // return false
            // else if node contains data to be removed
            // current = currents next 
            // else
            //found = recremove(current's next by reference, data)
            // return found

            bool found = false;
            if (current != null)
            {
                if (found = current.data.Equals(data))
                {
                    current = current.next; // the part that actually removes the node
                }
                else
                {
                    found = RecRemove(ref current.next, data); // passing reference of a reference
                }
            }
            return found;  

        }



        public override IEnumerator<T> GetEnumerator()
        {
            return new Enumerator(this);
        }
    }

        #endregion

}

