﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList<int> ll = new LinkedList<int>();
            ll.Add(3);
            ll.Add(7);
            ll.Add(5);

        //    Console.WriteLine(ll.ToString());

            ll.Insert(0, 9);
            ll.Insert(2, 10);
            ll.Insert(ll.Count, 17);

           // ll.Remove(9);
            //ll.Remove(5);
            //ll.Remove(17);


            Console.WriteLine(ll.ToString());

            ll.ReplaceAt(0, 22);

            Console.WriteLine(ll.ToString());

            ll.ReplaceAt(8, 99);

            Console.WriteLine(ll.ToString());




        }
    }
}
