﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataStructuresCommon;

namespace LinkedList
{
    public abstract class A_List<T> : A_Collection<T>, I_List<T> where T: IComparable<T>
    {

        public T ElementAt(int index)
        {
            int count = 0;
            // get an enumerator
            IEnumerator<T> myEnum = GetEnumerator();
            //reset the enumerator
            myEnum.Reset();

            // while there are more data items to check AND not at correct index
            //      increment the counter
            while (myEnum.MoveNext() && count != index)
            {
                    count++;                       
            }

            // if the count is beyond the index in the collection, 
            if (count > index)
            {
                throw new IndexOutOfRangeException("Invalid index (Out of bounds)");
            }
                // return enumerators current data item
            return myEnum.Current;
        }

        //TODO come back to it
        public int IndexOf(T data)
        {
            //similar to elementat

            throw new NotImplementedException();
        }


        #region Abstract methods as they are implementation dependant
        public abstract void Insert(int index, T data);

        public abstract T RemoveAt(int index);


        public abstract T ReplaceAt(int index, T data);
        #endregion
    }
}
