﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataStructuresCommon;

namespace LinkedList
{
    // note that we compiled datastructurescommon to a dll. we then went to the linkedlist project and 
    // and right clicked references, added a reference to the dll. then inserted a 'using' line for it.
    public interface I_List<T> : I_Collection<T> where T: IComparable<T>
    {
        // define the List abstract data type:

        /// <summary>
        /// returns the element at a particular inded
        /// </summary>
        /// <param name="index">index of item to find</param>
        /// <returns>the element at index</returns>
        T ElementAt(int index);

        /// <summary>
        /// given a data element, return the index of the first item encountered.
        /// </summary>
        /// <param name="data">the item to find</param>
        /// <returns>THe index of the item to find</returns>
        int IndexOf(T data);

        /// <summary>
        /// add a data item at any index in the list`
        /// </summary>
        /// <param name="index">the location to add the item</param>
        /// <param name="data">Item to add</param>
        void Insert(int index, T data);

        /// <summary>
        /// removes an element at a given index
        /// </summary>
        /// <param name="index">index of the element to remove</param>
        /// <returns>the data item that was removed</returns>
        T RemoveAt(int index);

        /// <summary>
        /// find a data item at a particular location and replace it with an item passed in
        /// </summary>
        /// <param name="index">position of the item to be replaced</param>
        /// <param name="data">new data to replace old item with</param>
        /// <returns>existing data</returns>
        T ReplaceAt(int index, T data);
    }
}
