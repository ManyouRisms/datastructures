﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructuresCommon
{
    public abstract class A_Collection<T> : I_Collection<T> where T : IComparable<T>
    {

        #region I_Collection implementation

        #region Abstract methods
        public abstract void Add(T Data);
        public abstract void Clear();
        public abstract bool Remove(T data);
        #endregion

        #region Implementable methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Contains(T data)
        {
            bool found = false;
            // get instance of enumerator
            IEnumerator<T> myEnum = GetEnumerator();
            myEnum.Reset();
            
            // loop through structure while data is not found, and while
            // there are still items to check
            while (!found && myEnum.MoveNext())
            {
                // compare the item passed in to the current item of the enumerator
                found = myEnum.Current.Equals(data);
            }

            return found;
        }

        /// <summary>
        /// recall that count is a property in C#, similar to a getter / setter.
        /// note that the following implementation may not be too efficient on larger
        /// data structures, as we loop through the entire structure to calculate count.
        /// therefore we want the ability to override this property in a child implementation.
        /// the 'virtual' keyword allows this to occur.
        /// 
        /// </summary>
        public virtual int Count
        {
            get 
            {
                int count = 0;
                // the foreach loop requires that the collection (this) to be able to return an enumerator
                foreach (T item in this)
                {
                    count++;
                }
                return count;
            }
        }


        //Override the implementation of ToString
        public override string ToString()
        {
            StringBuilder result = new StringBuilder("[");
            string sep = ", ";
            foreach (T item in this)
            {
                result.Append(item + sep);
            }
            if (Count > 0)
            {
                result.Remove(result.Length - sep.Length, sep.Length);
            }
            result.Append("]");
            return result.ToString();
        }

        #endregion

        #endregion

        #region IEnumerable implementation
        public abstract IEnumerator<T> GetEnumerator();

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion
    }
}
