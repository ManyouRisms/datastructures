﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructuresCommon
{
    // the interface will implement IEnumerable.
    // the collection will use generics as well.
    // the generic we used (T) must be comparable, so we 
    //  use the where clause
    public interface I_Collection<T> : IEnumerable<T> where T: IComparable<T>
    {
        /// <summary>
        /// Adds the given data to this collection
        /// </summary>
        /// <param name="Data">Data to add</param>
        
        void Add(T Data);
        /// <summary>
        /// Removes all items from the collection
        /// </summary>
        void Clear();

        /// <summary>
        /// Check the collection to return whether the passed data exists
        /// </summary>
        /// <param name="data">The data item defined. </param>
        /// <returns>True if item is found, otherwise false</returns>
        bool Contains(T data);

        /// <summary>
        /// Remove the first instance of the value if it exists
        /// </summary>
        /// <param name="data">Data to remove</param>
        /// <returns>True if found and removed, otherwise false.</returns>
        bool Remove(T data);

        /// <summary>
        /// Property to access the number of elements int he collection
        /// </summary>
        int Count
        {
            get;
        }

        /// <summary>
        /// Determines if this data structure is equal to another
        /// </summary>
        /// <param name="other">The passed in data structure to be compared with the calling structure</param>
        /// <returns>True if it is equal, otherwise false</returns>
        bool Equals(object other);


    }
}
