﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataStructuresCommon;

namespace BinarySearchTree
{
    public class BST<T> : A_BST<T> where T : IComparable<T>
    {
        // not really needed for csharp but might increase readability. these are set to defaults
        public BST()
        {
            this.nRoot = null;
            iCount = 0;

        }

        /// <summary>
        /// Note that the function is virtual and can be overridden in a child class to perform work.
        /// </summary>
        /// <param name="nCurrent"></param>
        /// <returns></returns>
        internal virtual Node<T> Balance(Node<T> nCurrent)
        {
            return nCurrent;
        }

        #region Assignment 1

        //the accessible method
        public int CountNodes()
        {
            //if im null return 0, otherwise recurse
            return (nRoot == null ? 0 : RecCountNodes(nRoot));
        }

        //the private method
        private int RecCountNodes(Node<T> nCurrent)
        {
            //count init at 1 to count myself
            int count = 1;

            // if my left child isn't null recurse to it
            if (nCurrent.Left != null) { count += RecCountNodes(nCurrent.Left); }

            //if my right child ilsn't null recurse to it
            if (nCurrent.Right != null) { count += RecCountNodes(nCurrent.Right); }

            return count;
        }

        #endregion

        #region A_BST implementation

        // publically accessible method
        public override T Find(T data)
        {
            T returnData = default(T); // default of T
            // return either data, or recurse if we missed it.
            returnData = ( nRoot.Data.CompareTo(data) == 0 
                ? nRoot.Data : RecFind(nRoot, data) );

            return returnData;
        }
        //private method
        private T RecFind(Node<T> nCurrent, T data)
        {
            T returnValue = default(T); //default of T
            if (nCurrent != null)
            {
                if (nCurrent.Data.CompareTo(data) == 0)
                { // found it
                    returnValue = nCurrent.Data;
                }
                else if (nCurrent.Data.CompareTo(data) < 0)
                { // going right
                    returnValue = RecFind(nCurrent.Right, data);
                }
                else
                { // going left
                    returnValue = RecFind(nCurrent.Left, data);
                }
            }
            else
            {
                throw new ApplicationException("Item not found in tree");
            }
            return returnValue;
        }

        public override int Height()
        {
            //throw new NotImplementedException();
            if (nRoot != null)
            {
                return RecHeight(nRoot);
            }
            else
            {
                throw new ApplicationException("shes frigged");
            }
        }


        //Alg:
        //heightLeft <= 0
        //heightRight <= 0
        //heightCurrent <= 0

        //If current node is a leaf node
        //    heightCurrent <- 0
        //Else
        //    If currents left exists
        //        heightLeft <- Recursively find height of left
        //    If current's right exists
        //        heightRight <- recursively find height of right
        //    If heightLeft > heightRight
        //        heightCurrent <- heightLeft
        //    Else
        //        heightCurrent <- heightRight

        protected int RecHeight(Node<T> nCurrent)
        {
            int heightLeft = 0;
            int heightRight = 0;
            int heightCurrent = 0;

            if (!nCurrent.IsLeaf())
            {

                if (nCurrent.Left != null)
                {
                    heightLeft = RecHeight(nCurrent.Left) + 1;
                }
                if (nCurrent.Right != null)
                {
                    heightRight = RecHeight(nCurrent.Right) + 1;
                }

                heightCurrent = (heightLeft > heightRight ? heightLeft : heightRight);
            }
            return heightCurrent;
        }

        public override void Iterate(ProcessData<T> pd, TRAVERSALORDER to)
        {
            if (nRoot != null)
            {
                RecIterate(nRoot, pd, to);
            }
        }


        //three traversal methods are in here, only thing that changes is when the node is processed.
        private void RecIterate(Node<T> nCurrent, ProcessData<T> pd, TRAVERSALORDER to)
        {
            //Pre order
            if (to == TRAVERSALORDER.PRE_ORDER)
            {
                pd(nCurrent.Data); // by using the delegate, I don't have to care what happens to the data.
                // now the use of the structure decides what to do with the data
            }


            if (nCurrent.Left != null)
            {
                RecIterate(nCurrent.Left, pd, to);
            }

            //In order
            if (to == TRAVERSALORDER.IN_ORDER)
            {
                pd(nCurrent.Data); // by using the delegate, I don't have to care what happens to the data.
                // now the use of the structure decides what to do with the data
            }

            if (nCurrent.Right != null)
            {
                RecIterate(nCurrent.Right, pd, to);
            }

            //Post order
            if (to == TRAVERSALORDER.POST_ORDER)
            {
                pd(nCurrent.Data); // by using the delegate, I don't have to care what happens to the data.
                // now the use of the structure decides what to do with the data
            }
        }

        // has two parts
        //  add
        //  recursiveadd
        //could do it the three ways, recurse to the right node, return its address
        // pass by reference,
        // or look forward to the next node, without recursing to it
        // downside is having to handle the root as a separate case


        //look forward method
        public override void Add(T Data)
        {
            if (nRoot == null)
            {
                nRoot = new Node<T>(Data);
            }
            //if root exists, start recursing.
            else
            {
                RecAdd(Data, nRoot);
                nRoot = Balance(nRoot);
            }

            iCount++;
        }


        private void RecAdd(T Data, Node<T> current)
        {

            if (Data.CompareTo(current.Data) < 0)
            {
                if (current.Left == null)
                {
                    current.Left = new Node<T>(Data);
                }
                else
                {
                    RecAdd(Data, current.Left);
                    // balance the left
                    current.Left = Balance(current.Left);

                }
            }
            else
            {
                if (current.Right == null)
                {
                    current.Right = new Node<T>(Data);
                }
                else
                {
                    RecAdd(Data, current.Right);
                    // balance the right
                    current.Right = Balance(current.Right);
                }
            }
        }


        public override void Clear()
        {
            throw new NotImplementedException();
        }

        public override bool Remove(T data)
        {
            bool wasRemoved = false;
            nRoot = RecRemove(nRoot, data, ref wasRemoved);
            nRoot = Balance(nRoot);
            return wasRemoved;

        }

        // not the lookahead method, therefore don't have to check before recursing, just recurse,and worry about things later
        private Node<T> RecRemove(Node<T> nCurrent, T data, ref bool wasRemoved)
        {
            // variable used to compare data items of type 'T'
            int iCompare = 0;
            if (nCurrent != null)
            {
                iCompare = data.CompareTo(nCurrent.Data);
                // if the item to remove is less than current's data.
                if (iCompare < 0)
                {
                    // recursively remove from current's left subtree
                    nCurrent.Left = RecRemove(nCurrent.Left, data, ref wasRemoved);
                    nCurrent.Left = Balance(nCurrent.Left);

                }
                else if (iCompare > 0)
                {
                    nCurrent.Right = RecRemove(nCurrent.Right, data, ref wasRemoved);
                    nCurrent.Right = Balance(nCurrent.Right);
                }
                else // found the node to remove
                {
                    wasRemoved = true; // indicate that we found the item

                    // check if the node is a leaf - ie no children
                    if (nCurrent.IsLeaf()) // is a leaf
                    {
                        nCurrent = null; // this will get returned to the parent node, and therefore it will be deleted
                        //reduce the count
                        iCount--;
                    }
                    else // not a leaf
                    {
                        if (nCurrent.Left != null)
                        {
                            //replace current data with the substitute. we have a method (recfindlargest) which will find the largest, we pass in the left subtree
                            // which will be a sufficient replacement
                            nCurrent.Data = RecFindLargest(nCurrent.Left);
                            // remove the substitute from the current's left subtree
                            nCurrent.Left = RecRemove(nCurrent.Left, nCurrent.Data, ref wasRemoved); // returns a reference, assign that reference to
                            nCurrent.Left = Balance(nCurrent.Left);
                        }
                        else // go the opposite way

                            nCurrent.Data = RecFindSmallest(nCurrent.Right);
                        nCurrent.Right = RecRemove(nCurrent.Right, nCurrent.Data, ref wasRemoved);
                        nCurrent.Right = Balance(nCurrent.Right);
                    }
                }
            }

            return nCurrent;
        }


        public override IEnumerator<T> GetEnumerator()
        {
            return new Enumerator(this);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Find smallest data item in the entire BST
        /// </summary>
        /// <returns>The smallest item</returns>
        public T FindSmallest()
        {
            if (nRoot == null)
            {
                throw new ApplicationException("Structure is empty.");
            }
            else
            {

                return RecFindSmallest(nRoot);
            }
        }

        private T RecFindSmallest(Node<T> nCurrent)
        {
            if (nCurrent.Left == null)
            {
                return nCurrent.Data;
            }
            else
            {
                return RecFindSmallest(nCurrent.Left);
            }

        }

        /// <summary>
        /// Find the largest data item in the BST
        /// </summary>
        /// <returns>Returns a reference to the largest item.</returns>
        public T findLargest()
        {
            if (nRoot == null)
            {
                throw new ApplicationException("Structure is empty!");
            }
            else
            {
                return RecFindLargest(nRoot);
            }


        }

        private T RecFindLargest(Node<T> nCurrent)
        {
            if (nCurrent.Right == null)
            {
                return nCurrent.Data;
            }
            else
            {
                return RecFindLargest(nCurrent.Right);
            }
        }

        virtual public void PrintAsTree()
        {
            Queue<NodeLevel> q = new Queue<NodeLevel>();
            q.Enqueue(new NodeLevel(nRoot, 1));
            int iCurrentLevel = 1;
            int iInterval = 60;


            while (q.Count > 0)
            {

                char[] cArray = new char[240];
                int iIndex = 0;
                int iLevelStartPosition = (int)Math.Pow(2, (iCurrentLevel - 1));
                while (q.Count > 0 && (int)Math.Log(q.Peek().iPosition, 2) + 1 == iCurrentLevel)
                {

                    NodeLevel nl = q.Dequeue();

                    if (nl.n.Left != null)
                        q.Enqueue(new NodeLevel(nl.n.Left, nl.iPosition * 2));

                    if (nl.n.Right != null)
                        q.Enqueue(new NodeLevel(nl.n.Right, nl.iPosition * 2 + 1));


                    //Print the value on this level
                    String sData = nl.n.Data.ToString();
                    char[] cData = sData.ToArray();
                    if (nl.iPosition != iLevelStartPosition)
                    {
                        iIndex = 2 * iInterval * (nl.iPosition - iLevelStartPosition) + iInterval +
                            (nl.iPosition - iLevelStartPosition) * 2;
                    }
                    else
                    {
                        iIndex = iInterval;
                    }

                    Array.Copy(cData, 0, cArray, iIndex, cData.Length);

                }
                //Start a new level

                string s = new string(cArray);
                Console.WriteLine(s + '\n');

                iCurrentLevel++;
                iInterval = (int)(iInterval / 2);

            }
            Console.WriteLine();
        }

        private class NodeLevel
        {
            internal Node<T> n;
            internal int iPosition;

            public NodeLevel(Node<T> n, int iPosition)
            {
                this.n = n;
                this.iPosition = iPosition;
            }
        }


        #endregion



        #region Enumerator implementation

        private class Enumerator : IEnumerator<T>
        {

            private BST<T> parent = null;
            private Node<T> nCurrent = null;
            private Stack<Node<T>> sNodes = null;

            //Constructor
            public Enumerator(BST<T> parent)
            {
                this.parent = parent;
                Reset();
            }

            public T Current
            {
                get { return nCurrent.Data; }
            }

            public void Dispose()
            {
                parent = null;
                nCurrent = null;
                sNodes = null;
            }

            object System.Collections.IEnumerator.Current
            {
                get { return nCurrent.Data; }
            }

            //Move next moves to the next element by setting nCurrent
            //Returns true if it moved else false.
            // determines the order of traversal.
            public bool MoveNext()
            {
                bool bMoved = false;
                if (sNodes.Count > 0)
                {
                    bMoved = true;
                    nCurrent = sNodes.Pop();
                    if (nCurrent.Right != null)
                    {
                        sNodes.Push(nCurrent.Right);
                    }
                    if (nCurrent.Left != null)
                    {
                        sNodes.Push(nCurrent.Left);
                    }
                }
                return bMoved;
            }

            public void Reset()
            {
                sNodes = new Stack<Node<T>>();
                //Push the root node on the stack
                if (parent.nRoot != null)
                {
                    sNodes.Push(parent.nRoot);
                }
                nCurrent = null;
            }
        }

        #endregion

    }
}
