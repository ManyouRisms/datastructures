﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    public class AVLT<T> : BST<T> where T : IComparable<T>
    {
        #region Balance
        internal override Node<T> Balance(Node<T> nCurrent)
        {
            int rightChildTreeDiff;
            int leftChildTreeDiff;
            Node<T> nNewRoot = nCurrent;
            // if current is not null
            // heightdiff < -- get height diff of the current node
            // if the tree is unbalanced to the right
            //RightChildTreeDiff <-- get height diff of current's right child
            // if the child is left heavy
            //nNewRoot <-- doubleleft on current node
            // else
            //nNewRoot <-- single left on current node
            // if the tree is unbalanced to the left
            // LeftChildTreeDiff <-- get height diff of current's left child
            //if the child is left heavy
            //nNewRoot <-- doubleright on current ndoe
            //else
            //nNewRoot <-- single right on current node

            if (nCurrent != null)
            {
                int heightDiff = GetHeightDifference(nCurrent);
                if (heightDiff < -1)
                {
                    rightChildTreeDiff = GetHeightDifference(nCurrent.Right);
                    if (rightChildTreeDiff > 1)
                    {
                        nNewRoot = DoubleLeft(nCurrent);
                    }
                    else
                    {
                        Console.WriteLine("Single left\n");
                        nNewRoot = SingleLeft(nCurrent);
                    }
                }
                else if (heightDiff > 1)
                {
                    leftChildTreeDiff = GetHeightDifference(nCurrent.Left);
                    if (leftChildTreeDiff > 1)
                    {
                        Console.WriteLine("Double right\n");
                        nNewRoot = DoubleRight(nCurrent);
                    }
                    else
                    {
                        Console.WriteLine("Single Right");
                        nNewRoot = SingleRight(nCurrent);
                    }
                }

            }
            return nNewRoot;
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// This method returns the height difference between the children. A positive
        /// number indicates a left-heavy tree, while a negative number indicates a
        /// right heavy tree.
        /// </summary>
        /// <param name="nCurrent">Node to determine its height difference.</param>
        /// <returns>Height Difference as + or -</returns>
        private int GetHeightDifference(Node<T> nCurrent)
        {
            int iHeightLeft = -1;
            int iHeightRight = -1;
            int iHeightDiff = 0;

            if (nCurrent != null)
            {
                if (nCurrent.Right != null)
                {
                    iHeightRight = RecHeight(nCurrent.Right);
                }
                if (nCurrent.Left != null)
                {
                    iHeightLeft = RecHeight(nCurrent.Left);
                }
            }
            else
            {
                throw new ApplicationException("Error at 0x0FF7CBE3: Your node is null");
            }
            iHeightDiff = iHeightLeft - iHeightRight;
            return iHeightDiff;
        }


        #endregion
        #region Rotation Methods
        /// <summary>
        /// Passes in the old root, does the rotation and returns the new root. Assumes the current subtree
        /// requires a single left. (Testing not done here..)
        /// </summary>
        /// <param name="nOldRoot"></param>
        /// <returns></returns>
        private Node<T> SingleLeft(Node<T> nOldRoot)  //passing in the old root, were going to return the new root
        {
            // step 1 - assign the new root
            Node<T> nNewRoot = nOldRoot.Right;

            // step 2 - (if it has a root)
            nOldRoot.Right = nNewRoot.Left;

            // step 3 - new roots left gets assigned the old root
            nNewRoot.Left = nOldRoot;

            //return the new root
            return nNewRoot;
        }

        /// <summary>
        /// Passes in the old root, does the rotation and returns the new root. Assumes the current subtree
        /// requires a single right. (Testing not done here..)
        /// </summary>
        /// <param name="nOldRoot"></param>
        /// <returns></returns>
        private Node<T> SingleRight(Node<T> nOldRoot)  //passing in the old root, were going to return the new root
        {
            // step 1 - assign the new root
            Node<T> nNewRoot = nOldRoot.Left;

            // step 2 - (if it has a root)
            nOldRoot.Left = nNewRoot.Right;

            // step 3 - new roots left gets assigned the old root
            nNewRoot.Right = nOldRoot;

            //return the new root
            return nNewRoot;
        }

        private Node<T> DoubleLeft(Node<T> nOldRoot)
        {
            Console.WriteLine("Double left\n");

            nOldRoot.Right = SingleRight(nOldRoot.Right);
            return SingleLeft(nOldRoot);
        }

        private Node<T> DoubleRight(Node<T> nOldRoot)
        {
            Console.WriteLine("Double right\n");

            nOldRoot.Left = SingleLeft(nOldRoot.Left);
            return SingleRight(nOldRoot);
        }
        #endregion
    }
}