﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    public class Node<T> where T : IComparable<T>
    {
        #region Attributes

        private T tData;

        // node tracks children and references to nodes
        private Node<T> nLeft;


        private Node<T> nRight;



        #endregion

        #region Constructors
        public Node() : this(default(T), null, null) { }
        public Node (T tData, Node<T> nLeft, Node<T> nRight)
        {
            this.tData = tData;
            this.nLeft = nLeft;
            this.nRight = nRight;
        }

        public Node(T tData) : this(tData, null, null) { }
        #endregion

        #region Properties

        // note that the default property has both a 'get' and 'set'. you
        // should consider whether you need both or not. 
        public T Data
        {
            get { return tData; }
            set { tData = value; }
        }

        public Node<T> Left
        {
            get {
                //business rules go here
                return nLeft; }
            set { 
                // also here

                nLeft = value; } // a microsoft thing, you don't need a parameter.
        }

        public Node<T> Right
        {
            get { return nRight; }
            set { nRight = value; }
        }
        #endregion

        #region Other Functionality

        public bool IsLeaf()
        {
            return Left==null && Right == null;
        }


        


        #endregion
    }
}
