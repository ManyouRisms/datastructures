﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataStructuresCommon;
namespace BinarySearchTree
{
    // define a delegate that will point to a method that will do something to each data member 
    // of type 'T' in the tree.
    public delegate void ProcessData<T> (T data);

    //Set up an enumeration to determine the order of iteration.
    public enum TRAVERSALORDER { PRE_ORDER, IN_ORDER, POST_ORDER };
    public interface I_BST<T> : I_Collection<T> where T : IComparable<T>
    {
        /// <summary>
        /// Given a data element, find the corrosponding element of 
        /// equal value to it.
        /// </summary>
        /// <param name="data">The data item to compare to be an item
        /// in the tree</param>
        /// <returns>A reference to the item if found. Otherwise returns
        /// the default value for the type T (Default(T)). 
        /// Be careful.</returns>
        T Find(T data);

        /// <summary>
        /// Calculate the height of the tree
        /// </summary>
        /// <returns>int representing the height of the tree.</returns>
        int Height();

        /// <summary>
        /// Similar to an enumerator, but more efficient. Also, the iterate
        /// method utilizes a delegate to perform some action on each data item.
        /// 
        /// </summary>
        /// <param name="pd">A delegate to a function pointer.</param>
        /// <param name="to">The traversal order.</param>
        void Iterate(ProcessData<T> pd, TRAVERSALORDER to);

    }
}
