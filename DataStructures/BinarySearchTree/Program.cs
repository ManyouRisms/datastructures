﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    class Program
    {

        //public static void PrintInt(int x)
        //{
        //    Console.WriteLine(x);
        //}

        static void TestAVLTree()
        {
            AVLT<int> myTree = new AVLT<int>();
            myTree.Add(2);
            myTree.Add(8);
            myTree.Add(12);
            myTree.Add(16);
            myTree.Add(19);
            myTree.Add(22);
            myTree.Add(42);
            myTree.Add(53);
            myTree.Add(57);
            myTree.Add(60);
            myTree.Add(65);

            Console.WriteLine(myTree.Height());
            
            myTree.PrintAsTree();

            myTree.Remove(2);
            myTree.Remove(12);
            Console.WriteLine(myTree.Height());
            myTree.Remove(53);
            myTree.PrintAsTree();

        }

        public static void PrintData(string dataString)
        {
            Console.WriteLine(dataString);
        }

        public static void PrintInt(int intToPrint)
        {
            Console.WriteLine(intToPrint);
        }



        static void testRemoveMethod()
        {
            BST<string> myTree = new BST<string>();

            myTree.Add("Jesse");
            myTree.Add("Brendon");
            myTree.Add("Brandon");
            myTree.Add("Lyle");
            myTree.Add("Brad");
            myTree.Add("Ben");
            myTree.Add("Paul");
            myTree.Add("Colin");
            myTree.Add("Tyler");
            myTree.Add("Devon");


            Console.WriteLine(myTree.ToString());
            myTree.Remove("Jesse");

            Console.WriteLine(myTree.ToString());

        }

        static void testTraversalMethods()
        {
            BST<int> myTree = new BST<int>();

            myTree.Add(50);
            myTree.Add(27);
            myTree.Add(16);
            myTree.Add(88);
            myTree.Add(34);
            myTree.Add(65);
            myTree.Add(52);
            myTree.Add(77);
            myTree.Add(93);
            myTree.Add(4);
            myTree.Add(12);
            myTree.Add(29);
            myTree.Add(44);
            myTree.Add(92);
            
           // myTree.Iterate(PrintInt, TRAVERSALORDER.IN_ORDER);
            myTree.PrintAsTree();

        }


        static void TestBST()
        {
            BST<int> myTree = new BST<int>();

            //Console.WriteLine(myTree.Height());
            myTree.Add(2);
            myTree.Add(42);
            // Console.WriteLine(myTree.Height());
            myTree.Add(12);
            myTree.Add(53);
            myTree.Add(8);
            myTree.Add(16);
            myTree.Add(60);
            //Console.WriteLine(myTree.Height());

            myTree.Add(22);
            myTree.Add(57);
            myTree.Add(65);
            myTree.Add(19);
            myTree.Add(2);
            myTree.Add(22);
            myTree.Add(57);
            myTree.Add(65);
            myTree.Add(19);
            myTree.Add(2);
            myTree.Add(22);
            myTree.Add(57);
            myTree.Add(65);
            myTree.Add(19);
            myTree.Add(2);
            myTree.Add(22);
            myTree.Add(57);
            myTree.Add(65);
            myTree.Add(19);
            myTree.Add(2);
            myTree.Add(22);
            myTree.Add(57);
            myTree.Add(65);
            myTree.Add(19);
            myTree.Add(2);
            myTree.Add(22);
            myTree.Add(57);
            myTree.Add(65);
            myTree.Add(19);
            //Console.WriteLine(myTree.Height());

            myTree.PrintAsTree();

        }
        static void Main(string[] args)
        {

            //TestAVLTree();

            //testTraversalMethods();
            //testRemoveMethod();
            //TestBST();
            //testAVLRotations();
            //testCount();
            //testTheories();
            testTraversalMethods();
        }

        static void testTheories()
        {
            AVLT<int> myTree = new AVLT<int>();
            myTree.Add(1);
            myTree.Add(2);
            myTree.Add(10);
            myTree.Add(3);
            myTree.Add(6);
            myTree.Add(27);
            myTree.Add(11);
            //Console.WriteLine(myTree.ToString());
            myTree.PrintAsTree();

        }

        static void testAVLRotations()
        {
            AVLT<int> myTree = new AVLT<int>();


            myTree.Add(21);
            myTree.Add(34);

            myTree.PrintAsTree();
            addSpaces();
            myTree.Add(78);
            myTree.PrintAsTree();
            addSpaces();

            myTree.Add(16);
            myTree.PrintAsTree();
            addSpaces();

            myTree.Add(52);
            myTree.PrintAsTree();
            addSpaces();

            myTree.Add(65);
            myTree.PrintAsTree();
            addSpaces();

            myTree.Add(92);
            myTree.PrintAsTree();
            addSpaces();

            myTree.Add(81);

            myTree.PrintAsTree();
            addSpaces();


        }
        public static void addSpaces()
        {
            Console.WriteLine("\n\n\n\n\n\n\n\n\n");
        }

        public static void testCount()
        {
            BST<int> myTree = new BST<int>();
            myTree.Add(5);

            Console.WriteLine(myTree.CountNodes());


        }

    }
}
