﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;


        /// <summary>
        /// Implementation of a radix sort to sort an array of string (ascii) values.
        /// </summary>
        public class StringRadixSorter : AbstractSorter<string>
        {
            /// <summary>
            /// A linked list array to store the values based on the digit being
            /// considered.
            /// </summary>
            private LinkedList<string>[] buckets;

            /// <summary>
            /// Number of buckets. We use 128 buckets because that's hte number of unique characters in ASCII
            /// </summary>
            private const int NUM_BUCKETS = 128;

            /// <summary>
            /// Constructor passes the array to the parent class. Also initializes
            /// the buckets.
            /// </summary>
            /// <param name="arrayToSort">The array to be sorted</param>
            public StringRadixSorter(string[] arrayToSort)
                : base(arrayToSort)
            {
                // initialize the bucket array
                buckets = new LinkedList<string>[NUM_BUCKETS];
                for (int i = 0; i < buckets.Length; i++)
                {
                    buckets[i] = new LinkedList<string>();
                }
            }

            public override void Sort()
            {
                this.doSort(0, this.Length - 1);
            }

            /// <summary>
            /// Implementation of the radix sort over the specified range.
            /// </summary>
            /// <param name="start">The index of the first element to be sorted
            /// (inclusive)</param>
            /// <param name="end">The index of the last element to be sorted
            /// (exclusive)</param>
            public void doSort(int start, int end)
            {
                // find the largest value
                int largest = FindLargest(start, end);
                // how many digits
                int maxRadix = largest-1;
                // loop through each of the characters, starting with the rightmost one (least significant)
                //  If a word doesn't have a character at the given position, the null character is used as its value
                for (int radix = maxRadix; radix >= 0 ; radix--)
                {
                    // loop through each of the array elements
                    for (int j = start; j <= end; j++)
                    {
                        // put array value into correct bucket
                        buckets[GetRadixValue(array[j], radix)].AddLast(array[j]);
                    }
                    // rebuild the array
                    int arrayPos = start;
                    // go through each bucket
                    for (int bucketNum = 0; bucketNum < buckets.Length; bucketNum++)
                    {
                        LinkedList<string>.Enumerator e = buckets[bucketNum].GetEnumerator();
                        // coalesce the buckets back into the array
                        foreach (string current in buckets[bucketNum])
                        {
                            array[arrayPos++] = current;
                        }
                        buckets[bucketNum].Clear();
                    }
                }
            }

            /// <summary>
            /// Gets the appropriate character from the given number.
            /// </summary>
            /// <param name="value">The string to extract a char from</param>
            /// <param name="radix">Which char to get</param>
            /// <returns>The char in that position, or null char if none is in that position</returns>
            private char GetRadixValue(string value, int charIndex)
            {
                //Last character first
                if(charIndex >= value.Length)
                {
                    //return value[value.Length - 1];
                    return char.MinValue;
                }
                return value[charIndex];
            }

            /// <summary>
            /// Does a pass through the array to find the longest string. This is
            /// used later to determine how many characters there are.
            /// </summary>
            /// <param name="start">The index of the first element to be sorted
            /// (inclusive)</param>
            /// <param name="end">The index of the last element to be sorted
            /// (inclusive)</param>
            /// <returns>The number of chars in the longest string</returns>
            private int FindLargest(int start, int end)
            {
                int largest = this.array[start].Length;
                for (int i = start + 1; i <= end; i++)
                {
                    if (this.array[i].Length > largest)
                    {
                        largest = array[i].Length;
                    }
                }
                return largest;
            }
        }
    

}
