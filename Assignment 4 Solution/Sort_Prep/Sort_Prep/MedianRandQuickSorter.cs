﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    public class MedianRandQuickSorter<T> : MedianThreeQuickSorter<T> where T : IComparable<T>
    {
        public MedianRandQuickSorter(T[] array)
            : base(array)
        {

        }



        /// <summary>
        /// Chooses a pivot by selecting 3 random index values from the
        /// current subsection of the list and returning the median, rather
        /// than using the first, last and middle values.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public override int GetPivotBetween(int left, int right)
        {
            Random r = new Random();
            int iA = r.Next(left, right);
            int iB = r.Next(left, right);
            int iC = r.Next(left, right);
            return this.GetIndexOfMedian(iA, iB, iC);
            
        }
    }
}
