﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{

    /// <summary>
    /// MedianActual calculates the perfect optimal pivot value as the exact median value at any given point.
    /// Since we're dealing with averages, we can only sort numbers with this sort.
    /// </summary>
    /// <typeparam name="?"></typeparam>
    public class MedianActualQuickSorter : QuickSorter<int>
    {
        public MedianActualQuickSorter(int[] array)
            : base(array)
        {

        }



        /// <summary>
        /// Calculates the average of all of the values between left and right,
        /// then returns the index of the array value which was closest to that
        /// average. The optimal pivot for quicksort is the actual median value.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public override int GetPivotBetween(int left, int right)
        {
            int average = 0;
            int count = right - left + 1;
            int sum = 0;
            for(int pos = left; pos <= right; pos++)
            {
                sum += this.array[pos];
            }
            average = sum / count;

            int bestKnownIndex = left;
            
            for(int currentIndex = left; currentIndex <= right; currentIndex++)
            {
                int bestKnownValue = this.array[bestKnownIndex];
                int currentValue = this.array[currentIndex];

                //If the absolute different in value between current and the average is
                // less than the difference between average and the value we previously
                // thought was closest to the average, then we have a new closest value.
                if(Math.Abs(average - currentValue) < Math.Abs(average - bestKnownValue))
                {
                    bestKnownIndex = currentIndex;
                }
            }
            return bestKnownIndex;
        }
    }
}
