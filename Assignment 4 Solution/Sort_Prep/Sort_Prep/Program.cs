﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.Write("Enter number of elements to sort: ");
            //Read input from user
            String input = Console.ReadLine();
            int arraySize = Int32.Parse(input);
            
            
            Console.Clear();
                
            int[] array = new int[arraySize];

            //fill the array with ever decreasing values

            for (int i = array.Length - 1; i >= 0; i--)
            {
                array[i] = array.Length - i;
            }

            //Bubble sorts
            AbstractSorter<int> sorter = new BubbleSorter<int>(array);
            ShuffleAndSort(array, sorter);

            //Selection sorts
            sorter = new SelectionSorter<int>(array);
            ShuffleAndSort(array, sorter);

            //Insertion sorts
            sorter = new InsertionSorter<int>(array);
            ShuffleAndSort(array, sorter);


            //Shell sorts
            sorter = new ShellSorter<int>(array);
            ShuffleAndSort(array, sorter);

            sorter = new KnuthShellSort<int>(array);
            ShuffleAndSort(array, sorter);

            sorter = new CiuraShellSorter<int>(array);
            ShuffleAndSort(array, sorter);

            sorter = new LazarusShellSort<int>(array);
            ShuffleAndSort(array, sorter);

            sorter = new SedgewickShellSort<int>(array);
            ShuffleAndSort(array, sorter);


            //Quick Sorts
            sorter = new QuickSorter<int>(array);
            ShuffleAndSort(array, sorter);

            sorter = new MedianThreeQuickSorter<int>(array);
            ShuffleAndSort(array, sorter);

            sorter = new MedianRandQuickSorter<int>(array);
            ShuffleAndSort(array, sorter);

            sorter = new MedianActualQuickSorter(array);
            ShuffleAndSort(array, sorter);

            sorter = new IterativeQuickSorter<int>(array);
            ShuffleAndSort(array, sorter);


            //Heap sorts
            sorter = new HeapSorter<int>(array);
            ShuffleAndSort(array, sorter);


            //Radix sorts
            sorter = new EfficientRadixSorter<int>(array, new IntegerRadixKVP());
            ShuffleAndSort(array, sorter);

            sorter = new RadixSorter(array);
            ShuffleAndSort(array, sorter);

            

//            checkStringRadixSorter();

            Console.Read();
        }


        public static void ShuffleAndSort(int[] array, AbstractSorter<int> sorter)
        {
            Random rng = new Random();
            int n = array.Length;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                int value = array[k];
                array[k] = array[n];
                array[n] = value;
            }
            TestSorter(sorter);
        }


        public static void checkStringRadixSorter()
        {
            string[] strArray = { 
                "rikisha",
                "chulpa",
                "pegasus",
                "blink",
                "pannier",
                "tartly",
                "parka",
                "curvey",
                "gare",
                "supper",
                "gnostic",
                "hotch",
                "copy",
                "nunlike",
                "hoarier",
                "gipper",
                "schizo",
                "sial",
                "mounty",
                "cryst",
                "fima",
                "sluit",
                "vinie",
                "beer",
                "genesic",
                "abluent",
                "overrun",
                "pampa",
                "tapeman",
                "tallyho",
                "average",
                "bloat",
                "flews",
                "zona",
                "winker",
                "ferrety",
                "minter",
                "apteral",
                "strain",
                "hotcha",
                "blanch",
                "ananda",
                "sootier",
                "yogic",
                "oakley",
                "wig",
                "angler",
                "faraday",
                "kouprey",
                "tonga"};

            //Radix sorter using a keyvaluepair object to abstract away significant digit generation
            AbstractSorter<string> sradix = new EfficientRadixSorter<string>(strArray, new StringRadixKVP());

            //Plain radix sorter for strings
//            AbstractSorter<string> sradix = new StringRadixSorter(strArray);

            Console.WriteLine("\nBefore:");
            Console.WriteLine(sradix);
            sradix.Sort();
            Console.WriteLine("\nAfter:");
            Console.WriteLine(sradix);

            //Your brain asplode
            //Console.WriteLine(strArray.Aggregate( (accumulated, next) => result + next + "\n"  ));
        }

        private static void TestSorter(AbstractSorter<int> sorter)
        {
            Console.WriteLine(sorter.GetType().Name + " with "
                + sorter.Length + " elements.");
            if (sorter.Length <= 50)
            {
                Console.WriteLine("Before sort: \n" + sorter);
            }

            //Determine the time it takes to sort
            long startTime = Environment.TickCount;
            sorter.Sort();
            long endTime = Environment.TickCount;

            if (sorter.Length <= 50)
            {
                Console.WriteLine("After sort: \n" + sorter);
            }

            //Print the time to do the sort
            Console.WriteLine("Sort took " + (endTime - startTime) + " milliseconds\n\n");
        }





        /// <summary>
        /// The following are KeyValuePair classes used by the efficientRadixSorter to separate significant digit
        /// selection from the sort itself.
        /// </summary>

        public class IntegerRadixKVP : RadixKVP<int>
        {
            public IntegerRadixKVP() : this(int.MaxValue)
            {

            }


            public IntegerRadixKVP(int value) : base(value)
            {

            }

            public override int GetValueForRadix(int radix, int maxRadix)
            {
                return (this.Value / ((int)Math.Pow(this.GetNumberOfUniqueValues(), radix))) % this.GetNumberOfUniqueValues();
            }

            public override RadixKVP<int> GetInstance(int forValue)
            {
                return new IntegerRadixKVP(forValue);
            }

            /// <summary>
            /// Get the maximum number of significant digits
            /// </summary>
            /// <returns></returns>
            public override int GetRadixSpan()
            {
                return ((int)Math.Floor(Math.Log10(this.Value))) + 1;
            }

            //10 digits, 10 buckets
            public override int GetNumberOfUniqueValues()
            {
                return 10;
            }
        }


        public class StringRadixKVP : RadixKVP<string>
        {
            public StringRadixKVP()
                : this("")
            {

            }


            public StringRadixKVP(string value)
                : base(value)
            {

            }

            public override int GetValueForRadix(int radix, int maxRadix)
            {
                int normRadix = maxRadix - radix;
                //Last character first
                if (normRadix >= this.Value.Length)
                {
                    //return value[value.Length - 1];
                    return char.MinValue;
                }
                return Value[normRadix];
            }

            public override RadixKVP<string> GetInstance(string forValue)
            {
                return new StringRadixKVP(forValue);
            }

            public override int GetRadixSpan()
            {
                return this.Value.Length;
            }

            public override int GetNumberOfUniqueValues()
            {
                return 128; //0->127 in ascii key table
            }
        }



        
    }
}
