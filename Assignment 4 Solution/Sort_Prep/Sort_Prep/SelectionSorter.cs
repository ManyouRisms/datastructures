﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    public class SelectionSorter<T> : AbstractSorter<T> where T : IComparable<T>
    {
        public SelectionSorter(T[] array)
            : base(array)
        {

        }


        public override void Sort()
        {
            //While there are still unsorted items
                //Look through the unsorted array and find the next lowest item.
                //Put it at the end of the sorted portion by swapping it with whatever was already there
                //Move to the next unsorted item

            //Alternatively:
                //While there are still unsorted items
                    //Look through the unsorted items
                        //If the current unsorted item is smaller than current, swap

            for(int firstUnsortedIndex = 0; firstUnsortedIndex < this.Length; firstUnsortedIndex++)
            {
                int smallestItemIndex = firstUnsortedIndex;
                for(int currentUnsortedItemIndex = smallestItemIndex; currentUnsortedItemIndex < this.Length; currentUnsortedItemIndex++ )
                {
                    if (this.array[currentUnsortedItemIndex].CompareTo(this.array[smallestItemIndex]) < 0)
                    {
                        smallestItemIndex = currentUnsortedItemIndex;
                    }
                }
                if(smallestItemIndex != firstUnsortedIndex)
                {
                    Swap(smallestItemIndex, firstUnsortedIndex);
                }
            }



        }
    }
}
