﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    class LazarusShellSort<T> : ShellSorter<T> where T : IComparable<T>
    {

        public LazarusShellSort(T[] array)
            : base(array)
        {

        }

        /// <summary>
        /// The Frank & Lazarus algorithm is like the Shell algorithm in that it generates the
        /// largest gaps first. Defined as 2(N/(2^(k+1)))+1, where N is the number of items
        /// in the list, and k is 1 for the first (largest) gap, 2 for the second gap and so on.
        /// </summary>
        /// <param name="arraySize"></param>
        /// <returns></returns>
        public override IEnumerable<int> GetGapList(int arraySize)
        {
            Queue<int> gaps = new Queue<int>();
            int numberOfGaps = 1;
            int gap = arraySize;
            do
            {
                //2[N/2^(k+1)]+1, from 2[n/4] to 1

                gap = 2 * (arraySize / pow(2, numberOfGaps + 1)) + 1;
                gaps.Enqueue(gap);
                numberOfGaps++;
            } while (gap > 1);
            return gaps;
        }


    }
}
