﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    class BubbleSorter<T> : AbstractSorter<T> where T : IComparable<T>
    {

        public BubbleSorter(T[] array)
            : base(array)
        {

        }

        public override void Sort()
        {
            //While there are unsorted items
                //Check two items: current, and current+1, starting at current=0
                    //If current > current+1, then swap
                    //Move to next current until current+1 is the last index
                //Consider current+1 sorted
                

            int numberOfUnsortedItems = this.Length - 1;
            bool didASwap = true;


            do
            {
                didASwap = false;
                for (int current = 0; current < numberOfUnsortedItems; current++)
                {
                    int next = current + 1;
                    if (this.array[current].CompareTo(this.array[next]) > 0)
                    {
                        Swap(current, next);

                        didASwap = true;
                    }
                }
                numberOfUnsortedItems--;
            } while (didASwap);
        }
    }
}
