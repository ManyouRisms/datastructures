﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sort_Prep
{
    public abstract class AbstractSorter<T> where T : IComparable<T>
    {
        #region Attributes
        protected T[] array;
        #endregion

        #region Constructors
        public AbstractSorter(T[] array)
        {
            this.array = array;
        }
        #endregion

        #region Abstract Methods
        public abstract void Sort();
        #endregion

        #region Helper Methods
        protected void Swap(int first, int second)
        {
            T temp = array[first];
            array[first] = array[second];
            array[second] = temp;
        }

        public int Length
        {
            get { return array.Length; }
        }

        /// <summary>
        /// Indexer used to allow a user to index into a particular location in 
        /// this classes array.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T this[int index]
        {
            get
            {
                return array[index];
            }
            set
            {
                array[index] = value;
            }
        }
        #endregion

        public override string ToString()
        {
            return "[" + string.Join(", ", this.array) + "]";
        }

    }
}
