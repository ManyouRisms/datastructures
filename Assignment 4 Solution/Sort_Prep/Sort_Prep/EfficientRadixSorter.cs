﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    /// <summary>
    /// Implementation of a radix sort to sort an array of any values, as long as an
    /// implementation of RadixKVP has been created for that type.
    /// </summary>
    public class EfficientRadixSorter<T> : AbstractSorter<T> where T : IComparable<T>
    {
        /// <summary>
        /// An array of buckets, generally one for each possible value for a significant digit.
        /// </summary>
        private LinkedList<RadixKVP<T>>[] buckets;

        /// <summary>
        /// A list of KVP objects, one for each of the values in the input array
        /// </summary>
        private RadixKVP<T>[] kvpArray;

        private int NUM_BUCKETS = 0;

        /// <summary>
        /// Constructor passes the array to the parent class. Also initializes
        /// the buckets.
        /// </summary>
        /// <param name="arrayToSort">The array to be sorted</param>
        public EfficientRadixSorter(T[] arrayToSort, RadixKVP<T> kvpType)
            : base(arrayToSort)
        {
            this.NUM_BUCKETS = kvpType.GetNumberOfUniqueValues();
            // initialize the bucket array
            buckets = new LinkedList<RadixKVP<T>>[NUM_BUCKETS];
            for (int i = 0; i < buckets.Length; i++)
            {
                buckets[i] = new LinkedList<RadixKVP<T>>();
            }

            kvpArray = new RadixKVP<T>[arrayToSort.Length];
            int x = 0;
            foreach(T current in arrayToSort)
            {
                kvpArray[x++] = kvpType.GetInstance(current);
            }
        }

        public override void Sort()
        {
            this.doSort(0, this.Length-1);
        }

        /// <summary>
        /// Implementation of the radix sort over the specified range.
        /// </summary>
        /// <param name="start">The index of the first element to be sorted
        /// (inclusive)</param>
        /// <param name="end">The index of the last element to be sorted
        /// (exclusive)</param>
        public  void doSort(int start, int end)
        {
            // find the largest value
            int largest = FindLargest();
            // how many digits
            int maxRadix = largest-1;
            // loop through each of the digits, starting with least significant
            for (int radix = 0; radix <= maxRadix; radix++)
            {
                // loop through each of the array elements
                for (int j = start; j <= end; j++)
                {
                    // put array value into correct bucket
                    buckets[kvpArray[j].GetValueForRadix(radix, maxRadix)].AddLast(kvpArray[j]);
                }
                // rebuild the array
                int arrayPos = start;
                // go through each bucket
                for (int bucketNum = 0; bucketNum < buckets.Length; bucketNum++)
                {
                    LinkedList<RadixKVP<T>>.Enumerator e = buckets[bucketNum].GetEnumerator();
                    // coalesce the buckets back into the array
                    foreach (RadixKVP<T> kvp in buckets[bucketNum])
                    {
                        kvpArray[arrayPos++] = kvp;
                    }
                    buckets[bucketNum].Clear();
                }
            }

            int k = 0;
            foreach(RadixKVP<T> kvp in this.kvpArray)
            {
                this.array[k++] = kvp.Value;
            }
        }



        /// <summary>
        /// Does a pass through the array to find the largest value. This is
        /// used later to determine how many significant digits there are.
        /// </summary>
        /// <param name="start">The index of the first element to be sorted
        /// (inclusive)</param>
        /// <param name="end">The index of the last element to be sorted
        /// (exclusive)</param>
        /// <returns>The largest value in the array</returns>
        private int FindLargest()
        {
            int largest = 0;
            foreach(RadixKVP<T> radix in this.kvpArray)
            {
                int rsp = radix.GetRadixSpan();
                if(rsp > largest)
                {
                    largest = rsp;
                }
            }
            return largest;
        }

        
    }
}
