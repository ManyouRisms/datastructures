﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    /// <summary>
    /// Used by the EfficientRadixSorter to separate all operations involving
    /// significant digit generation and selection from the sorting algorithm
    /// itself.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class RadixKVP<T>
    {
        public T Value
        {
            get;
            set;
        }

        public RadixKVP(T value)
        {
            this.Value = value;
        }

        public abstract int GetValueForRadix(int radix, int maxRadix);

        public abstract RadixKVP<T> GetInstance(T forValue);

        public abstract int GetRadixSpan();

        public abstract int GetNumberOfUniqueValues();

    }
}
