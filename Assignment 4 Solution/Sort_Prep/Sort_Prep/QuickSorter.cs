﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    public class QuickSorter<T> : AbstractSorter<T> where T : IComparable<T>
    {

        //These two attributes are used by the PrintBars routine, which outputs the animated bargraphs
        // shown in class.
        //int comparisons = 0;
        //public List<int> pivots = new List<int>();

        public QuickSorter(T[] array)
            : base(array)
        {

        }

        public override void Sort()
        {
            doQuickSort(0, this.Length-1);

            
        }

        protected void doQuickSort(int left, int right)
        {
            //If the set of items to partition contains more than 1 item
                //Partition: re-arrange the set of items so that everything to the left of the pivot is less,
                    //everything to the right of the pivot is greater
                //sort by partitioning the set of items to the left of the pivot (recursively partition everything left)
                //sort by partitioning the set of items to the right of the pivot (recursively partition everything right)


            if(left < right)
            {
                int finalPivotPosition = Partition(left, right);
                //pivots.Add(finalPivotPosition);

                //A common mistake here was to include the pivot in either the left
                // or right recursive calls. Since the partition item is in its final
                // position, re-sorting it is not required. Sort everything below it (left),
                // then everythign above it (right), but exclude the pivot itself.
                doQuickSort(left, finalPivotPosition - 1);
                doQuickSort(finalPivotPosition + 1, right);
            }
            //else
            //{
            //    pivots.Add(left); //Used for tracking in the PrintBars animated graph routine
            //}
        }

        

        protected virtual int Partition(int left, int right)
        {
            //Invariants- At the end of this call to partition:
                //Everything to the left of the partitionPoint is less than the pivot
                //Everything from the partitionPoint right is greater than or equal to the pivot

            //currentItem looks at each item between left and right in turn
            //Boundary always points to the index of the first item that's known to be higher than the pivot


            //Choose a value as the pivot
            int pivot = GetPivotBetween(left, right);
            
            T pivotValue = this.array[pivot];

            //PrintBars(pivot, left, right);


            //Move the pivot out of the way by pushing it all the way to the right
            Swap(pivot, right);

            pivot = right; //The pivot is now located in the rightmost index

            //lowHighBoundary is the boundary between items which are known to be less than the
            // value of the pivot and items which are either known to be higher or haven't yet been 
            // examined. lowHighBoundary will always be a value which is >= the pivot after the first loop
            int lowHighBoundary = left;

            //Inspect each item in this chunk of the array
            for(int currentItem = left; currentItem < right; currentItem++)
            {
                //comparisons++;
                //PrintBars(right, left, right, currentItem, lowHighBoundary);

                //If the current item is less than the pivot, put it where the partitionPoint is and increment the partition point.
                if(this.array[currentItem].CompareTo(pivotValue) < 0)
                {
                    //PrintBars(right, left, right, currentItem, lowHighBoundary, true);

                    Swap(currentItem, lowHighBoundary);
                    lowHighBoundary++;

                    //PrintBars(right, left, right, currentItem, lowHighBoundary);
                }
            }

            //LowHighBoundary is now the boundary point between the items lower than the pivot (left)
            // and higher than the pivot (lowHighBoundary and right). lowHighBoundary will be larger than the pivot, 
            // and so can be legally swapped to the right, where the pivot is currently located.
            Swap(lowHighBoundary, pivot);

            //PrintBars(lowHighBoundary, left, right, -1, -1, true);

            return lowHighBoundary;
        }


        /// <summary>
        /// The original definition of QuickSorter chose the right most value as the pivot,
        /// as this was simplest and required fewer comparisons and swaps. This leads to worst-case
        /// behaviour when the list is already sorted, since we end up partitioning the list into
        /// N-1 items on the left and 0 items on the right at each partition step.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public virtual int GetPivotBetween(int left, int right)
        {
            return right;
        }

        #region PrintBars
        //public void PrintBars(int pivot, int left, int right, int? comp1=-1, int? comp2=-1, bool? didSwap=false)
        //{
        //    Console.SetCursorPosition(0,0);
            
        //    for(int i = 0; i < this.Length; i++)
        //    {
        //        int current = int.Parse( this.array[i].ToString());
        //        String lrVal = " ";
        //        char barChar = '#';
        //        //Console.Write("{0,2}-", current);
        //        if(i == left)
        //        {
        //            lrVal="L";
        //            //barChar = '-';
        //        }
        //        else if(i == right)
        //        {
        //            lrVal="R";
        //            //barChar = '+';
        //        }
        //        else if(this.pivots.Contains(i))
        //        {
        //            lrVal = "X";
        //        }
        //        if (i == pivot)
        //        {
        //            Console.ForegroundColor = ConsoleColor.Green;
        //            //Console.Beep(37 + int.Parse(this.array[pivot].ToString()) * 20, 100);
        //        }
        //        else if (i == comp1)
        //        {
        //            Console.ForegroundColor = ConsoleColor.DarkRed;

                    
        //        }
        //        else if (i == comp2)
        //        {
        //            Console.ForegroundColor = ConsoleColor.DarkYellow;
        //        }
        //        else if(i < right && i >= left)
        //        {
        //            Console.ForegroundColor = ConsoleColor.DarkGreen;
        //        }
        //        int cWidth = Console.BufferWidth - 5;
        //        Console.WriteLine("{0,2}-{1}{2,-" + cWidth + "}",current,lrVal, new string(barChar, current));
        //        Console.ResetColor();

        //    }
        //    Console.WriteLine("Number of comparisons: {0}                                                                          ", comparisons);
        //    if (didSwap == true)
        //    {
        //        System.Threading.Thread.Sleep(20);
        //        if (comp1 != -1)
        //        {
        //            //Console.Beep(37 + int.Parse(this.array[comp2.Value].ToString()) * 31, 100);
        //        }
        //    }
        //    else if(comp1 != -1 && didSwap == false)
        //    {

        //        //Console.Beep(37 + int.Parse(this.array[comp1.Value].ToString()) * 31, 100);
        //    }
            
        //    System.Threading.Thread.Sleep(20);
        //}
        #endregion

    }
}
