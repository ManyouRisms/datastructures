﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    class KnuthShellSort<T> : ShellSorter<T> where T : IComparable<T>
    {

        public KnuthShellSort(T[] array)
            : base(array)
        {

        }

        /// <summary>
        /// Knuth's shell sort attempts to deal with the "power of 2" problem in Shell's original algorithm.
        /// It is defined as "gap=((3^k)-1)/2" where k is 1 for the first gap, 2 for the second gap and so
        /// on, while the produced gap is not greater than N/3.
        /// </summary>
        /// <param name="arraySize"></param>
        /// <returns></returns>
        public override IEnumerable<int> GetGapList(int arraySize)
        {
            Stack<int> gaps = new Stack<int>();


            int gap = 1;
            int gapNumber = 1;
            //((3^gapNumber)-1)/2
            gap = (pow(3, gapNumber) - 1) / 2;

            while (gap < arraySize / 3)
            {
                gaps.Push(gap);

                gapNumber++;

                gap = (pow(3, gapNumber) - 1) / 2; 
            }

            return gaps;
        }


    }
}
