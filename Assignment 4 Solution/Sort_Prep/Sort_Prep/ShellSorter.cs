﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    class ShellSorter<T> : InsertionSorter<T> where T : IComparable<T>
    {

        public ShellSorter(T[] array) : base(array)
        {

        }

        /// <summary>
        /// ShellSort is an optimized version of the insertion sort that attempts to deal
        /// with the case of insertion sort where small items must be swapped many times to 
        /// reach their correct position. Rather than swapping items one position at a time,
        /// Shell's sort swaps them an ever-decreasing number (gap) of spaces at once.
        /// </summary>
        public override void Sort()
        {

            foreach (int gap in GetGapList(this.Length))
            {
                //Split the list into a number of interleaved smaller lists.
                //For each value of gap, where gap is <= n/2 and ends with 1
                    //Insertion sort each gap-list
                        //First gap list has item 1 at position zero, item 2 at item 1 index + gap
                        //Second gap list has item 1 at position one, item 2 at item 1 + gap
                        //And so on, until we have sorted all of the gap lists
                    //Move to the next value for the gap list.
                for (int offsetOfFirstElement = 0; offsetOfFirstElement < gap; offsetOfFirstElement++)
                {
                    //Shell sort internally uses an Insertion sort, where each item in the list is "gap"
                    //apart from its neighbors.
                    InsertionSortGapList(offsetOfFirstElement, gap);
                }
            }
        }

        
        /// <summary>
        /// Shell's gap list algorithm is defined as first gap=N/2, subsequent gaps = previousGap/2.
        /// Because we use integer math we eventually end up with a gap of 1, at which point we stop 
        /// producing gaps. This algorithm for determining gaps suffers from an issue when N is a power
        /// of 2; in that case, the gaps will also always be powers of 2, resulting in odd list items
        /// in the array only ever being sorted against odd, even against even. This results in O(n^2) 
        /// runtime.
        /// </summary>
        /// <param name="arraySize"></param>
        /// <returns></returns>
        public virtual IEnumerable<int> GetGapList(int arraySize)
        {
            

            int gap = arraySize/2;
            while(gap >= 1)
            {
                //Yield returns a value, but remembers where we left off so that the next time this method 
                // is called we start back up right where we left off (the next line after yield return).
                yield return gap;
                gap = gap / 2;
            }
        }

        public static int pow(int num, int exp)
        {
            int rval = num;
            if (exp == 0)
            {
                rval = 1;
            }

            for (int i = 1; i < exp; i++)
            {
                rval *= num;
            }
            return rval;
        }

        
    }
}
