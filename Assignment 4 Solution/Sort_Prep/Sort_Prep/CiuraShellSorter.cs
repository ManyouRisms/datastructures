﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    class CiuraShellSorter<T> : ShellSorter<T> where T : IComparable<T>
    {

        public CiuraShellSorter(T[] array)
            : base(array)
        {

        }


        /// <summary>
        /// Ciura's gap algorithm defines a fixed list of the first 9 gaps. Higher gaps
        /// can be calculated as gap = 2.25*previousGap, where gap remains below n/3.
        /// </summary>
        /// <param name="arraySize"></param>
        /// <returns></returns>
        public override IEnumerable<int> GetGapList(int arraySize)
        {
            //Use static list of gaps
            List<int> gaps = new List<int> { 701,301,132,57,23,10,4,1 };

            //This algorithm defines a fixed set of gaps which have been experimentally found
            // to be very efficient. However, with larger lists (ie lists where N/3 > 701),
            // we can calculate additional, larger gaps to help keep the sort efficient. 
            // The suggested formula is gap=2.25*previousGap, where gap must be < N/3.
            Stack<int> prefixGaps = new Stack<int>();
            int gapValue = (int)(2.25 * 701);
            if(arraySize/3 > 701)
            { 
                //If the gap of 701 is too small, we need to generate a sequence of additional gaps
                // the largest of which will be just below N/3.
                while (gapValue < arraySize / 3)
                {
                    
                    prefixGaps.Push(gapValue);
                    gapValue = (int)(2.25 * gapValue);
                }
            }

            //If we had to generate any additional gaps, they'll be in order from largest->smallest (1)
            // in prefixGaps. We can just add them to the static list of gaps to get a complete list.
            IEnumerable<int> allGaps = prefixGaps.Concat(gaps);
            foreach (int gap in allGaps)
            {
                //If the size of our list is smaller than 2103 (701*3), we only want to return those gaps
                // which are less than N/3.
                if (gap < arraySize / 3)
                {
                    yield return gap;
                }
            }
            
        }
    }
}
