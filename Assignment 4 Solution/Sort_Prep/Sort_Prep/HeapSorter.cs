﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    class HeapSorter<T> : AbstractSorter<T> where T : IComparable<T>
    {
        public HeapSorter(T[] array)
            : base(array)
        {
        }

        public override void Sort()
        {
            //Create initial heap
            Heapify();
            //For each location in the array starting at the last and going to the first
            // (effectively repeatedly breaking the heap by swapping the last leaf node
            // to the root, and removing the root from the tree)
            for (int i = array.Length - 1; i > 0; i--)
            {
                //Move largest element to the current position of the sorted array.
                // This effectively partitions the list into sorted (the last i items)
                // and unsorted portions, where the unsorted portion is the remainder of 
                // the heap.
                Swap(0, i);
                
                //Sift the swapped element (which will be relatively small, since it
                // came from the "bottom" of the heap) down the heap until the heap
                // property is restored. The item at index i is the item we just pulled
                // from the root of the heap, and is no longer considered part of the heap.
                RecursiveSiftDown(0, i - 1);
                //IterativeSiftDown(0, i - 1);
                
                
                //PrintBars(-1, -1, -1, i-1);
            }
        }



        private void Heapify()
        {
            //Get the index of the last parent in the heap. This is an optimization:
            // the "furthest down" parent on the heap is the first one from the end which
            // could possibly not have the heap property, since leaf nodes always have hte heap
            // property as they have no children.
            int indexOfLastParent = GetParentIndex(array.Length - 1);


            //For every parent starting at last up to and including the first
            for (int i = indexOfLastParent; i >= 0; i--)
            {
                //Sift the current element down the heap until it has the heap property
                RecursiveSiftDown(i, array.Length - 1);
                //IterativeSiftDown(i, array.Length - 1);
            }
        }

        int GetParentIndex(int index)
        {
            return (index - 1) / 2;
        }

        int GetLeftChildIndex(int index)
        {
            return index * 2 + 1;
        }

        int GetRightChildIndex(int index)
        {
            return index * 2 + 2;
        }

        /// <summary>
        /// Iterative implementation of siftdown. Very similar to recursive, but rather than
        /// recursing it simply re-calculates the parent, left and right children indexes at
        /// the end of each iteration.
        /// </summary>
        /// <param name="currentRootIndex"></param>
        /// <param name="lastIndexInHeap"></param>
        private void IterativeSiftDown(int currentRootIndex, int lastIndexInHeap)
        {
            int indexOfLeftChild = GetLeftChildIndex(currentRootIndex);
            int indexOfRightChild = GetRightChildIndex(currentRootIndex);

            int indexOfLargest = currentRootIndex;

            //If there is at least one child of the current item
            while (indexOfLeftChild <= lastIndexInHeap)
            {
                //If the left child (left child will exist if there are any children)
                // and it is greater than the root, then it is the largest we've found so far
                if (this.array[currentRootIndex].CompareTo(this.array[indexOfLeftChild]) < 0)
                {
                    indexOfLargest = indexOfLeftChild;
                }
                //If there is a right child, also check it against the currently known largest value,
                // keeping track of which one was larger
                if (indexOfRightChild <= lastIndexInHeap && this.array[indexOfLargest].CompareTo(this.array[indexOfRightChild]) < 0)
                {
                    indexOfLargest = indexOfRightChild;
                }
                //PrintBars(currentRootIndex, indexOfLeftChild, indexOfRightChild, lastIndexInHeap);
                
                //If the largest item of the three was the root, then we have a heap. Otherwise,
                // we need to swap the root with the largest of its children, then re-heap the 
                // child's tree (since the root we swapped to its position may have been lower than one
                // or more of that child element's children.)
                // We don't need to re-heap the other child, since it won't be affected: it's still smaller than

                if (indexOfLargest == currentRootIndex)
                {
                    return;

                }
                else
                {
                    Swap(currentRootIndex, indexOfLargest);
                    //PrintBars(currentRootIndex, indexOfLeftChild, indexOfRightChild, lastIndexInHeap);

                    //Rather than recursing, we can just re-calculate the root, left and right children and repeat.
                    currentRootIndex = indexOfLargest;

                    indexOfLeftChild = GetLeftChildIndex(currentRootIndex);
                    indexOfRightChild = GetRightChildIndex(currentRootIndex);
                }

            }

        }

        /// <summary>
        /// Recursive implementation of siftdown.
        /// </summary>
        /// <param name="currentRootIndex"></param>
        /// <param name="lastIndexInHeap"></param>
        private void RecursiveSiftDown(int currentRootIndex, int lastIndexInHeap)
        {
            int indexOfLeftChild = GetLeftChildIndex(currentRootIndex);
            int indexOfRightChild = GetRightChildIndex(currentRootIndex);

            int indexOfLargest = currentRootIndex;

            //If there is at least one child of the current item
            if (indexOfLeftChild <= lastIndexInHeap)
            {
                //If the left child (left child will exist if there are any children)
                // and it is greater than the root, then it is the largest we've found so far
                if (this.array[currentRootIndex].CompareTo(this.array[indexOfLeftChild]) < 0)
                {
                    indexOfLargest = indexOfLeftChild;
                }
                //If there is a right child, also check it against the currently known largest value,
                // keeping track of which one was larger
                if (indexOfRightChild <= lastIndexInHeap && this.array[indexOfLargest].CompareTo(this.array[indexOfRightChild]) < 0)
                {
                    indexOfLargest = indexOfRightChild;
                }

                //If the largest item of the three was the root, then we have a heap. Otherwise,
                // we need to swap the root with the largest of its children, then re-heap the 
                // child's tree (since the root we swapped to its position may have been lower than one
                // or more of that child element's children).
                if (indexOfLargest != currentRootIndex)
                {
                    //If we don't check for "largest value is not the root", we end up in infinite recursion.
                    Swap(currentRootIndex, indexOfLargest);
                    RecursiveSiftDown(indexOfLargest, lastIndexInHeap);
                }
            }

        }


        
        ///Another way to implement siftdown, not used in class
        //private void SiftDown(int currentItemIndex, int lastIndexInHeap)
        //{
        //    T valueOfLeftChild = default(T);
        //    T valueOfRightChild = default(T);
        //    T tParent = array[currentItemIndex];
        //    int indexOfLeftChild = GetLeftChildIndex(currentItemIndex);
        //    int indexOfRightChild = GetRightChildIndex(currentItemIndex);


        //    //if right exists, so does the left
        //    if (indexOfRightChild <= lastIndexInHeap)
        //    {
        //        //Get a reference to the left and the right children
        //        valueOfLeftChild = array[indexOfLeftChild];
        //        valueOfRightChild = array[indexOfRightChild];

        //        //If left is larger than the parent _AND_ larger than or equal to the left
        //        if (valueOfLeftChild.CompareTo(tParent) > 0 && valueOfLeftChild.CompareTo(valueOfRightChild)>= 0)
        //        {
        //            //Swap left and parent
        //            Swap(indexOfLeftChild, currentItemIndex);
        //            //Recursively sift the left child down the heap
        //            SiftDown(indexOfLeftChild, lastIndexInHeap);
        //        }
        //        //Else if right is the biggest of the parent and greater than the left
        //        else if (valueOfRightChild.CompareTo(tParent) > 0 && valueOfRightChild.CompareTo(valueOfLeftChild) > 0)
        //        {
        //            //Swap the right and the parent
        //            Swap(indexOfRightChild, currentItemIndex);
        //            //Recursively sift the right child down the heap
        //            SiftDown(indexOfRightChild, lastIndexInHeap);
        //        }


                
        //    }
        //    //else if only the left child exists
        //    else if (indexOfLeftChild <= lastIndexInHeap)
        //    {
        //        //Get a reference to the left child
        //        valueOfLeftChild = array[indexOfLeftChild];
        //        //If left is the biggest
        //        if (valueOfLeftChild.CompareTo(tParent) > 0 )
        //        {
        //            //Swap the left child and the parent
        //            Swap(indexOfLeftChild, currentItemIndex);
        //        }
        //    }
        //}



        public void PrintBars(int parent, int cmp1, int cmp2, int boundary)
        {
            Console.SetCursorPosition(0, 0);

            for (int i = 0; i < this.Length; i++)
            {
                int current = int.Parse(this.array[i].ToString());
                String lrVal = " ";
                char barChar = '#';
                
                if(i == parent)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                }
                else if(i == cmp1 || i == cmp2)
                {
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                }
                else if(i > boundary)
                {
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                }
                int cWidth = Console.BufferWidth - 5;
                Console.WriteLine("{0,2}-{1}{2,-" + cWidth + "}", current, lrVal, new string(barChar, current));
                Console.ResetColor();

            }
            

            System.Threading.Thread.Sleep(50);
        }
    }
}
