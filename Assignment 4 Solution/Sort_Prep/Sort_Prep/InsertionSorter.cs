﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    class InsertionSorter<T> : AbstractSorter<T> where T : IComparable<T>
    {
        public InsertionSorter(T[] array)
            : base(array)
        {

        }

        public override void Sort()
        {
            //Split the array into sorted and unsorted portions. Start the sorted portion out with the left most element
            //Take the next item from the unsorted portion
            //Shift it left by swapping until it's in the correct position
            //When there are no more items in the unsorted portion, we're done

            this.InsertionSortGapList(0, 1);

            ///Our original implementation of insertionsort, before factoring it out so that it could
            /// be used by shellsort.
            /// 
            //for (int unsortedListStartIndex = 1; unsortedListStartIndex < this.Length; unsortedListStartIndex++)
            //{
            //    //unsortedListStartIndex is the position at which the next unsorted item is initially found. This also
            //    // serves as our divide between the sorted and unsorted portions of the array.

            //    //CurrentUnsortedItemIndex is the current position in the list of the item we're inserting into the sorted list
            //    int currentUnsortedItemIndex = unsortedListStartIndex;

            //    //If we can still shift the "unsorted" item further to the left in the sorted section, and 
            //    //it should be shifted left (ie it's less than the item to its left), then shift it left
            //    while (currentUnsortedItemIndex - 1 >= 0 &&
            //        this.array[currentUnsortedItemIndex].CompareTo(this.array[currentUnsortedItemIndex - 1]) < 0)
            //    {
            //        this.Swap(currentUnsortedItemIndex, currentUnsortedItemIndex - 1);
            //        currentUnsortedItemIndex--;
            //    }
            //}
        }

        public void InsertionSortGapList(int offsetOfFirstElement, int gap)
        {
            //PrintBars(offsetOfFirstElement, gap);
            for (int unsortedListStartIndex = offsetOfFirstElement + gap; unsortedListStartIndex < this.Length; unsortedListStartIndex += gap)
            {
                //nextUnsortedItemInitialPosition is the position at which the next unsorted item was initially found. This also
                // serves as our divide between the sorted and unsorted portions of the array.

                int currentUnsortedItemIndex = unsortedListStartIndex;

                //PrintBars(offsetOfFirstElement, gap, currentUnsortedItemIndex, currentUnsortedItemIndex - gap);

                //If we can still shift the "unsorted" item further to the left in the sorted section, and 
                //it should be shifted left (ie it's less than the next item to its left in its gap list), then shift it left
                while (currentUnsortedItemIndex - gap >= 0 &&
                    this.array[currentUnsortedItemIndex].CompareTo(this.array[currentUnsortedItemIndex - gap]) < 0)
                {
                    //PrintBars(offsetOfFirstElement, gap, currentUnsortedItemIndex, currentUnsortedItemIndex - gap, true);

                    //currentUnsortedItemIndex - gap is the index of the previous item in this list
                    this.Swap(currentUnsortedItemIndex, currentUnsortedItemIndex - gap);

                    //PrintBars(offsetOfFirstElement, gap, currentUnsortedItemIndex, currentUnsortedItemIndex - gap, true);

                    currentUnsortedItemIndex -= gap; //Move left to the position we just swapped to in the list.
                }
            }
        }

        public void PrintBars(int start, int gap, int? cmp1 = -1, int? cmp2 = -1, bool? didSwap = false)
        {
            Console.SetCursorPosition(0, 0);

            Console.WriteLine("Gap: " + gap + "                                          ");
            Console.WriteLine("List: " + (start + 1) + "                                          ");
            
            for (int i = 0; i < this.Length; i++)
            {
                int current = int.Parse(this.array[i].ToString());
                String lrVal = " ";
                if (cmp1 == i || cmp2 == i)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;

                }
                if (i == start)
                {

                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    

                    start += gap;
                }

                if (cmp1 == i || cmp2 == i)
                {

                    if (didSwap == true)
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;

                    }

                }

                int cWidth = Console.BufferWidth - 5;
                Console.WriteLine("{0,2}-{1}{2,-" + cWidth + "}", current, lrVal, new string('#', current));
                Console.ResetColor();

            }
            System.Threading.Thread.Sleep(50);
            if (cmp1 > 0)
            {
                System.Threading.Thread.Sleep(50);
            }
        }
    }
}
