﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    public class IterativeQuickSorter<T> : MedianRandQuickSorter<T> where T : IComparable<T>
    {
        public IterativeQuickSorter(T[] array)
            : base(array)
        {

        }

        /// <summary>
        /// "Iterative" implementation of a quicksort. In air-quotes as it just uses a stack
        /// data structure instead of the call stack.
        /// </summary>
        public override void Sort()
        {
            Stack<Tuple<int, int>> partitions = new Stack<Tuple<int, int>>();

            //Prime the stack
            //A 2-tuple is just a pair of two values. Item1 is left index, Item2 is right index
            partitions.Push(Tuple.Create(0, this.Length-1));

            while (partitions.Count > 0)
            {
                Tuple<int, int> currentPartition = partitions.Pop();
                int left = currentPartition.Item1;
                int right = currentPartition.Item2;
                if (left < right)
                {
                    //Partition the current subsection of the list
                    int partitionPosition = Partition(left, right);

                    //Remember to partition to the right of the pivot later
                    partitions.Push(Tuple.Create(partitionPosition + 1, right));

                    //Partition to the left of the pivot next (LIFO)
                    partitions.Push(Tuple.Create(left, partitionPosition - 1));
                }
            }

            
        }

    }
}
