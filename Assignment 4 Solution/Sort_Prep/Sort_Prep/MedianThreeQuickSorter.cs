﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    public class MedianThreeQuickSorter<T> : QuickSorter<T> where T : IComparable<T>
    {
        public MedianThreeQuickSorter(T[] array)
            : base(array)
        {

        }




        public override int GetPivotBetween(int left, int right)
        {
            int midIndex = (left + right) / 2;
            return GetIndexOfMedian(left, midIndex, right);    
        }

        /// <summary>
        /// Implementation of the Median of Three pivot selection algorithm. This algorithm attempts
        /// to improve the efficiency of the Quicksort by being more likely to choose a value which
        /// is closer to the "median" of the set of values in this section of the list, which should
        /// partition the list roughly in half. This is the best algorithm for dealing with an already
        /// sorted list in the quicksort.
        /// </summary>
        /// <param name="lowIndex"></param>
        /// <param name="midIndex"></param>
        /// <param name="highIndex"></param>
        /// <returns></returns>
        protected int GetIndexOfMedian(int lowIndex, int midIndex, int highIndex)
        {
            T lowValue = array[lowIndex]; //A
            T highValue = array[highIndex]; //B
            T midValue = array[midIndex]; //C


            if (lowValue.CompareTo(highValue) < 0) //A < B
            {
                if (highValue.CompareTo(midValue) < 0) //A < B < C
                {
                    return highIndex;
                }
                else if (lowValue.CompareTo(midValue) < 0) //A < C < B
                {
                    return midIndex;
                }
                else //C < A < B
                {
                    return lowIndex;
                }
            }
            else // A > B 
            {
                if (highValue.CompareTo(midValue) > 0)//C < B < A
                {
                    return highIndex;
                }
                else if (lowValue.CompareTo(midValue) > 0)// B < C < A
                {
                    return midIndex;
                }
                else // B < A < C
                {
                    return lowIndex;
                }
            }
        }
    }
}
