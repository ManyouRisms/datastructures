﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Prep
{
    /// <summary>
    /// Implementation of a radix sort to sort an array of integer values.
    /// </summary>
    public class RadixSorter : AbstractSorter<int>
    {
        /// <summary>
        /// A linked list array to store the values based on the digit being
        /// considered.
        /// </summary>
        private LinkedList<int>[] buckets;
        /// <summary>
        /// Number of buckets. We use 10 for simplicity, but can be any value.
        /// </summary>
        private const int NUM_BUCKETS = 10;

        /// <summary>
        /// Constructor passes the array to the parent class. Also initializes
        /// the buckets.
        /// </summary>
        /// <param name="arrayToSort">The array to be sorted</param>
        public RadixSorter(int[] arrayToSort)
            : base(arrayToSort)
        {
            // initialize the bucket array
            buckets = new LinkedList<int>[NUM_BUCKETS];
            for (int i = 0; i < buckets.Length; i++)
            {
                buckets[i] = new LinkedList<int>();
            }
        }

        public override void Sort()
        {
            this.doSort(0, this.Length-1);
        }

        /// <summary>
        /// Implementation of the radix sort over the specified range.
        /// </summary>
        /// <param name="start">The index of the first element to be sorted
        /// (inclusive)</param>
        /// <param name="end">The index of the last element to be sorted
        /// (exclusive)</param>
        public  void doSort(int start, int end)
        {
            // find the largest value
            int largest = FindLargest(start, end);
            // how many digits
            int maxRadix = (int)Math.Log(largest, NUM_BUCKETS);
            // loop through each of the digits, starting with least significant
            for (int radix = 0; radix <= maxRadix; radix++)
            {
                // loop through each of the array elements
                for (int j = start; j <= end; j++)
                {
                    // put array value into correct bucket
                    buckets[GetRadixValue(array[j], radix)].AddLast(array[j]);
                }
                // rebuild the array
                int arrayPos = start;
                // go through each bucket
                for (int bucketNum = 0; bucketNum < buckets.Length; bucketNum++)
                {
                    LinkedList<int>.Enumerator e = buckets[bucketNum].GetEnumerator();
                    // coalesce the buckets back into the array
                    foreach (int number in buckets[bucketNum])
                    {
                        array[arrayPos++] = number;
                    }
                    buckets[bucketNum].Clear();
                }
            }
        }

        /// <summary>
        /// Gets the appropriate digit from the given number.
        /// </summary>
        /// <param name="number">The number to use</param>
        /// <param name="radix">Which digit to get</param>
        /// <returns>The digit in that position</returns>
        private int GetRadixValue(int number, int radix)
        {
            return (number / ((int)Math.Pow(NUM_BUCKETS, radix))) % NUM_BUCKETS;
        }

        /// <summary>
        /// Does a pass through the array to find the largest value. This is
        /// used later to determine how many digits there are.
        /// </summary>
        /// <param name="start">The index of the first element to be sorted
        /// (inclusive)</param>
        /// <param name="end">The index of the last element to be sorted
        /// (exclusive)</param>
        /// <returns>The largest value in the array</returns>
        private int FindLargest(int start, int end)
        {
            int largest = array[start];
            for (int i = start + 1; i <= end; i++)
            {
                if (largest.CompareTo(array[i]) < 0)
                {
                    largest = array[i];
                }
            }
            return largest;
        }
    }
}
